import logo from './logo.svg';
import './App.css';
import {
  ThemeProvider,
  CssBaseline,
  createTheme,
  Button,
  colors,
  Box,
  Typography as Typo
} from '@material-ui/core';

import Router from "./router/index"
function App() {
  return (
    <ThemeProvider theme={newTheme}>
      <div>
        <CssBaseline />
        <Router />
      </div>
    </ThemeProvider>
  );
}

export default App;

const newTheme = createTheme({
  // mã màu
  palette: {
    primary: {
      main: colors.green[600],
    }
  },
  // custome chữ
  typography: {
    fontFamily: `Raleway', sans-serif`
  }
})
