import React from 'react';
import { Box } from '@material-ui/core';
import CarTable from '../../Components/CarTable'
const Index = (props) => {
    return (
        <Box>
            <CarTable {...props} />
        </Box>
    );
}

export default Index;
