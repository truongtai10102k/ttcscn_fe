import React from 'react';
import CompanyTable from '../../Components/CompanyTable';
import { Box } from '@material-ui/core';
const Index = (props) => {
    return (
        <Box>
            <CompanyTable {...props} />
        </Box>
    );
}

export default Index;
