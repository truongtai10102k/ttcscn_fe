import React, { useState } from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import TableContract from "../../Components/TableContract"
import FullScreenLoading from "../../Components/Loading/FullScreenLoading"
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: "100%",
  },
}));

export default function FullWidthTabs(props) {
  const { listData, putContact, getContact, isLoading } = props

  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0); 
  const [type, setType] = useState('pendding');

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const handleChangeTab = (type) => {
    setType(type)
    getContact({ page: 1, status: type })
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="Chờ Duyệt" {...a11yProps(0)} onClick={() => handleChangeTab('pendding')} />
          <Tab label="Đang Xử Lý" {...a11yProps(1)} onClick={() => handleChangeTab('processing')} />
          <Tab label="Từ Chối" {...a11yProps(2)} onClick={() => handleChangeTab('refuse')} />
          <Tab label="Đã Hoàn Thành" {...a11yProps(3)} onClick={() => handleChangeTab('complete')} />
        </Tabs>
      </AppBar>
      {
        isLoading
          ?
          <FullScreenLoading />
          :
          <SwipeableViews
            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
            index={value}
            onChangeIndex={handleChangeIndex}
          >
            <TabPanel value={value} index={0} dir={theme.direction}>
              <TableContract {...props} getContact={getContact} listData={listData} putContact={putContact} type={type} />
            </TabPanel>
            <TabPanel value={value} index={1} dir={theme.direction}>
              <TableContract {...props} getContact={getContact} listData={listData} putContact={putContact} type={type} />
            </TabPanel>
            <TabPanel value={value} index={2} dir={theme.direction}>
              <TableContract {...props} getContact={getContact} listData={listData} putContact={putContact} type={type} />
            </TabPanel>
            <TabPanel value={value} index={3} dir={theme.direction}>
              <TableContract {...props} getContact={getContact} listData={listData} putContact={putContact} type={type} />
            </TabPanel>
          </SwipeableViews>
      }
    </div>
  );
}