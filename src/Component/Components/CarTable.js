import React, { useState } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {
    IconButton, Table,
    TableBody, TableContainer,
    TableCell, TableHead,
    TableRow, Paper, Box,
    TextField, Button,
} from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete';
import {
    Visibility as ViewIcon,
    EditOutlined as EditIcon,
    DeleteOutline as DeleteIcon,
    AddCircle as AddIcon
} from "@material-ui/icons"
import Pagination from '@material-ui/lab/Pagination'
import ModalCar from '../Components/ModalCar';
import { ModalConfirmDeleteCar } from './ModalConfirm'
export default function CustomizedTables(props) {
    const { listData, listCompany, deleteCar,
        getCar, activePage, totalPage, tx, company
    } = props
    const [openDialog, setOpenDialog] = useState(false);
    const [type, setType] = useState('');
    const [data, setData] = useState({});
    const [textSearch, setTextSearch] = useState('');
    const [idCompany, setIdCompany] = useState('');
    const [openModalDelete, setOpenModalDelete] = useState(false);
    const classes = useStyles();

    const hanldeAddDialog = () => {
        setType('add')
        setData({})
        setOpenDialog(true)
    }

    const hanldeCloseDialog = () => {
        setOpenDialog(false)
    }
    const hanldeCloseModalDelete = () => {
        setOpenModalDelete(false)
    }

    const hanldeGetCarByCompany = (e, value) => {
        if (value && value.id) {
            getCar({ page: 1, id_car_company: value.id })
            setIdCompany(value.id)
            setTextSearch('')
        } else {
            getCar({ page: 1, id_car_company: '' })
            setIdCompany('')
            setTextSearch('')
        }
    }

    return (
        <Box className={classes.container}>
            <Box className={classes.searchBox}>
                <Box className={classes.dropDown}>
                    <Autocomplete
                        fullWidth
                        className="input-filter"
                        size="small"
                        // value={{ title: fractionPending }}
                        options={listCompany}
                        getOptionLabel={(option) => option.name}
                        renderInput={(params) => <TextField {...params}
                            placeholder="Hãng xe"
                            variant="outlined"
                        />}
                        onChange={hanldeGetCarByCompany}
                    />
                </Box>

                <TextField
                    fullWidth
                    value={textSearch}
                    className={classes.inputSearch}
                    label='Tìm Kiếm ...' size="small"
                    variant='outlined'
                    onChange={(e) => setTextSearch(e.target.value)}
                />
                <Button variant="contained" color="primary" onClick={() => {
                    getCar({ page: 1, textSearch: textSearch, id_car_company: idCompany })
                }}>Tìm Kiếm</Button>

                <Button className={classes.addCompany} variant="contained" color="primary"
                    onClick={hanldeAddDialog}
                >Thêm Mới</Button>
            </Box>
            <TableContainer className={classes.tableContainer} component={Paper}>
                <Table stickyHeader className={classes.table}>
                    <TableHead className={classes.head} >
                        <TableRow>
                            <StyledTableCell className={classes.tableHead} align="center" width="" >STT</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="">Tên Hãng Xe</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="">Tên Xe</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="">Biển Số</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="">Trạng Thái</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="">Hành Động</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {listData && listData.map((row, index) => (
                            <StyledTableRow key={index} onClick={() => {
                                // alert(row.name)
                                
                            }}>
                                <StyledTableCell component="th" scope="row" className={classes.tableCell} width="5%" align="center">
                                    {index + 1}
                                </StyledTableCell>
                                <StyledTableCell className={classes.tableCell} align="center" width="" >{row.name_car_company}</StyledTableCell>
                                <StyledTableCell className={classes.tableCell} align="center" width="" >{row.name}</StyledTableCell>
                                <StyledTableCell className={classes.tableCell} align="center" width="" >{row.licensePlate}</StyledTableCell>
                                <StyledTableCell className={classes.tableCell} align="center" width="" >{row.status ? 'Còn xe' : 'Đang cho thuê'}</StyledTableCell>
                                <StyledTableCell align="center" width="">
                                    <IconButton color="primary" component="span"
                                        onClick={() => {
                                            setType('view')
                                            setData(row)
                                            setOpenDialog(true)
                                        }}>
                                        <ViewIcon />
                                    </IconButton>
                                    <IconButton color="primary" component="span"
                                        onClick={() => {
                                            setType('edit')
                                            setData(row)
                                            setOpenDialog(true)
                                        }}>
                                        <EditIcon fontSize="medium" />
                                    </IconButton>
                                    <IconButton color="primary" component="span"
                                        onClick={() => {
                                            setOpenModalDelete(true)
                                            setData(row)
                                        }}
                                    >
                                        <DeleteIcon />
                                    </IconButton>
                                </StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>

            </TableContainer>
            <Pagination
                className={classes.pagination}
                color="secondary"
                count={totalPage}
                page={activePage}
                onChange={(e, number) => {
                    getCar({ page: number, textSearch: tx, id_car_company: (company === undefined ? '' : company) })
                }} />
            <ModalCar
                open={openDialog}
                close={hanldeCloseDialog}
                data={data}
                type={type}
                {...props}
            />
            <ModalConfirmDeleteCar
                close={hanldeCloseModalDelete}
                open={openModalDelete}
                data={data}
                deleteCar={deleteCar}
            />
        </Box >
    );
}

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    }
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        }, '& > *': {
            marginTop: theme.spacing(2),
        },
    },
}))(TableRow);

const useStyles = makeStyles({
    container: {
        margin: '10px 10px 0'
    },
    tableContainer: {
        maxHeight: "80vh",
        // maxWidth: '40%',
        position: 'relative'
    }, addCompany: {
        marginLeft: 35
    },
    table: {
        // minWidth: 700,

    },
    tableHead: {
        backgroundColor: "#43a047",
        borderRight: "1px solid #fff"

    },
    tableCell: {
        borderRight: "1px solid #43a047a6",
    }, pagination: {
        display: "flex",
        justifyContent: 'center',
        margin: '20px 60px 0'
    }, searchBox: {
        paddingBottom: 8,
        display: 'flex'
    }, inputSearch: {
        maxWidth: 350,
        marginRight: 10
    }, dropDown: {
        width: 200,
        marginRight: 10
    }
});

