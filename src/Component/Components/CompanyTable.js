import React, { useState } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {
    IconButton, Table,
    TableBody, TableContainer,
    TableCell, TableHead,
    TableRow, Paper, Box,
    TextField, Button
} from '@material-ui/core'
import {
    Visibility as ViewIcon,
    EditOutlined as EditIcon,
    DeleteOutline as DeleteIcon,
    AddCircle as AddIcon
} from "@material-ui/icons"
import Pagination from '@material-ui/lab/Pagination'
import ModalCompany from '../Components/ModalCompany';
import { ModalConfirmDelete } from '../Components/ModalConfirm';
import ImageUrl from '../../image/logo512.png';

export default function CustomizedTables(props) {
    const { listData, deleteCompany,
        activePage, totalPage, tx, getCompany
    } = props
    const [openDialog, setOpenDialog] = useState(false);
    const [openConfirmDelete, setOpenConfirmDelete] = useState(false);
    const [type, setType] = useState('');
    const [data, setData] = useState({});
    const [textSearch, setTextSearch] = useState('');
    const classes = useStyles();

    const hanldeAddDialog = () => {
        setType('add')
        setData({})
        setOpenDialog(true)
    }

    const hanldeCloseDialog = () => {
        setOpenDialog(false)
    }


    const hanldeCloseDelete = () => {
        setOpenConfirmDelete(false)
    }

    return (
        <Box className={classes.container}>
            <Box className={classes.searchBox}>
                <TextField fullWidth
                    className={classes.inputSearch}
                    label='Tìm Kiếm ...' size="small"
                    variant='outlined'
                    value={textSearch}
                    onChange={(e) => setTextSearch(e.target.value)}
                />
                <Button variant="contained" color="primary"
                    onClick={() => {
                        getCompany({ page: 1, textSearch })
                    }}
                >Tìm Kiếm</Button>
                <Button className={classes.addCompany} variant="contained" color="primary"
                    onClick={hanldeAddDialog}
                >Thêm Mới</Button>
            </Box>
            <TableContainer className={classes.tableContainer} component={Paper}>
                <Table stickyHeader className={classes.table}>
                    <TableHead className={classes.head} >
                        <TableRow>
                            <StyledTableCell className={classes.tableHead} align="center" width="5%" >STT</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="50%">Tên Hãng Xe</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="45%">Hành Động</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {listData && listData.map((row, index) => (
                            <StyledTableRow key={index} onClick={() => {
                                // alert(row.name)
                            }}>
                                <StyledTableCell component="th" scope="row" className={classes.tableCell} width="5%" align="center">
                                    {index + 1}
                                </StyledTableCell>
                                <StyledTableCell className={classes.tableCell} align="center" width="50%" >{row.name}</StyledTableCell>
                                <StyledTableCell align="center" width="45%">
                                    <IconButton color="primary" component="span"
                                        onClick={() => {
                                            setType('view')
                                            setData(row)
                                            setOpenDialog(true)
                                        }}>
                                        <ViewIcon />
                                    </IconButton>
                                    <IconButton color="primary" component="span"
                                        onClick={() => {
                                            setType('edit')
                                            setData(row)
                                            setOpenDialog(true)
                                        }}>
                                        <EditIcon fontSize="medium" />
                                    </IconButton>
                                    <IconButton color="primary" component="span"
                                        onClick={() => {
                                            setOpenConfirmDelete(true)
                                            setData(row)
                                        }}
                                    >
                                        <DeleteIcon />
                                    </IconButton>
                                </StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>

            </TableContainer>
            <Pagination
                className={classes.pagination}
                color="secondary"
                count={totalPage}
                onChange={(e, number) => {
                    getCompany({ page: number, textSearch: tx })
                }}
                page={activePage}
            />
            <ModalCompany
                open={openDialog}
                close={hanldeCloseDialog}
                data={data}
                type={type}
                {...props}
            />
            <ModalConfirmDelete
                open={openConfirmDelete}
                close={hanldeCloseDelete}
                data={data}
                deleteCompany={deleteCompany}
            />
        </Box >
    );
}

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    }
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        }, '& > *': {
            marginTop: theme.spacing(2),
        },
    },
}))(TableRow);

const useStyles = makeStyles({
    container: {
        margin: '10px 10px 0'
    },
    tableContainer: {
        maxHeight: "72vh",
        maxWidth: '40%',
        position: 'relative'
    }, addCompany: {
        marginLeft: 35
    },
    table: {
        // minWidth: 700,

    },
    tableHead: {
        backgroundColor: "#43a047",
        borderRight: "1px solid #fff"

    },
    tableCell: {
        borderRight: "1px solid #43a047a6",
    }, pagination: {
        // display: "flex",
        // justifyContent: 'center',
        margin: '20px 60px 0'
    }, searchBox: {
        paddingBottom: 8
    }, inputSearch: {
        maxWidth: 250,
        marginRight: 10
    }
});
