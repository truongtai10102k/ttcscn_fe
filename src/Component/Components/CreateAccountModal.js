import React, { useState } from 'react';
import { Box, Dialog, makeStyles, Typography, Grid, TextField, Button } from '@material-ui/core';
const CreateAccountModal = (props) => {
    const { open, close, postAccountAdmin } = props

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [phone, setPhone] = useState(0);
    const [mail, setEmail] = useState('');

    function validateEmail(email) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    const handleSubmit = () => {
        if (!validateEmail(mail)) {
            alert("vui lòng nhập đúng định dang mail")
            return;
        }
        if (password.length < 6) {
            alert('Mật khẩu dài hơn 6 kí tự')
            return;
        }
        if (confirmPassword !== password) {
            alert("Mật khẩu không khớp")
            return;
        }
        if (phone && firstName && lastName && password && confirmPassword && userName) {
            postAccountAdmin({ CustomerName: (firstName + lastName), userName, password, phone, email:mail })
        } else {
            alert('Vui lòng nhập đủ trưởng')
        }
    }


    const handleChange = (e, setState) => {
        setState(e.target.value)
    }

    const classes = useStyles()
    return (
        <Box>
            <Dialog open={open} onClose={close}>
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5">
                        Đăng Kí Tài Khoản
                    </Typography>
                    <Box className={classes.form}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    autoComplete="fname"
                                    name="firstName"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="firstName"
                                    label="First Name"
                                    autoFocus
                                    size="small"
                                    onChange={(e) => {
                                        handleChange(e, setFirstName)
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="lastName"
                                    label="Last Name"
                                    name="lastName"
                                    autoComplete="lname"
                                    size="small"
                                    onChange={(e) => {
                                        handleChange(e, setLastName)
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    label="UserName"
                                    size="small"
                                    onChange={(e) => {
                                        handleChange(e, setUserName)
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                    size="small"
                                    onChange={(e) => {
                                        handleChange(e, setPassword)
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Confirm Password"
                                    type="password"
                                    size="small"
                                    onChange={(e) => {
                                        handleChange(e, setConfirmPassword)
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth

                                    label="Phone Number"
                                    type="Number"
                                    size="small"
                                    onChange={(e) => {
                                        handleChange(e, setPhone)
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email Address"
                                    name="email"
                                    autoComplete="email"
                                    size="small"
                                    type="email"
                                    onChange={(e) => {
                                        handleChange(e, setEmail)
                                    }}
                                />
                            </Grid>

                        </Grid>
                        <Box className={classes.buttonGroup}>
                            <Button variant='contained' color='primary'
                                onClick={handleSubmit}
                            >Xác Nhận</Button>
                            <Button className={classes.buttonItem} color='#414141' variant='contained'
                                onClick={close}
                            >Hủy</Button>
                        </Box>
                    </Box>
                </div>

            </Dialog>
        </Box>
    );
}

export default CreateAccountModal;


const useStyles = makeStyles((theme) => ({
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        maxWidth: "450px",
        height: "500px",
        margin: 'auto',
        marginTop: "3%",
        backgroundColor: '#fafafa',
        padding: "15px 40px 15px",
        borderRadius: "10px",
        position: 'relative',
        zIndex: 2,
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    }, buttonGroup: {
        display: 'flex',
        justifyContent: 'space-evenly',
        marginTop: 30
    }, buttonItem: {
        padding: '0 40px'
    }
}));