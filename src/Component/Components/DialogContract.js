import React, { useState, useEffect } from 'react';
import {
    Box, Dialog,
    makeStyles, TextField,
    Button,
    IconButton, Grid
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
const DialogContract = (props) => {
    const { openContract, closeContract, data, putContact, type, carDetail, getCarDetail } = props

    useEffect(() => {
        if (data.id_car) {
            getCarDetail({ id: data.id_car })
            setNote(data.note)
        }
    }, [data]);
    useEffect(() => {
        if (carDetail?.price) {
            setPrice(carDetail.price)
        }
        if (carDetail?.insuranceFee) {
            setInsuranceFee(carDetail.insuranceFee)
            setMoney(handleBillMoney(data.startDate, data.endDate))
        }
    }, []);

    const classes = useStyles()
    const [note, setNote] = useState(data.note ? data.note : '');
    const [price, setPrice] = useState(0);
    const [money, setMoney] = useState(0);
    const [insuranceFee, setInsuranceFee] = useState(0);
    let buttonSuccess = '';
    let buttonRefuse = '';
    //status
    if (type == 'pendding') {
        buttonSuccess = 'Đồng Ý';
        buttonRefuse = 'Từ Chối';
    }
    if (type == 'processing') {
        buttonSuccess = 'Hoàn Thành';
        buttonRefuse = 'Từ Chối';
    }
    
    let statusSuccess = '';
    let statusRefuse = '';
    //button
    if (type == 'pendding') {
        statusSuccess = 'processing';
        statusRefuse = 'refuse';
    }
    if (type == 'processing') {
        statusSuccess = 'complete';
        statusRefuse = 'refuse';
    }

    const hanldeSuccess = () => {
        putContact({ id: data.id, note: note, status: statusSuccess, type })
        closeContract()
    }
    const hanldeRefuse = () => {
        putContact({ id: data.id, status: statusRefuse, type })
        closeContract()
    }
    const handleBillMoney = (start, end) => {
        var date1 = new Date(start);
        var date2 = new Date(end);
        var Difference_In_Time = date2.getTime() - date1.getTime();
        var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
        return (Difference_In_Days * (+price) + (+insuranceFee))
    }
 
    return (
        <Dialog open={openContract} onClose={closeContract}>
            <Box className={classes.container} >
                <Box className={classes.dialogHead}>Thông Tin Hợp Đồng <IconButton className={classes.iconbtn} onClick={closeContract}><CloseIcon /></IconButton> </Box>

                <Box>
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Mã hợp đồng' value={data && data.contractId} disabled />
                    </Box>
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Tên khách hàng' value={data && data.customerName} disabled />
                    </Box>
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Số Chứng Minh Thư' value={data && data.idCard} disabled />
                    </Box>
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' type='date' variant='outlined' label='Ngày Cấp' value={data && data.issueDate} disabled />
                    </Box >
                    <Box className={classes.inputText}>
                        <TextField minRows='3' multiline fullWidth size='small' variant='outlined' label='Địa Chỉ Thường Trú' value={data && data.address} disabled />
                    </Box >


                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Hãng xe' value={carDetail && carDetail.name_car_company} disabled />
                    </Box>
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Tên xe' value={carDetail && carDetail.name} disabled />
                    </Box>
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Biển Số' value={carDetail && carDetail.licensePlate} disabled />
                    </Box>
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Năm sản xuất' value={carDetail && carDetail.manufacture} disabled />
                    </Box>
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Số khung' value={carDetail && carDetail.frameNumber} disabled />
                    </Box>
                    <Box className={classes.inputText}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={5}>
                                <TextField
                                    fullWidth size='small'
                                    variant='outlined'
                                    label='Giá Thuê Trên Ngày'
                                    value={carDetail && carDetail.price}
                                    disabled
                                    type='number'
                                />
                            </Grid>

                            <Grid item xs={12} sm={7}>
                                <TextField
                                    fullWidth size='small'
                                    variant='outlined'
                                    label='Phí Bảo Hiểm'
                                    value={carDetail && carDetail.insuranceFee}
                                    disabled
                                />
                            </Grid>
                        </Grid>
                    </Box>

                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' type='datetime-local' variant='outlined' label='Ngày đăng kí nhận xe' value={data && data.startDate} disabled />
                    </Box >
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' type='datetime-local' variant='outlined' label='Ngày đăng kí trả xe' value={data && data.endDate} disabled />
                    </Box >
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Tổng tiền' value={data && data.money} disabled />
                    </Box >
                    <Box className={classes.inputText}>
                        <Box>Ghi Chú</Box>
                        <textarea rows={5} value={note} className={classes.textarea} onChange={(e) => setNote(e.target.value)} disabled={type === 'complete' || type === 'refuse'} />
                    </Box >
                </Box>
                {
                    type !== 'refuse' && type !== 'complete'
                        ?

                        <Box className={classes.button}>
                            <Button className={classes.buttonLeft} color='primary' variant='contained'
                                onClick={hanldeSuccess}
                            >{buttonSuccess}</Button>
                            <Button color='#414141' variant='contained' onClick={hanldeRefuse}> {buttonRefuse} </Button>
                        </Box>
                        :
                        <Box className={classes.button}>
                            <Button color='#414141' variant='contained' onClick={closeContract}> Đóng </Button>
                        </Box>
                }

            </Box>
        </Dialog>
    );
}

export default DialogContract;


const useStyles = makeStyles((themes) => ({
    container: {
        maxWidth: 800,
        // padding:30
    },
    dialogHead: {
        width: 550,
        textAlign: 'center',
        padding: '10px 0',
        fontWeight: 'bold',
        fontSize: 20,
        borderBottom: '1px solid #000'
    }, inputText: {
        paddingTop: 20,
        width: 400,
        margin: '0 auto'
    }, button: {
        padding: "15px 50px",
        display: 'flex',
        justifyContent: 'flex-end'
    }, buttonLeft: {
        marginRight: 15
    }, textarea: {
        width: '100%',
        maxWidth: '100%',
        minWidth: '100%',
    }, iconbtn: {
        position: 'relative',
        left: 130,
        top: -10,
        fontSize: 16
    }
}))