import React from 'react'
import { Box, makeStyles } from '@material-ui/core'
// import loadingGif from '../../../global/assets/loadingFullScreen.gif'
import loadingGif from './7jqO.gif'

export default function FullScreenLoading() {
    const classes = useStyles()
    return (
        <>
            <Box className={classes.loadingWrapper}>
                <img src={loadingGif} className={classes.loadingGif} alt='loading . . .' />
            </Box>
        </>
    )
}

const useStyles = makeStyles(theme => ({
    loadingWrapper: {
        position: 'fixed',
        zIndex: 1000,
        top: 0,
        left: 0,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100vh',
        width: '100%',
        backgroundColor: '#0d0121',
        opacity: .7
    },
    copyright: {
        position: 'absolute',
        width: "40%",
        textAlign: "center",
        left: "30%",
        top: "90%",
        color: '#fafafa',
        zIndex: 1001,
    },
    boxCopyright: {
        // position:'relative',
        width: "100%"
    },
    loadingGif: {
        maxWidth: 700
    }
}))