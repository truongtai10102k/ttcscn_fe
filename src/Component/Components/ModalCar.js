import React, { useState, useEffect } from 'react';
import {
    Box, Dialog, Button,
    makeStyles, TextField,
    IconButton, Grid
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CloseIcon from '@material-ui/icons/Close';
import TinyEditor from '../Components/TinyEditor'
import AlbumPicker from '../Components/Picker/AlbumPicker'
import { defaultRules } from '../../constants'
const ModalCar = (props) => {

    const listFuel = [{ title: 'Xăng' }, { title: 'Dầu' }, { title: 'Điện' },]
    const { open, close, data, type, listCompany, postCar, putCar } = props
    const [Id, setId] = useState('');
    const [company, setCompany] = useState('');
    const [nameCompany, setNameCompany] = useState('');
    const [name, setName] = useState('');
    const [manufacture, setManufacture] = useState('');
    const [licensePlate, setLicensePlate] = useState('');
    const [seat, setSeat] = useState('');
    const [frameNumber, setFrameNumber] = useState('');
    const [fuel, setFuel] = useState('');
    const [attrition, setAttrition] = useState('');
    const [price, setPrice] = useState('');
    const [insuranceFee, setInsuranceFee] = useState('');
    const [description, setDescription] = useState('');
    const [utilitie, setUtilitie] = useState('');
    const [rule, setRule] = useState(defaultRules);
    const [image, setImage] = useState([]);
    const classes = useStyles()

    const handChange = (e, setState) => {
        setState(e.target.value)
    }
    useEffect(() => {
        if (type === 'add') {
            setCompany(listCompany[0])
            setNameCompany(listCompany[0].name)
            setManufacture('')
            setLicensePlate('')
            setName('')
            setSeat('')
            setFrameNumber('')
            setFuel(listFuel[0])
            setAttrition('')
            setPrice('')
            setInsuranceFee('')
            setDescription('')
            setUtilitie('')
            setRule(defaultRules)
        } else {
            console.log("hihihihi adatadatadata ", data);
            setId(data?.id)
            setNameCompany(data?.name_car_company)
            setName(data?.name)
            setManufacture(data?.manufacture)
            setLicensePlate(data?.licensePlate)
            setSeat(data?.seat)
            setFrameNumber(data?.frameNumber)
            setFuel({ title: data?.fuel })
            setAttrition(data?.attrition)
            setPrice(data?.price)
            setInsuranceFee(data?.insuranceFee)
            setDescription(data?.description)
            setUtilitie(data?.utilitie)
            setRule(data?.rule)
            setImage(data?.imageUrl)
            // setCompany(listCompany.filter(item => item.name === data.company.name)[0])
        }
    }, [data]);

    const handleSubmit = () => {
        const data = {
            id_car_company: company.id,
            name_car_company: nameCompany,
            name,
            manufacture,
            licensePlate,
            seat,
            frameNumber,
            fuel: fuel.title,
            attrition,
            price,
            insuranceFee,
            description,
            utilitie,
            rule,
            imageUrl: image
        }
        if (type === 'edit') {
            data.id = Id
        }
        if (type === 'add') {
            postCar(data)
        } else {
            putCar({ data })
        }

        close()
    }
    return (
        <Dialog open={open} onClose={close}>
            <Box className={classes.container} >
                <Box className={classes.dialogHead}> {type !== 'add' ? 'Thông Tin Xe' : 'Thêm Xe'} <IconButton className={classes.iconbtn} onClick={close}><CloseIcon /></IconButton> </Box>
                <Box>
                    <Box className={classes.inputText}>
                        <Autocomplete
                            disabled={type === 'view'}
                            fullWidth
                            className="input-filter"
                            size="small"
                            value={{ name: nameCompany }}
                            options={listCompany}
                            getOptionLabel={(option) => option.name}
                            renderInput={(params) => <TextField {...params}
                                placeholder="Hãng xe"
                                variant="outlined"
                            />}
                            onChange={(e, value) => {
                                setNameCompany(value ? value.name : '')
                                setCompany(value)
                            }}
                        />
                    </Box>
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Tên Xe' value={name}
                            disabled={type === 'view'}
                            onChange={(e) => handChange(e, setName)}
                        />
                    </Box>
                    <Box className={classes.inputText}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={5}>
                                <TextField
                                    fullWidth size='small'
                                    variant='outlined'
                                    label='Năm sản xuất'
                                    value={manufacture}
                                    disabled={type === 'view'}
                                    type='number'
                                    onChange={(e) => handChange(e, setManufacture)}
                                />
                            </Grid>
                            <Grid item xs={12} sm={7}>
                                <TextField
                                    fullWidth size='small'
                                    variant='outlined'
                                    disabled={type === 'view'}
                                    label='Biển Số Xe'
                                    value={licensePlate}
                                    onChange={(e) => handChange(e, setLicensePlate)}
                                />
                            </Grid>
                        </Grid>
                    </Box>
                    <Box className={classes.inputText}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={5}>
                                <TextField
                                    fullWidth size='small'
                                    variant='outlined'
                                    label='Số Chỗ Ngồi'
                                    disabled={type === 'view'}
                                    value={seat}
                                    type='number'
                                    onChange={(e) => handChange(e, setSeat)}

                                />
                            </Grid>
                            <Grid item xs={12} sm={7}>
                                <TextField
                                    fullWidth size='small'
                                    variant='outlined'
                                    disabled={type === 'view'}
                                    label='Số khung'
                                    value={frameNumber}
                                    onChange={(e) => handChange(e, setFrameNumber)}

                                />
                            </Grid>
                        </Grid>
                    </Box>
                    <Box className={classes.inputText}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={5}>
                                <Autocomplete
                                    fullWidth
                                    className="input-filter"
                                    size="small"
                                    value={fuel}
                                    disabled={type === 'view'}
                                    options={listFuel}
                                    getOptionLabel={(option) => option.title}
                                    renderInput={(params) => <TextField {...params}
                                        placeholder="Nhiên Liệu"
                                        variant="outlined"
                                    />}
                                    onChange={(e, value) => {
                                        setFuel(value)
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12} sm={7}>
                                <TextField
                                    fullWidth size='small'
                                    variant='outlined'
                                    disabled={type === 'view'}
                                    label='Tiêu Hao Nhiên Liệu/100km'
                                    value={attrition}
                                    onChange={(e) => handChange(e, setAttrition)}

                                />
                            </Grid>
                        </Grid>

                        <Box className={classes.inputText}>
                            <Grid container spacing={2}>
                                <Grid item xs={12} sm={5}>
                                    <TextField
                                        fullWidth size='small'
                                        variant='outlined'
                                        label='Giá Thuê Trên Ngày'
                                        disabled={type === 'view'}
                                        value={price}
                                        onChange={(e) => handChange(e, setPrice)}

                                        type='number'
                                    />
                                </Grid>

                                <Grid item xs={12} sm={7}>
                                    <TextField
                                        fullWidth size='small'
                                        variant='outlined'
                                        label='Phí Bảo Hiểm'
                                        disabled={type === 'view'}
                                        value={insuranceFee}
                                        onChange={(e) => handChange(e, setInsuranceFee)}

                                    />
                                </Grid>
                            </Grid>
                        </Box >
                        <Box className={classes.inputText}>
                            <Box>Mô Tả</Box>
                            <textarea rows={5} className={classes.textarea} value={description}
                                disabled={type === 'view'}
                                onChange={(e) => handChange(e, setDescription)}
                            />
                        </Box >
                        <Box className={classes.inputText}>
                            <Box>Các Tiện Ích Trên Xe</Box>
                            <textarea rows={3} className={classes.textarea} value={utilitie}
                                disabled={type === 'view'}
                                onChange={(e) => handChange(e, setUtilitie)}

                            />
                        </Box >
                        <Box className={classes.inputText}>
                            <Box>Điều Khoản Thuê Xe</Box>
                            <textarea rows={10} className={classes.textarea} value={rule}
                                disabled={type === 'view'}
                                onChange={(e) => handChange(e, setRule)}

                            />
                        </Box >
                        <Box className={classes.inputText}>
                            <Box>Ảnh xe</Box>
                            <Box className={classes.albumPicker}>
                                <AlbumPicker
                                    height={250}
                                    width={250}
                                    setListImage={(data) => setImage(data)}
                                    listSrc={image}
                                    type={type}
                                />
                            </Box>
                        </Box >
                    </Box>


                </Box>
                <Box className={classes.button}>
                    {
                        type !== 'view'
                            ?
                            <Button className={classes.buttonLeft} color='primary' variant='contained'
                                onClick={handleSubmit}
                            >Đồng Ý</Button>
                            :
                            <></>
                    }

                    <Button color='#414141' variant='contained' onClick={close}>Hủy</Button>
                </Box>
            </Box>
        </Dialog >
    );
}

export default ModalCar;


const useStyles = makeStyles((themes) => ({
    container: {
        maxWidth: 800,
        // padding:30
    },
    dialogHead: {
        width: 550,
        textAlign: 'center',
        padding: '10px 0',
        fontWeight: 'bold',
        fontSize: 20,
        borderBottom: '1px solid #000'
    }, inputText: {
        paddingTop: 15,
        width: 400,
        margin: '0 auto'
    }, button: {
        padding: "15px 50px",
        display: 'flex',
        justifyContent: 'flex-end'
    }, buttonLeft: {
        marginRight: 15
    }, textarea: {
        width: '100%',
        maxWidth: '100%',
        minWidth: '100%',
    }, iconbtn: {
        position: 'relative',
        left: 130,
        top: -10,
        fontSize: 16
    }, albumPicker: {
        display: 'flex',
        justifyContent: 'center'
    }
}))