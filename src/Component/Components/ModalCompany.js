import React, { useState, useEffect } from 'react';
import { Dialog, Box, TextField, makeStyles, Button, } from '@material-ui/core'
import ImagePicker from '../Components/Picker/ImagePicker'
const ModalCompany = (props) => {
    const { open, close, type, data, postCompany, putCompany } = props
    const [nameCompany, setNameCompany] = useState('');
    const [image, setImage] = useState('');
    const classes = useStyles()

    useEffect(() => {
        if (data && data.name) {
            setNameCompany(data.name)
        }
        if (data && data.imageUrl) {
            setImage(data.imageUrl)
        }
    }, [data]);
    useEffect(() => {
        if (type && type === 'add') {
            setNameCompany('')
            setImage('')
        }
    }, [type]);

    const handleChangeCompany = (e) => {
        setNameCompany(e.target.value)
    }
    const handleSetImage = (data) => {
        setImage(data)
    }

    const handlePostCompany = () => {
        postCompany({ name: nameCompany, imageUrl: image })
        close()
    }

    const handlePutCompany = () => {
        putCompany({ id: data.id, name: nameCompany, imageUrl: image })
        close()
    }

    return (
        <Dialog open={open} onClose={close}>
            <Box className={classes.container}>
                <Box className={classes.title}>{type !== '' && type === 'add' ? 'Thêm Hãng Xe' : 'Sửa Hãng Xe'}</Box>
                <Box className={classes.input}>
                    <TextField
                        label='Tên Công Ty'
                        variant='outlined'
                        fullWidth
                        autoFocus
                        value={nameCompany}
                        onChange={handleChangeCompany}
                    />
                </Box>
                <Box className={classes.image}>
                    <Box className={classes.text}> Ảnh Đại Diện </Box>
                    <ImagePicker
                        setImageUpload={handleSetImage}
                        imageSrc={data && data.imgURL ? data.imgURL : ''}
                        readonly={type === 'view' ? true : false}
                        imageSrc={image}
                        type={type}
                    />
                </Box>
                {
                    type !== 'view'
                        ?
                        <Box className={classes.buttonGroup}>
                            <Button variant='contained' color='primary'
                                onClick={() => {
                                    if (type === 'add') {
                                        handlePostCompany()
                                    } else {
                                        handlePutCompany()
                                    }
                                }
                                }

                            >Xác Nhận</Button>
                            <Button className={classes.buttonItem} color='#414141' variant='contained'
                                onClick={close}
                            >Hủy</Button>
                        </Box>
                        :
                        <></>
                }
            </Box>
        </Dialog>
    );
}

export default ModalCompany;

const useStyles = makeStyles((themes) => ({
    container: {
        width: 400,
        height: 480
    }, title: {
        fontSize: 18,
        fontWeight: 600,
        textAlign: 'center',
        padding: '15px 0',
        borderBottom: '1px solid #000'
    }, input: {
        marginTop: 20,
        padding: '0 20px'
    },
    buttonGroup: {
        display: 'flex',
        justifyContent: 'space-evenly',
        marginTop: 30
    }, buttonItem: {
        padding: '0 40px'
    }, image: {
        width: '100%',
        height: 'auto',
        display: 'flex',
        marginTop: '10px',
        flexDirection: 'column',
        alignItems: 'center'
    }, text: {
        padding: '10px 20px',
        fontSize: 18
    }
}))
