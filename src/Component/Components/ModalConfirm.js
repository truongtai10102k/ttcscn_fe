import React from 'react';
import {
    Box, Button, Dialog,
    makeStyles
} from '@material-ui/core';
const ModalConfirmLock = (props) => {
    const { open, close, deactiveAccount, data } = props;
    const classes = useStyles()
    return (
        <Dialog open={open} onClose={close}>
            <Box className={classes.container}>
                <Box className={classes.title}>Xác Nhận Khóa Tài Khoản </Box>
                <Box className={classes.buttonGroup}>
                    <Button variant='contained' color='primary' onClick={() => {
                        deactiveAccount(data.id)
                        close()
                    }}>Xác Nhận</Button>
                    <Button className={classes.buttonItem} color='#414141' variant='contained'
                        onClick={close}
                    >Hủy</Button>
                </Box>
            </Box>
        </Dialog>
    );
}
const ModalConfirmDelete = (props) => {
    const { open, close, data, deleteCompany } = props;
    const classes = useStyles()
    return (
        <Dialog open={open} onClose={close}>
            <Box className={classes.container}>
                <Box className={classes.title}>Xác Nhận Xóa</Box>

                <Box className={classes.buttonGroup}>
                    <Button variant='contained' color='primary'
                        onClick={() => {
                            deleteCompany(data.id)
                            close()
                        }}
                    >Xác Nhận</Button>
                    <Button className={classes.buttonItem} color='#414141' variant='contained'
                        onClick={close}
                    >Hủy</Button>
                </Box>
            </Box>
        </Dialog>
    );
}

const ModalConfirmUnlock = (props) => {
    const { open, close, activeAccount, data } = props
    const classes = useStyles()
    return (
        <Dialog open={open} onClose={close}>
            <Box className={classes.container}>
                <Box className={classes.title}>Xác Nhận Mở Khóa Tài Khoản </Box>
                <Box className={classes.buttonGroup}>
                    <Button variant='contained' color='primary'
                        onClick={() => {
                            activeAccount(data.id)
                            close()
                        }}
                    >Xác Nhận</Button>
                    <Button className={classes.buttonItem} color='#414141' variant='contained'
                        onClick={close}
                    >Hủy</Button>
                </Box>
            </Box>

        </Dialog>
    );
}
const ModalConfirmDeleteCar = (props) => {
    const { open, close, deleteCar, data } = props
    const classes = useStyles()
    return (
        <Dialog open={open} onClose={close}>
            <Box className={classes.container}>
                <Box className={classes.title}  style={{ paddingBottom: 10 }} >Xác Nhận Xóa Xe</Box>
                <Box padding='0 20px'>
                    <Box component='span'>Lưu ý : </Box>
                    <Box component='span' style={{
                        color: 'red',
                        fontSize: '12px',
                        textAlign: 'center'
                    }}>Nếu bạn xóa xe thì toàn bộ hợp đồng của xe đều bị xóa</Box>
                </Box>
                <Box className={classes.buttonGroup}>
                    <Button variant='contained' color='primary'
                        onClick={() => {
                            deleteCar(data.id)
                            close()
                        }}
                    >Xác Nhận</Button>
                    <Button className={classes.buttonItem} color='#414141' variant='contained'
                        onClick={close}
                    >Hủy</Button>
                </Box>
            </Box>

        </Dialog>
    );
}

export {
    ModalConfirmLock,
    ModalConfirmUnlock,
    ModalConfirmDelete,
    ModalConfirmDeleteCar
};

const useStyles = makeStyles((themes) => ({
    container: {
        width: 350,
        height: 200
    }, title: {
        fontSize: 18,
        fontWeight: 600,
        textAlign: 'center',
        padding: '40px 0'
    },
    buttonGroup: {
        display: 'flex',
        justifyContent: 'space-evenly',
        marginTop: 20
    }, buttonItem: {
        padding: '0 40px'
    }
}))