import React, { useState, useRef, useCallback, useEffect } from 'react';
import { useDropzone } from 'react-dropzone'
import {
    Box,
    makeStyles,
    ImageList,
    ImageListItem,
    ImageListItemBar,
    IconButton,
    Button
} from '@material-ui/core';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import { DOMAIN_GET_IMAGE } from "../../../FetchAPI/callAPIConfig"
import Loading from '../../../image/loadingImage.gif'
import callAPI from './CallAPIImage';
const ImagePicker = (props) => {
    const { setListImage, width, height, listSrc, readonly, type } = props
    const classes = useStyles();
    const [listImageURL, setListImageURL] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const onDrop = useCallback((async (acceptedFiles) => {
        setIsLoading(true)

        acceptedFiles.map((acceptedFile) => {
            // fileData.append("data", acceptedFile);
            var reader = new FileReader();
            var url = reader.readAsDataURL(acceptedFile);
            reader.onloadend = function (e) {
                // console.log("1",reader.result);
                setListImageURL(state => [...state, reader.result])
            }.bind(this);
        })


        let fileData = new FormData();
        for (let i = 0; i < acceptedFiles.length; i++) {
            fileData.append("File", acceptedFiles[i]);
        }
        let result = await callAPI(fileData);
        if (result.data) {
            setListImage(result.data)
        } else {
            alert("upload ảnh lỗi ")
        }
        setIsLoading(false)
    }), [])

    useEffect(() => {
        if (listSrc && type !== 'add') {
            let arrImg = listSrc.trim().split('@@')
            let newArr = arrImg.filter((item, idx) => { return idx > 0 })
            setListImageURL(newArr)
        }
    }, [listSrc]);
    const { getRootProps, getInputProps, open, isDragActive } = useDropzone({ onDrop })

    return (
        <Box className={classes.imageContainer}>
            {
                isLoading
                    ?
                    <img
                        src={Loading}
                        className={classes.listImg}
                        style={{ height: height && height, width: width && width }}
                    />
                    :
                    <Box>
                        <Box
                            className={classes.imageBox}
                            style={{ height: height && height, width: width && width }}
                            {...getRootProps()}

                            style={{ height: height && height, width: width && width }}
                        >
                            <input {...getInputProps()} type={"file"} disabled={readonly && readonly === true ? readonly : false} />
                            {
                                isDragActive ?
                                    <Box className={classes.textImage}>Thả ảnh vào đây ...</Box> :
                                    <Box className={classes.textImage}>Click hoặc kéo thả ảnh</Box>
                            }
                        </Box>
                        <Box
                            className={classes.root}
                            style={{ height: height && height, width: width && width }}
                        >
                            <ImageList className={classes.imageList} cols={1} >
                                {listImageURL && listImageURL.map((item, idx) => (
                                    <ImageListItem key={idx} className={classes.imageItem} >

                                        <img onClick={open} className={classes.listImg} src={item && item.includes('data:image/') ? item : DOMAIN_GET_IMAGE + item} />
                                        {
                                            readonly === true
                                                ?
                                                <></>
                                                :
                                                <ImageListItemBar
                                                    classes={{
                                                        root: classes.titleBar,
                                                        title: classes.title,
                                                    }}

                                                    actionIcon={
                                                        readonly === true
                                                            ?
                                                            <IconButton onClick={() => {
                                                                let newArrayURL = listImageURL.filter((value, index) => {
                                                                    return index !== idx
                                                                })
                                                                setListImageURL(newArrayURL)
                                                            }}>

                                                                <DeleteOutline className={classes.iconDelete} />
                                                            </IconButton>
                                                            :
                                                            <></>
                                                    }
                                                />
                                        }

                                    </ImageListItem>
                                ))}
                            </ImageList>
                        </Box>
                    </Box>
            }
        </Box>
    );
}

const useStyles = makeStyles(theme => ({
    imageContainer: {
        position: "relative"
    }, imageBox: {
        width: "300px",
        height: "300px",
        border: "1px solid #000",
        position: "absolute",
        top: 0,
        cursor: "pointer",
        left: 0,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    textImage: {
        textAlign: "center",
        fontWeight: 400,
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
        width: "300px",
        height: "300px",
        alignItems: 'center',
        "&::-webkit-scrollbar": {
            width: "5px",
        }
    },
    imageList: {
        alignItems: "center",
        flexWrap: 'nowrap',
        transform: 'translateZ(0)',
        "&::-webkit-scrollbar": {
            height: "10px",
        },
        "&::-webkit-scrollbar-track": {
            background: "#f1f1f1",
        },
        "&::-webkit-scrollbar-thumb": {
            background: "#888",
            borderRadius: "10px",
        }
    },
    iconDelete: {
        color: "#fff",
    },
    titleBar: {
        background:
            'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    }, listImg: {
        objectFit: 'cover'
    }
}))


export default ImagePicker;
