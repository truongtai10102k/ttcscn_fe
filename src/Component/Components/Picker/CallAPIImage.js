import { DOMAIN_IMAGE } from "../../../FetchAPI/callAPIConfig"
export default function callAPI(data) {
    return new Promise((resolve, reject) => {
        const url = DOMAIN_IMAGE
        fetch(url, {
            method: 'POST',
            body: data,
        })
            .then((response) => response.json().then((data) => {
                resolve(data)
            }))
            .catch((error) => reject(error));
    });
}
