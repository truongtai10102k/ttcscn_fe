import React, { useState, useEffect, useCallback } from 'react';
import { useDropzone } from 'react-dropzone'
import { Box, makeStyles, } from '@material-ui/core';
import { DOMAIN_IMAGE, DOMAIN_GET_IMAGE } from "../../../FetchAPI/callAPIConfig"
import Loading from '../../../image/loadingImage.gif'
import callAPI from './CallAPIImage';

const ImagePicker = (props) => {

    // truyen setImage để lấy thông tin ảnh
    // width, height is width and height of image

    const { setImageUpload, width, height, readonly, imageSrc, type } = props
    const classes = useStyles();
    const [isLoading, setIsLoading] = useState(false);
    const [ImageURL, setImageURL] = useState(imageSrc && imageSrc);

    const onDrop = useCallback((async (acceptedFiles) => {
        setIsLoading(true)
        var reader = new FileReader();
        reader.readAsDataURL(acceptedFiles[0]);
        reader.onloadend = function (e) {
            setImageURL(reader.result)
        }.bind(this);
        let fileData = new FormData();
        fileData.append("File", acceptedFiles[0]);
        let result = await callAPI(fileData)
        let url = await result.data.trim().split('@@')
        setImageUpload(url[1])

        setIsLoading(false)
    }), [])
    const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop })
    return (
        <Box className={classes.imageContainer}>
            {
                isLoading
                    ?
                    <img
                        src={Loading}
                        className={classes.listImg}
                        style={{ height: height && height, width: width && width }}
                    />
                    :
                    <Box>
                        <Box
                            className={classes.imageBox}
                            {...getRootProps()}
                            style={{
                                zIndex: ImageURL ? "1" : "2",
                                height: height && height,
                                width: width && width
                            }}
                        >
                            <input {...getInputProps()} type={"file"} disabled={readonly && readonly ? readonly : false} />
                            {
                                isDragActive ?
                                    <Box className={classes.textImage}>Thả ảnh vào đây ...</Box> :
                                    <Box className={classes.textImage}>Click hoặc kéo thả ảnh</Box>
                            }
                        </Box>

                        <img className={classes.img} src={ImageURL && ImageURL.includes('data:image/') ? ImageURL : DOMAIN_GET_IMAGE + ImageURL} style={{
                            zIndex: ImageURL ? "2" : "1",
                            height: height && height,
                            width: width && width
                        }} />
                    </Box>
            }
        </Box>
    );
}

const useStyles = makeStyles(theme => ({
    imageBox: {
        width: "200px",
        height: "200px",
        border: "1px solid #000",
        position: "absolute",
        top: 0,
        cursor: "pointer",
        left: 0,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    }, img: {
        width: "200px",
        height: "200px",
        border: "1px solid #000",
        objectFit: "cover",
        position: "relative",
        top: 0,
        left: 0,
        pointerEvents: "none"
    },
    textImage: {
        textAlign: "center",
        fontWeight: 400,
    }, imageContainer: {
        position: "relative"
    }
}))


export default ImagePicker;
