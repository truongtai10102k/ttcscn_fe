import React, { useState } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import CreateAccountModal from './CreateAccountModal';
import {
    IconButton, Table,
    TableBody, TableContainer,
    TableCell, TableHead,
    TableRow, Paper, Box,
    TextField, Button, Dialog
} from '@material-ui/core'
import {
    Visibility as ViewIcon,
    LockOutlined as LockIcon,
    LockOpenOutlined as OpenIcon
} from "@material-ui/icons"
import { ModalConfirmLock, ModalConfirmUnlock } from './ModalConfirm';
import Pagination from '@material-ui/lab/Pagination'
import LocalStorage from '../../localStorage';


const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    }
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        }, '& > *': {
            marginTop: theme.spacing(2),
        },
    },
}))(TableRow);

function createData(id, userName, customerName, phone, address, status) {
    return { id, userName, customerName, phone, address, status };
}
const rows = [
    createData('id_001', "userUser", "Nguyễn Văn A", '0123456789', 'Hưng Thịnh,Hưng Nguyên,Nghệ An', true),
    createData('id_002', "userUser", "Nguyễn Văn A", '0123456789', 'Hưng Thịnh,Hưng Nguyên,Nghệ An', true),
    createData('id_003', "userUser", "Nguyễn Văn A", '0123456789', 'Hưng Thịnh,Hưng Nguyên,Nghệ An', false),
    createData('id_004', "userUser", "Nguyễn Văn A", '0123456789', 'Hưng Thịnh,Hưng Nguyên,Nghệ An', true),
    createData('id_005', "userUser", "Nguyễn Văn A", '0123456789', 'Hưng Thịnh,Hưng Nguyên,Nghệ An', false),
    createData('id_006', "userUser", "Nguyễn Văn A", '0123456789', 'Hưng Thịnh,Hưng Nguyên,Nghệ An', true),
    createData('id_007', "userUser", "Nguyễn Văn A", '0123456789', 'Hưng Thịnh,Hưng Nguyên,Nghệ An', true),
    createData('id_008', "userUser", "Nguyễn Văn A", '0123456789', 'Hưng Thịnh,Hưng Nguyên,Nghệ An', false),
    createData('id_009', "userUser", "Nguyễn Văn A", '0123456789', 'Hưng Thịnh,Hưng Nguyên,Nghệ An', true),
];

const useStyles = makeStyles({
    tableContainer: {
        maxHeight: "72vh"
    },
    table: {
        minWidth: 700,

    },
    tableHead: {
        backgroundColor: "#43a047",
        borderRight: "1px solid #fff"

    },
    tableCell: {
        borderRight: "1px solid #43a047a6",
    }, pagination: {
        display: "flex",
        justifyContent: 'center',
        marginTop: '15px'
    }, searchBox: {
        display: 'flex',
        paddingBottom: 15,
        width: '100%',
        justifyContent: 'space-between'
    }, inputSearch: {
        maxWidth: 350,
        marginRight: 20
    }, resetButton: {
        // padding: '6px 0'
        marginRight: '50px'
    }, searchGroup: {
        maxWidth: '50%',
        display: 'flex'
    }, btnbtn: {
        width: '150px'
    }, newButton: {
        marginRight: '30px',
        // padding: '6px 0'
    }
});

export default function CustomizedTables(props) {
    const {
        type, activeAccount, deactiveAccount,
        putAccount, listData, postAccountAdmin,
        activePage, totalPage, tx, getAccount
    } = props
    const [openLock, setOpenLock] = useState(false);
    const [openUnlock, setOpenUnlock] = useState(false);
    const [openRegester, setOpenRegester] = useState(false);
    const [openResetPassword, setOpenResetPassword] = useState(false);
    const [data, setData] = useState({});
    const [textSearch, setTextSearch] = useState('');
    const classes = useStyles();

    const hanldeCloseLock = () => {
        setOpenLock(false)
    }
    const hanldeCloseUnlock = () => {
        setOpenUnlock(false)
    }
    const hanldeCloseDialog = () => {
        setOpenResetPassword(false)
    }
    const hanldeCloseRegester = () => {
        setOpenRegester(false)
    }

    return (
        <Box>
            <Box className={classes.searchBox}>
                <Box className={classes.searchGroup}>
                    <TextField
                        fullWidth
                        className={classes.inputSearch}
                        label='Tìm Kiếm ...'
                        size="small" variant='outlined'
                        onChange={(e) => setTextSearch(e.target.value)}
                    />
                    <Button
                        className={classes.btnbtn}
                        variant="contained"
                        color="primary"
                        onClick={() => { getAccount({ page: 1, role: type, textSearch: textSearch }) }}
                    >Tìm Kiếm</Button>
                </Box>
                {
                    type === 'admin'
                        ?
                        <Box  >
                            <Button className={classes.newButton} variant="contained" color="secondary"
                                onClick={() => setOpenRegester(true)}
                            >Tạo Mới</Button>
                            <Button className={classes.resetButton} variant="contained" color="secondary"
                                onClick={() => setOpenResetPassword(true)}
                            >Đổi Mật Khẩu</Button>
                        </Box>
                        :
                        <></>
                }
            </Box>
            <TableContainer className={classes.tableContainer} component={Paper}>
                <Table stickyHeader className={classes.table}>
                    <TableHead className={classes.head} >
                        <TableRow>
                            <StyledTableCell className={classes.tableHead} align="center" width="5%" >STT</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="15%">Tên Đăng Nhâp</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="15%">Tên Khách Hàng</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="15%">SĐT</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="15%">Email</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="15%">Trạng Thái</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="20%">Hành Động</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {listData && listData.map((row, index) => (
                            <StyledTableRow key={index} onClick={() => {
                                // alert(row.name)

                            }}>
                                <StyledTableCell component="th" scope="row" className={classes.tableCell} width="5%" align="center">
                                    {index + 1}
                                </StyledTableCell>
                                <StyledTableCell className={classes.tableCell} align="center" width="15%" >{row.userName}</StyledTableCell>
                                <StyledTableCell className={classes.tableCell} align="center" width="15%">{row.CustomerName}</StyledTableCell>
                                <StyledTableCell className={classes.tableCell} align="center" width="15%">{row.phone}</StyledTableCell>
                                <StyledTableCell className={classes.tableCell} align="center" width="15%">{row.email}</StyledTableCell>
                                <StyledTableCell className={classes.tableCell} align="center" width="15%"> {row.status === 'active' ? 'Đang hoạt động ' : "Tạm Khóa"}</StyledTableCell>
                                <StyledTableCell align="center" width="20%">
                                    {
                                        row.status === 'active'
                                            ?
                                            <IconButton color="primary" component="span" onClick={() => {
                                                setOpenLock(true)
                                                setData(row)
                                            }} >
                                                <OpenIcon fontSize="medium" />
                                            </IconButton>
                                            :
                                            <IconButton color="primary" component="span" onClick={() => {
                                                setOpenUnlock(true)
                                                setData(row)
                                            }}>
                                                <LockIcon />
                                            </IconButton>
                                    }
                                </StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <Pagination
                className={classes.pagination}
                color="secondary"
                count={totalPage}
                page={activePage}
                onChange={(e, number) => {
                    getAccount({ textSearch: tx, role: type, page: number })
                }} />
            <ModalConfirmLock open={openLock} close={hanldeCloseLock} deactiveAccount={deactiveAccount} data={data} />
            <ModalConfirmUnlock open={openUnlock} close={hanldeCloseUnlock} activeAccount={activeAccount} data={data} />
            <DialogResetPassword open={openResetPassword} close={hanldeCloseDialog} putAccount={putAccount} />
            <CreateAccountModal postAccountAdmin={postAccountAdmin} open={openRegester} close={hanldeCloseRegester} />
        </Box >
    );
}

const DialogResetPassword = (props) => {
    const { open, close, putAccount } = props

    const [oldPassword, setOldPassword] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    const handleChange = (e, setState) => {
        setState(e.target.value)
    }

    const handleSubmit = () => {
        if (password.length < 6 || oldPassword.length < 6) {
            alert('Mật khẩu dài hơn 6 kí tự');
            return;
        }
        if (password === oldPassword) {
            alert('Mật khẩu cũ phải khác mật khẩu mới')
            return;
        }
        if (password !== confirmPassword) {
            alert("Mật khẩu không khớp")
            return;
        } else {
            const user = LocalStorage.getToken();
            putAccount({ userName: user.user.userName, password, currentPassword: oldPassword })
            close()
        }

    }

    const classes = useStylesDialog()
    return (
        <Dialog open={open} onClose={close}>
            <Box className={classes.contaier}>
                <Box className={classes.text}> Thay Đổi Mật Khẩu</Box>
                <Box className={classes.listInput}>
                    <TextField type='password' size='small' fullWidth variant='outlined' label='Nhập Mật Khẩu Cũ' onChange={(e) => handleChange(e, setOldPassword)} />
                    <TextField type='password' className={classes.input} size='small' fullWidth variant='outlined' label='Nhập Mật Khẩu Mới' onChange={(e) => handleChange(e, setPassword)} />
                    <TextField type='password' className={classes.input} size='small' fullWidth variant='outlined' label='Nhập Lại Mật Khẩu Mới' onChange={(e) => handleChange(e, setConfirmPassword)} />
                </Box>
                <Box className={classes.buttonGroup}>
                    <Button variant='contained' color='primary'
                        onClick={handleSubmit}
                    >Xác Nhận</Button>
                    <Button className={classes.buttonItem} color='#414141' variant='contained'
                        onClick={close}
                    >Hủy</Button>
                </Box>
            </Box>
        </Dialog>
    )
}

const useStylesDialog = makeStyles({
    contaier: {
        width: 400,
        height: 330
    }, text: {
        padding: '20px 0',
        textAlign: 'center',
        fontSize: 18,
        fontWeight: 600,
        borderBottom: '1px solid #000'
    }, listInput: {
        padding: '20px 20px'
    },
    input: {
        margin: '10px 0'
    },
    buttonGroup: {
        display: 'flex',
        justifyContent: 'space-evenly',
    },
});