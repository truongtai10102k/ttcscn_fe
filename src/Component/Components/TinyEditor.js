import React, { useRef,useState, useEffect } from 'react';
import { Editor } from '@tinymce/tinymce-react';
import { Button, Box } from "@material-ui/core"
const TinyComponent = (props) => {
    // onlyView is set mode view. if true => text editor only view, cannot edit text
    // setText is state use set text from props
    // text is initialValue in text editor
    const { onlyView, setText, text, width, height } = props;
    // url file upload to image server
    const uploadImageURL = ""
    const editorRef = useRef(null);
    const handleSetText = () => {
        if (editorRef.current) {
            setTextEditor(editorRef.current.getContent());
        }
    };
    const [textEditor, setTextEditor] = useState('');

    const handleUploadImage = function (blobInfo, success, failure, progress) {
        var xhr, formData;

        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', { uploadImageURL })

        xhr.upload.onprogress = function (e) {
            progress(e.loaded / e.total * 100);
        };

        xhr.onload = function () {
            var json;

            if (xhr.status === 403) {
                failure('HTTP Error: ' + xhr.status, { remove: true });
                return;
            }

            if (xhr.status < 200 || xhr.status >= 300) {
                failure('HTTP Error: ' + xhr.status);
                return;
            }
            json = JSON.parse(xhr.responseText);

            if (!json || typeof json.location != 'string') {

                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }
            success(json.location);
        };

        xhr.onerror = function () {
            failure('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
        };

        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());

        xhr.send(formData);
    }

    return (
        <Box>

            <Editor
                apiKey={"3tln8xwxf5nbrw6ekyo6og56i42bf6jk8zr6nk9emuvdd0gm"}
                onInit={(evt, editor) => editorRef.current = editor}
                // initialValue={text}
                disabled={onlyView}
                value={textEditor}
                onChange={handleSetText}
                init={{

                    auto_focus: true,
                    height: height ? height : 350,
                    width: width ? width : 600,
                    menubar: false,
                    plugins: [
                        'advlist autolink lists link image charmap print preview anchor',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table paste code help wordcount',
                        'image',
                    ],
                    toolbar: 'undo redo image | formatselect | ' +
                        'bold italic underline forecolor backcolor  | alignleft aligncenter ' +
                        'alignright alignjustify | bullist numlist outdent indent | ' +
                        'removeformat |  help',
                    images_upload_handler: handleUploadImage,

                    content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
                }}
            />
        </Box>
    );
}

export default TinyComponent;
