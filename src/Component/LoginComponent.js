import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import * as colors from '@material-ui/core/colors';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import BackgoundImage from "../image/bg.jpg"
import Regester from "./RegesterComponent"
import ResetPassword from "./ResetPasswordComponent"
import localStorage from '../localStorage';
const useStyles = makeStyles((theme) => ({
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        maxWidth: "450px",
        height: "400px",
        margin: 'auto',
        marginTop: "8%",
        backgroundColor: '#fafafa',
        padding: "15px 40px 0",
        borderRadius: "10px",
        position: 'relative',
        zIndex: 2,
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        color: '#000',

    },
    submit: {
        margin: theme.spacing(3, "auto", 2),

    },
    Logo: {
        color: colors.green[900]
    },
    container: {
        backgroundImage: `url(${BackgoundImage})`,
        backgroundRepeat: "none",
        backgroundSize: "cover",
        width: '100%',
        height: '100%',
        top: 0,
        left: 0,
        position: 'absolute',
        padding: 0,
        '&::before': {
            content: '""',
            width: '100%',
            height: "100vh",
            position: 'absolute',
            zIndex: 1,
            top: 0,
            left: 0,
            backgroundColor: '#0000007d',
        },
        '&.MuiContainer-maxWidthLg': {
            maxWidth: '100%',
        }
    }
}));

function Login(props) {
    const classes = useStyles();
    const { setRegester, setResetPassword, loginDispatch } = props
    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");

    const handleUserName = (e) => {
        setUserName(e.target.value)
    }
    const handlePassword = (e) => {
        setPassword(e.target.value)
    }

    const handleClick = () => {
        if (userName && password) {
            loginDispatch({ userName, password })
        } else {
            alert("Vui Lòng Nhập Đủ Thông Tin")
        }
    }
    return (
        <Container component="main" className={classes.container}>
            <div className={classes.paper} >
                <Typography className={classes.Logo} component="h1" variant="h2">
                    Thuê Xe
                </Typography>

                <Box className={classes.form} noValidate>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="UserName"
                        name="email"
                        autoComplete="email"
                        autoFocus
                        onChange={handleUserName}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        onChange={handlePassword}

                    />
                    <Button
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={handleClick}
                    >
                        Đăng Nhập
                    </Button>
                    <Grid container>
                        <Grid item xs >
                            <Button   onClick={() => {
                                setResetPassword(true)
                            }} >
                                Quên Mật Khẩu
                            </Button>
                        </Grid>
                        <Grid item>

                            <Button   color="secondary" onClick={() => {
                                setRegester(true)
                            }}>
                                Đăng Kí
                            </Button>
                        </Grid>
                    </Grid>
                </Box>
            </div>
        </Container>
    );
}

export default (props) => {
    const { loginDispatch, postAccount, forgotPassword } = props
    const [regester, setRegester] = useState(false);
    const [resetPassword, setResetPassword] = useState(false);

    return (
        (!resetPassword && !regester)
            ?
            <Login
                loginDispatch={loginDispatch}
                setRegester={(bool) => { setRegester(bool) }}
                setResetPassword={(bool) => { setResetPassword(bool) }}
            />
            :
            (
                regester && !resetPassword
                    ?
                    <Regester postAccount={postAccount} open={regester} setRegester={(bool) => { setRegester(bool) }} />
                    :
                    <ResetPassword forgotPassword={forgotPassword} setResetPassword={(bool) => { setResetPassword(bool) }} />
            )
    )
}