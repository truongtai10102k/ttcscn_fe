import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import BackgoundImage from "../image/bg.jpg"
const useStyles = makeStyles((theme) => ({
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        maxWidth: "450px",
        height: "500px",
        margin: 'auto',
        marginTop: "3%",
        backgroundColor: '#fafafa',
        padding: "15px 40px 15px",
        borderRadius: "10px",
        position: 'relative',
        zIndex: 2,
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    }, container: {
        margin: 0,
        backgroundImage: `url(${BackgoundImage})`,
        backgroundRepeat: "none",
        backgroundSize: "cover",
        width: '100%',
        height: '100%',
        top: 0,
        left: 0,
        position: 'absolute',
        padding: 0,
        '&::before': {
            content: '""',
            width: '100%',
            height: "100vh",
            position: 'absolute',
            zIndex: 1,
            top: 0,
            left: 0,
            backgroundColor: '#0000007d',
        },
        '&.MuiContainer-maxWidthLg': {
            maxWidth: '100%',
        }
    }
}));

export default function SignUp(props) {
    const classes = useStyles();
    const { setRegester, postAccount } = props

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [phone, setPhone] = useState(0);
    const [email, setEmail] = useState('');

    function validateEmail(email) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    const handleSubmit = () => {
        if (!validateEmail(email)) {
            alert("vui lòng nhập đúng định dang mail")
            return;
        }
        if (password.length < 6) {
            alert('Mật khẩu dài hơn 6 kí tự')
            return;
        }
        if (confirmPassword !== password) {
            alert("Mật khẩu không khớp")
            return;
        }
        if (phone && firstName && lastName && password && confirmPassword && userName) {
            postAccount({ CustomerName: (firstName + lastName), userName, password, phone, email })
            setRegester(false)
        } else {
            alert('Vui lòng nhập đủ trưởng')
        }
    }


    const handleChange = (e, setState) => {
        setState(e.target.value)
    }

    return (
        <Container className={classes.container} component="main">
            <CssBaseline />
            <div className={classes.paper}>
                <Typography component="h1" variant="h5">
                    Đăng Kí Tài Khoản
                </Typography>
                <Box className={classes.form}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoComplete="fname"
                                name="firstName"
                                variant="outlined"
                                required
                                fullWidth
                                id="firstName"
                                label="First Name"
                                autoFocus
                                size="small"
                                onChange={(e) => {
                                    handleChange(e, setFirstName)
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="lastName"
                                label="Last Name"
                                name="lastName"
                                autoComplete="lname"
                                size="small"
                                onChange={(e) => {
                                    handleChange(e, setLastName)
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                label="UserName"
                                size="small"
                                onChange={(e) => {
                                    handleChange(e, setUserName)
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                size="small"
                                onChange={(e) => {
                                    handleChange(e, setPassword)
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="password"
                                label="Confirm Password"
                                type="password"
                                size="small"
                                onChange={(e) => {
                                    handleChange(e, setConfirmPassword)
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth

                                label="Phone Number"
                                type="Number"
                                size="small"
                                onChange={(e) => {
                                    handleChange(e, setPhone)
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="email"
                                label="Email Address"
                                name="email"
                                autoComplete="email"
                                size="small"
                                type="email"
                                onChange={(e) => {
                                    handleChange(e, setEmail)
                                }}
                            />
                        </Grid>

                    </Grid>
                    <Button
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={handleSubmit}
                    >
                        Đăng Kí
                    </Button>
                    <Grid container justifyContent="flex-end">
                        <Grid item>
                            <Button onClick={() => {
                                setRegester(false)
                            }}>
                                Đăng Nhập
                            </Button>
                        </Grid>
                    </Grid>
                </Box>
            </div>
        </Container>
    );
}