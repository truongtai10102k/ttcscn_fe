import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import BackgoundImage from "../image/bg.jpg"
const useStyles = makeStyles((theme) => ({
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        maxWidth: "450px",
        height: "400px",
        margin: 'auto',
        marginTop: "8%",
        backgroundColor: '#fafafa',
        padding: "15px 40px 0",
        borderRadius: "10px",
        position: 'relative',
        zIndex: 2,
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    }, container: {
        margin: 0,
        backgroundImage: `url(${BackgoundImage})`,
        backgroundRepeat: "none",
        backgroundSize: "cover",
        width: '100%',
        height: '100%',
        top: 0,
        left: 0,
        position: 'absolute',
        padding: 0,
        '&::before': {
            content: '""',
            width: '100%',
            height: "100vh",
            position: 'absolute',
            zIndex: 1,
            top: 0,
            left: 0,
            backgroundColor: '#0000007d',
        },
        '&.MuiContainer-maxWidthLg': {
            maxWidth: '100%',
        }
    }
}));

export default function ResetPasswordComponent(props) {
    const classes = useStyles();
    const { setResetPassword, forgotPassword } = props

    const [email, setEmail] = useState('');
    function validateEmail(email) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    const hanldeForgotPassword = () => {
        if (email !== '') {
            if (validateEmail(email)) {
                forgotPassword(email)
                setResetPassword(false)
            } else {
                alert('Vui lòng nhập đúng định dạng Email')
            }
        } else {
            alert("Vui Lòng Nhập Đủ Thông Tin")
        }
    }

    return (
        <Container className={classes.container} component="main">
            <CssBaseline />
            <div className={classes.paper}>
                <Typography component="h1" variant="h5">
                    Quên Mật Khẩu
                </Typography>
                <Box className={classes.form} noValidate>
                    <Grid container spacing={2}>

                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="email"
                                label="Email Address"
                                name="email"
                                autoComplete="email"
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </Grid>

                    </Grid>
                    <Button
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={hanldeForgotPassword}
                    >
                        Xác Nhận
                    </Button>
                    <Grid container justifyContent="flex-end">
                        <Grid item>
                            <Button onClick={() => {
                                setResetPassword(false)
                            }}>
                                Đăng Nhập
                            </Button>
                        </Grid>
                    </Grid>
                </Box>
            </div>
        </Container>
    );
}