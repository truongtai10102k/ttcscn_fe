import React, { useState, useEffect } from 'react';
import { Box, Dialog, makeStyles, Typography, TextField, Button, Grid } from '@material-ui/core';
import {
    AirlineSeatReclineNormalOutlined as Seat,
    LocalGasStation as Fuel,
    Battery60 as Attrition,
    AssignmentInd as Card,
    CreditCard as Card2,
    LocationOn as Location,
    DepartureBoard as Submit
} from '@material-ui/icons';
import AlbumPicker from '../../Components/Picker/AlbumPicker';
const CarDetailModal = (props) => {
    const { open, close, data } = props
    const classes = useStyles()
    const [startTime, setStartTime] = useState('2017-05-27T10:30');
    const [endTime, setEndTime] = useState('2017-05-27T10:30');
    const [image, setListImage] = useState([]);
    const [money, setMoney] = useState(0);
    const handleBillMoney = (start, end) => {
        var date1 = new Date(start);
        var date2 = new Date(end);
        var Difference_In_Time = date2.getTime() - date1.getTime();
        var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
            return Math.ceil((Difference_In_Days * (+data.price) + (+data.insuranceFee)))
    }
    const ssdate = (start, end) => {
        var date1 = new Date(start);
        var date2 = new Date(end);
        var Difference_In_Time = date2.getTime() - date1.getTime();
        var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
        return Difference_In_Days
    }
    useEffect(() => {
        setEndTime('2017-05-27T10:30')
        setStartTime('2017-05-27T10:30')
        setMoney(handleBillMoney('2017-05-27T10:30', '2017-05-27T10:30'))
    }, []);

    useEffect(() => {
        if (endTime !== 0) {
            setMoney(handleBillMoney(startTime, endTime))
        }
    }, [endTime]);
    useEffect(() => {
        if (startTime !== 0) {
            setMoney(handleBillMoney(startTime, endTime))
        }
    }, [startTime]);

    const handleRedirect = async () => {
        let time = JSON.stringify(startTime + '&&' + endTime)
        await localStorage.setItem('car', time)
        window.location.href = window.location.origin + `/contact/${data.id}`
    }


    useEffect(() => {
        if (data?.imageUrl) {
            let arrImg = data?.imageUrl.trim().split('@@')
            let newArr = arrImg.filter((item, idx) => { return idx > 0 })
            setListImage(data?.imageUrl)
        }
    }, [data]);

    return (
        <Dialog open={open} onClose={close} maxWidth='60%' >
            <Box className={classes.container}>
                <Grid container>
                    <Grid item sm={8}>
                        <Box>
                            <Box className={classes.titleCar} >{data?.name_car_company + ` ` + data?.name}</Box>
                            <Box className={classes.imageBlock} >
                                <AlbumPicker
                                    height={450}
                                    width={500}
                                    // setListImage={(data) => setImage(data)}
                                    listSrc={image}
                                    readonly={true}
                                />
                            </Box>
                            <Box>
                                <Box className={classes.containerItem}>
                                    <Typography className={classes.itemLeft}>Đặc Điểm</Typography>
                                    <Box className={classes.itemRight}>
                                        <Box className={classes.containerItem1} style={{ justifyContent: 'space-between' }}>
                                            <Box>
                                                <Box className={classes.containerItem1}>
                                                    <Box> <Seat /></Box>
                                                    <Typography> Số Ghế : {data && data.seat} </Typography>
                                                </Box>
                                                <Box className={classes.containerItem1}>
                                                    <Box> <Fuel /></Box>
                                                    <Typography> Nhiên liệu :  {data && data.fuel}  </Typography>
                                                </Box>
                                            </Box>
                                            <Box className={classes.containerItem1}>
                                                <Box> <Attrition /></Box>
                                                <Box> Tiêu hao :  {data && data.attrition} lít/100km </Box>
                                            </Box>
                                        </Box>
                                    </Box>
                                </Box>
                                <Box className={classes.containerItem}>
                                    <Typography className={classes.itemLeft}>Mô tả</Typography>
                                    <Typography className={classes.itemRight}>
                                        {data && data.description}
                                    </Typography>
                                </Box>
                                <Box className={classes.containerItem}>
                                    <Typography className={classes.itemLeft}>Tính năng</Typography>
                                    <Typography className={classes.itemRight}>  {data && data.utilitie}</Typography>
                                </Box>
                            </Box>

                            <Box>
                                <Box className={classes.containerItem}>
                                    <Typography className={classes.itemLeft}>Giấy tờ thuê xe(bắt buộc)</Typography>
                                    <Box className={classes.itemRight}>
                                        <Box className={classes.containerItem1}>
                                            <Box className={classes.containerItem1}>
                                                <Box> <Card /></Box>
                                                <Typography> CMND và GPLX (đối chiếu) </Typography>
                                            </Box >
                                            <Box className={classes.containerItem1}>
                                                <Box> <Card2 /></Box>
                                                <Typography> Hộ Khẩu hoặc KT3 hoặc Passport (giữ lại) </Typography>
                                            </Box>
                                        </Box>

                                    </Box>
                                </Box>
                                <Box className={classes.containerItem}>
                                    <Typography className={classes.itemLeft}>Tài sản thế chấp</Typography>
                                    <Typography className={classes.itemRight}>
                                        15 triệu (tiền mặt/chuyển khoản khi nhận xe)
                                        hoặc Xe máy (kèm cà vẹt gốc) giá trị 15 triệu
                                    </Typography>
                                </Box>
                                <Box className={classes.containerItem}>
                                    <Typography className={classes.itemLeft}>Điều khoản</Typography>
                                    <Typography className={classes.itemRight}>
                                        {data && data.rule}
                                    </Typography>
                                </Box>
                            </Box>
                        </Box>
                    </Grid>
                    <Grid item sm={4}>
                        <Box>
                            <Box className={classes.titleHead}> <Box component={'span'} className={classes.price}>{data && data.price}đ</Box>  /Ngày</Box>
                            <Box className={classes.time}>
                                <TextField className={classes.rightItem} value={startTime} type='datetime-local' label='Ngày bắt đầu'
                                    onChange={(e) => {
                                        if (ssdate(e.target.value, endTime) >= 0) {
                                            setStartTime(e.target.value)
                                        } else {
                                            alert("Chọn ngày  không hợp lệ")
                                        }
                                    }}
                                />
                                <TextField className={classes.rightItem} value={endTime} type='datetime-local' label='Ngày kết thúc'
                                    onChange={(e) => {
                                        if (ssdate(startTime, e.target.value) >= 0) {
                                            setEndTime(e.target.value)
                                        } else {
                                            alert("Chọn ngày  không hợp lệ")
                                        }
                                    }}
                                />
                            </Box>
                            <Box className={classes.containerItem2}>
                                <Typography className={classes.itemLeft}>Đia điểm nhận xe</Typography>
                                <Typography>
                                    <Location /> Học viện Kỹ Thuật Mật Mã
                                </Typography>
                            </Box>
                            <Box className={classes.containerItem2}>
                                <Typography className={classes.itemLeft}>Giới hạn số Km</Typography>
                                <Typography>
                                    Tối đa 300km/ngày. Phí 4k/km nếu vượt giới hạn
                                </Typography>
                            </Box>
                            <Box className={classes.containerItem2}>
                                <Typography className={classes.itemLeft}>Bảo hiểm</Typography>
                                <Typography>
                                    Chuyến xe được mua bảo hiểm
                                </Typography>
                            </Box>
                            <Box className={classes.containerItem2}>
                                <Typography>Chi tiết giá</Typography>
                                <Box>
                                    <Box className={classes.containerItem1}>
                                        <Typography className={classes.itemLeft}>Đơn giá thuê</Typography>
                                        <Typography>{data && data.price} đ /ngày</Typography>
                                    </Box>
                                    <Box className={classes.containerItem1}>
                                        <Typography className={classes.itemLeft}>Phí bảo hiểm</Typography>
                                        <Typography>{data && data.insuranceFee} đ /ngày</Typography>
                                    </Box>
                                    <Box className={classes.containerItem1}>
                                        <Typography className={classes.itemLeft}>Tổng phí thuê xe </Typography>
                                        <Typography>{(data.price && +data.price) + (data.insuranceFee && +data.insuranceFee)} đ x 1 ngày</Typography>
                                    </Box>
                                    <Box className={classes.containerItem1}>
                                        <Typography className={classes.itemLeft}>Tổng cộng </Typography>
                                        <Typography>{
                                            startTime !== 0 && endTime !== 0
                                                ?
                                                money
                                                :
                                                (data.price && +data.price) + (data.insuranceFee && +data.insuranceFee)
                                        } đ</Typography>
                                    </Box>
                                </Box>
                            </Box>
                            <Box >
                                <Button variant='contained' color='primary' className={classes.button}
                                    onClick={handleRedirect}
                                >

                                    <Submit style={{marginRight: 10}} /> Đặt xe
                                </Button>
                                <Button variant='contained' color='secondary'
                                    onClick={close}
                                >
                                    Huỷ
                                </Button>
                            </Box>
                        </Box>
                    </Grid>

                </Grid>
            </Box>
        </Dialog >
    );
}

export default CarDetailModal;

const useStyles = makeStyles((theme) => {
    return ({
        container: {
            // background: 'transparent',
            // height: 1000,
            // width: 1000,
            display: 'flex',
            padding: '30px 40px'
        }, imageBlock: {
            width: '500px',
            margin: '0px auto 40px'
        }, image: {
            width: '100%',
            height: '100%',
            objectFit: 'cover'
        }, containerItem: {
            display: 'flex',
            width: '100%',
            justifyContent: 'flex-start',
            padding: '15px 0'
        }, containerItem1: {
            display: 'flex',
            width: '100%',
            justifyContent: 'flex-start',
        }, containerItem2: {
            width: '100%',
            padding: '15px 0'
        }, itemRight: {
            width: '100%',
            marginLeft: 20
        }, itemLeft: {
            width: '150px',
            height: '100%',
            fontWeight: 600
        }, price: {
            color: '#00a550',
            fontSize: '2.5rem',
            fontWeight: 700,
        }, titleCar: {
            color: '#00a550',
            fontSize: '3rem',
            fontWeight: 700,
            textAlign: 'center',
            paddingBottom: 20
        }, titleHead: {
            fontSize: '13px',
            fontWeight: 700,
            color: '#67737f',
            textAlign: 'center'
        }, rightItem: {
            margin: '10px 0'
        }, button: {
            marginRight: 20,
            textAlign: 'center'
        }, time: {
            display: 'flex',
            flexDirection: 'column',
            padding: '0 50px 0 0 ',
            justifyContent: 'flex-start',
        }
    })
})