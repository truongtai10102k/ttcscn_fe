import React, { useState, useEffect } from 'react';
import { Box, makeStyles, Typography, Grid, Container, Button } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination'
import CarDetailModal from './CarDetailModal';
import { DOMAIN_GET_IMAGE } from '../../../FetchAPI/callAPIConfig';
const CarItemComponent = (props) => {
    const { title, listDataEU, activePage, totalPage, textSeach,
        seat, company, searchCarEU, } = props
    const classes = useStyle()
    const [openDialog, setOpenDialog] = useState(false);
    const [data, setData] = useState({});
    const handleCloseModal = () => {
        setOpenDialog(false)
    }


    const listItem = listDataEU.map((item, idx) => {
        return (
            <Grid item sm={6} md={3} key={idx}>
                <Box className={classes.item}>
                    <Box>
                        <img className={classes.image} src={item.imageUrl ? DOMAIN_GET_IMAGE + item?.imageUrl.trim().split('@@')[1] : ''} />
                    </Box>
                    <Box className={classes.textBlock} >
                        <Typography className={classes.nameCar}>
                            {item.name}
                        </Typography>
                        <Box className={classes.textBottom}>
                            <Typography className={classes.textPrice}>
                                Hãng : {item.name_car_company}
                            </Typography>
                            <Typography className={classes.textPrice}>
                                Giá : {item.price}
                            </Typography>
                        </Box>
                        <Box className={classes.textBottom}>
                            <Typography className={classes.textPrice}>
                                Nhiên liệu : {item.fuel}
                            </Typography>
                            <Typography className={classes.textPrice}>
                                Năm SX : {item.manufacture}
                            </Typography>
                        </Box>
                    </Box>
                    <Box className={classes.button}>
                        <Button variant='contained' color='primary'
                            onClick={() => {
                                setOpenDialog(true)
                                setData(item)
                            }}
                        >Xem Xe</Button>
                    </Box>
                </Box>
                <CarDetailModal
                    data={data}
                    close={handleCloseModal}
                    open={openDialog}
                />
            </Grid>
        )
    })

    return (
        <Box>
            <Box className={classes.textBorder}>
                <Typography component={'span'} className={classes.text}>
                    {title}
                </Typography>
            </Box>
            <Container className={classes.listItem}>
                <Grid container spacing={2} >
                    {listItem}
                </Grid>
            </Container>
            {/* <Pagination
                className={classes.pagination}
                color="primary"
                count={totalPage}
                page={activePage}
                onChange={(e, number) => {
                    searchCarEU({ textSeach: textSeach, seat: seat, company: company, page: number })
                }}
            /> */}
        </Box>
    );
}

export default CarItemComponent;

const listCar = [
    {
        car: 'xe 1',
        company: 'Honda',
        price: '100000',
        fuel: 'Xăng',
        manufacture: 2012
    },
    {
        car: 'xe 2',
        company: 'Honda',
        price: '100000',
        fuel: 'Xăng',
        manufacture: 2012
    },
    {
        car: 'xe 3',
        company: 'Honda',
        price: '100000',
        fuel: 'Xăng',
        manufacture: 2012
    },
    {
        car: 'xe 4',
        company: 'Honda',
        price: '100000',
        fuel: 'Xăng',
        manufacture: 2012
    },
    {
        car: 'xe 5',
        company: 'Honda',
        price: '100000',
        fuel: 'Xăng',
        manufacture: 2012
    },
    {
        car: 'xe 6',
        company: 'Honda',
        price: '100000',
        fuel: 'Xăng',
        manufacture: 2012
    },
    {
        car: 'xe 7',
        company: 'Honda',
        price: '100000',
        fuel: 'Xăng',
        manufacture: 2012
    },
]






const useStyle = makeStyles((themes) => {
    return ({
        textBorder: {
            margin: '30px 0 15px',
            position: 'relative',
            '&::before': {
                content: '""',
                width: '100%',
                position: 'absolute',
                zIndex: 1,
                top: '20px',
                left: 0,
                borderTop: '4px solid #d1a9a9',
            },
        }, text: {
            color: '#43a047',
            fontSize: 28,
            fontWeight: '700',
            marginLeft: '100px',
            backgroundColor: '#323232',
            'zIndex': 2,
            position: 'relative',
            padding: '0 15px',
        }, image: {
            width: '100%',
            height: '250px',
            objectFit: 'cover',
            borderRadius: '8px'
        }, listItem: {
            padding: '0 40px',
            maxWitdh: '50%'
        }, item: {
            height: '100%',
            borderRadius: '5px',
            border: '6px solid #43a04782',
            color: '#ffffff96',
            margin: '10px 0',
            // padding: '10px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
            '&:hover': {
                border: '6px solid #43a047',
                color: '#fff',
            }
        }, textContent: {
            marginTop: 40,
            position: 'relative',
            '&::before': {
                content: '""',
                width: '60%',
                position: 'absolute',
                zIndex: 1,
                top: '-15px',
                left: '20%',
                borderTop: '2px solid #43a04782',
            },
        }, textBlock: {
            position: 'relative',
            '&::before': {
                content: '""',
                width: '60%',
                position: 'absolute',
                zIndex: 1,
                // bottom: '30px',
                left: '20%',
                borderTop: '2px solid #43a04782',
            },
        }, pagination: {
            display: "flex",
            justifyContent: 'center',
            margin: '40px 60px 0'
        }, nameCar: {
            fontSize: 30,
            textAlign: 'center',
            fontWeight: 500,
            color: '#60de65'
        }, textBottom: {
            display: 'flex',
            padding: '0px 10px',
            justifyContent: 'space-between',
        }, textPrice: {
            fontSize: 16
        }, button: {
            padding: '15px 10px',
            display: 'flex',
            justifyContent: 'end'
        }
    })
})