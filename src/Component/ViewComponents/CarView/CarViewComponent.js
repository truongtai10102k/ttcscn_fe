import React, { useEffect, useState } from 'react';
import { Box, makeStyles } from '@material-ui/core';
import SearchBox from './SearchBox';
import FooterComponent from '../Home/FooterComponent';
import CarItemComponent from './CarItemComponent';
import { connect } from 'react-redux';
import { searchCarEURequest, getCompanyRequest } from '../../../actions';
const CarViewComponent = (props) => {
    const { listDataEU, searchCarEU, getCompany, listCompany,
        listCarSearch, activePage, totalPage, listDataEUSpotLight
    } = props
    const classes = useStyles()
    useEffect(() => {
        getCompany({ page: 1 })
    }, []);
    const [textSeach, setTextSeach] = useState('');
    const [seat, setSeat] = useState('');
    const [company, setCompany] = useState('');
    console.log('====================================');
    console.log("listDataEUSpotLightlistDataEUSpotLight ", listDataEUSpotLight);
    console.log('====================================');
    let listCarSpotLight = listDataEUSpotLight.listCarByCompany
        && listDataEUSpotLight.listCarByCompany.length > 0
        && listDataEUSpotLight.listCarByCompany.map((item, idx) => {
            if (item.listCar !== []) {
                return (
                    <Box key={idx}>
                        <CarItemComponent listDataEU={item.listCar} title={item.nameCompany} />
                    </Box>
                )
            }
        })
    return (
        <Box className={classes.container}>
            <SearchBox
                searchCarEU={searchCarEU}
                listCompany={listCompany}
                setTextSeach={setTextSeach}
                setSeat={setSeat}
                setCompany={setCompany}
                textSeach={textSeach}
                seat={seat}
                company={company}
            />
            {
                textSeach && listCarSearch
                &&
                <CarItemComponent
                    listDataEU={listCarSearch}
                    activePage={activePage}
                    totalPage={totalPage}
                    title={'Kết quả tìm kiếm'}
                    textSeach={textSeach}
                    seat={seat}
                    company={company}
                    searchCarEU={searchCarEU}
                />
            }
            {listCarSpotLight}
            <Box className={classes.footer}>
                <FooterComponent />
            </Box>
        </Box>
    );
}



const mapStateToProps = (state) => {
    return ({
        listCompany: state.company.listData,
        listCarSearch: state.car.listDataSearch,
        activePage: state.car.activePage,
        totalPage: state.car.totalPage,
    })
}

const mapDispatchToProps = (dispatch) => {
    return {
        searchCarEU: (data) => {
            dispatch(searchCarEURequest(data));
        },
        getCompany: (data) => {
            dispatch(getCompanyRequest(data));
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CarViewComponent)

const useStyles = makeStyles((theme) => {
    return ({
        container: {
            width: '100%',
            backgroundColor: 'rgba(0,0,0,0.8)'
        }, footer: {
            marginTop: 50
        }
    })
})