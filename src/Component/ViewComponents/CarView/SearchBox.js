import React, { useEffect, useState } from 'react';
import { Box, Button, withStyles, makeStyles, TextField, Grid, Typography } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useLocation } from "react-router-dom";
const SearchBox = (props) => {
    const location = useLocation();
    const { searchCarEU, listCompany, setTextSeach,
        setSeat, setCompany, textSeach, seat, company } = props
    const classes = useStyles()

    useEffect(() => {
        if (location.state && location.state.textSearch) {
            searchCarEU({ textSeach: location.state.textSearch, seat: '', company: '', page: 1 })

            setTextSeach(location.state.textSearch)
            console.log(location.state?.textSearch);
        }
    }, [location]);
    const handleSearch = () => {
        searchCarEU({ textSeach: textSeach, seat: seat, company: company, page: 1 })

    }
    const listSeat = [
        {
            name: '2',
        }, {
            name: '4',
        }, {
            name: '6',
        }, {
            name: '8',
        }, {
            name: '12',
        }, {
            name: '24',
        }, {
            name: '30',
        }, {
            name: '45',
        }
    ]
    return (
        <Box className={classes.searchBox}>
            <Typography className={classes.title}>Hơn trăm xe đang đợi bạn, Hãy tìm kiếm nào !!</Typography>
            <Grid container justifyContent='space-between'>
                <Grid item md={2} sm={6}>
                    <Box className={classes.autocomplete}>
                        <Autocomplete
                            autoHighlight
                            fullWidth
                            color={'#fff'}
                            // value={{ title: fractionPending }}
                            options={listCompany}
                            onChange={(event, newValue) => {
                                setCompany(newValue);
                            }}
                            getOptionLabel={(option) => option.name}
                            renderInput={(params) => <CssTextField {...params}
                                placeholder="Hãng xe"
                                variant="outlined"
                                className={classes.textField}
                                style={{ color: 'white' }}
                                InputLabelProps={{
                                    style: {
                                        textOverflow: 'ellipsis',
                                        whiteSpace: 'nowrap',
                                        overflow: 'hidden',
                                        width: '100%',
                                        color: '#43a047'
                                    }
                                }}
                            />}

                        />
                    </Box>
                </Grid>
                <Grid item md={2} sm={6}>
                    <Box className={classes.autocomplete}>
                        <CssAutocomplete
                            autoHighlight
                            fullWidth
                            color={'#fff'}
                            // value={{ title: fractionPending }}
                            options={listSeat}
                            getOptionLabel={(option) => option.name}
                            renderInput={(params) => <CssTextField {...params}
                                placeholder="Chỗ ngồi"
                                variant="outlined"
                                className={classes.textField}
                                style={{ color: 'white' }}
                                InputLabelProps={{
                                    style: {
                                        textOverflow: 'ellipsis',
                                        whiteSpace: 'nowrap',
                                        overflow: 'hidden',
                                        width: '100%',
                                        color: '#43a047'
                                    }
                                }}
                            />}
                            onChange={(event, newValue) => {
                                setSeat(newValue);
                            }}
                        />
                    </Box>
                </Grid>
                <Grid item md={8} sm={12}>
                    <Box className={classes.searchGroup}>
                        <CssTextField
                            label='Tìm kiếm xe ...'
                            variant="outlined"
                            id="custom-css-outlined-input"
                            className={classes.textField}
                            fullWidth
                            color='primary'
                            InputProps={{
                                className: classes.multilineColor
                            }}
                            value={textSeach}
                            onChange={(e) => setTextSeach(e.target.value)}
                            InputLabelProps={{
                                style: {
                                    textOverflow: 'ellipsis',
                                    whiteSpace: 'nowrap',
                                    overflow: 'hidden',
                                    width: '100%',
                                    color: '#43a047'
                                }
                            }}
                        />
                        <Button variant='contained' color='primary' style={{ width: '105px', marginLeft: '15px' }}
                            onClick={handleSearch}
                        >Tìm Kiếm</Button>
                    </Box>
                </Grid>
            </Grid>
        </Box>
    )
}

export default SearchBox;

const useStyles = makeStyles((theme) => {
    return ({
        searchBox: {
            width: '100%',
            // display: 'flex',
            margin: '0 auto',
            backgroundColor: '#4b4b4b66',
            padding: '2rem 10rem',
        }, searchGroup: {
            display: 'flex',
            // width: '90%',
            margin: '0 auto',
            padding: '10px 0'
        }
        , text: {
            color: '#fff',
            textAlign: 'center',
            fontSize: 26,
            fontWeight: 'bold',
            marginBottom: 30
        }, textField: {
            width: '90%',
            marginLeft: 'auto',
            marginRight: 'auto',
            paddingBottom: 0,
            marginTop: 0,
            fontWeight: 500,
            color: 'white',
            borderColor: 'white'
        },
        input: {
            color: 'white'
        }, multilineColor: {
            color: 'white'
        }, title: {
            color: '#fff',
            fontSize: 24,
            textAlign: 'center',
            padding: '10px 0'
        }, autocomplete: {
            padding: '10px 0'
        },
    })
})


const CssTextField = withStyles({
    root: {
        color: '#43a047',
        '& label.Mui-focused': {
            color: 'green',
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: 'green',
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: '#43a047',
            },
            '&:hover fieldset': {
                borderColor: '#43a047',
            },
            '&.Mui-focused fieldset': {
                borderColor: 'green',
            },
        },
    },
})(TextField);

const CssAutocomplete = withStyles({
    root: {
        color: '#43a047',
        '.MuiInputBase-root': {
            color: '#fff'
        }
        // '& label.Mui-focused': {
        //     color: 'green',
        // },
        // '& .MuiInput-underline:after': {
        //     borderBottomColor: 'green',
        // },
        // '& .MuiOutlinedInput-root': {
        //     '& fieldset': {
        //         borderColor: '#43a047',
        //     },
        //     '&:hover fieldset': {
        //         borderColor: '#43a047',
        //     },
        //     '&.Mui-focused fieldset': {
        //         borderColor: 'green',
        //     },
        // },
    },
})(Autocomplete);