import React, { useState, useEffect } from 'react';
import {
    Box, Dialog,
    makeStyles, TextField,
    Button,
    IconButton, Grid
} from '@material-ui/core';
import { useParams } from 'react-router-dom';
import { connect } from "react-redux"
import { getCarDetailEURequest, postContactRequest } from '../../../actions/index';
import localStorageWorker from '../../../localStorage';
const ContactViewComponent = (props) => {
    const classes = useStyles()
    const { carId } = useParams()
    const { getCarDetail, carDetail, postContact } = props

    const [data, setData] = useState('');
    const [carDetail1, setCarDetail1] = useState(carDetail);
    const [startDate, setStartDate] = useState('2017-05-27T10:30');
    const [endDate, setEndDate] = useState('2017-05-27T10:30');
    const [money, setMoney] = useState(0);

    const [customerName, setCustomerName] = useState('');
    const [idCard, setIdCard] = useState(0);
    const [issueDate, setIssueDate] = useState('2017-05-27');
    const [address, setAddress] = useState('');

    const handleChange = (e, setStateChange) => {
        setStateChange(e.target.value)
    }

    const handleBillMoney = (start, end) => {
        var date1 = new Date(start);
        var date2 = new Date(end);
        var Difference_In_Time = date2.getTime() - date1.getTime();
        var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
        // return Math.ceil((Difference_In_Days * 5 + 10))
        return Math.ceil((Difference_In_Days * (+carDetail?.price) + (+carDetail?.insuranceFee)))
    }

    useEffect(() => {
        if (carDetail) {
            setCarDetail1(carDetail)
        }
    }, [carDetail]);
    useEffect(() => {
        if (endDate !== 0) {
            setMoney(handleBillMoney(startDate, endDate))
        }
    }, [endDate]);
    useEffect(() => {
        if (startDate !== 0) {
            setMoney(handleBillMoney(startDate, endDate))
        }
    }, [startDate]);

    useEffect(() => {
        getCarDetail({ id: carId })
        console.log("hihihiiiiiiii");
        let stringTime = JSON.parse(localStorage.getItem('car'))
        let arrTime = stringTime.split('&&');
        setData(arrTime)
        if (arrTime.length > 0) {
            setStartDate(arrTime[0])
            setEndDate(arrTime[1])
            setMoney(handleBillMoney(startDate, endDate))
            console.log("updat ", handleBillMoney(startDate, endDate));

        }
    }, []);


    const handleSubmit = () => {
        postContact({
            customerName,
            idCard,
            issueDate,
            address,
            startDate,
            endDate,
            dateRegester: Date.now(),
            id_car: carDetail.id,
            //id user account 
            customerId: 1,
            name_car: carDetail?.name,
            money: money,
            userName: localStorageWorker.getToken().user.userName
        })
    }

    return (
        <Box className={classes.block}>
            <Box className={classes.container} >
                <Box className={classes.dialogHead}>Thông Tin Hợp Đồng</Box>

                <Box>
                    {/* <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Mã hợp đồng' />
                    </Box> */}
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Tên khách hàng'
                            onChange={(e) => handleChange(e, setCustomerName)}
                        />
                    </Box>
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Số Chứng Minh Thư'
                            onChange={(e) => handleChange(e, setIdCard)} />
                    </Box>
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' type='date' variant='outlined' label='Ngày Cấp Chứng Minh Thư'
                            value={issueDate}
                            onChange={(e) => handleChange(e, setIssueDate)}
                        />
                    </Box >
                    <Box className={classes.inputText}>
                        <TextField minRows='3' multiline fullWidth size='small' variant='outlined' label='Địa Chỉ Thường Trú'
                            onChange={(e) => handleChange(e, setAddress)} />
                    </Box >
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Hãng xe'
                            value={carDetail1?.name_car_company}
                        />
                    </Box>
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Tên xe'
                            value={carDetail1?.name}
                        />
                    </Box>
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Biển Số'
                            value={carDetail1?.licensePlate}
                        />
                    </Box>
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Năm sản xuất'
                            value={carDetail1?.manufacture}
                        />
                    </Box>
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' variant='outlined' label='Số khung'
                            value={carDetail1?.frameNumber}
                        />
                    </Box>
                    <Box className={classes.inputText}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={5}>
                                <TextField
                                    fullWidth size='small'
                                    variant='outlined'
                                    label='Giá Thuê Trên Ngày'
                                    value={carDetail1?.price}
                                    type='number'
                                />
                            </Grid>

                            <Grid item xs={12} sm={7}>
                                <TextField
                                    fullWidth size='small'
                                    variant='outlined'
                                    label='Phí Bảo Hiểm'
                                    value={carDetail1?.insuranceFee}
                                />
                            </Grid>
                        </Grid>
                    </Box>
                    <Box className={classes.inputText}>
                        <TextField fullWidth className={classes.rightItem} value={startDate} type='datetime-local' label='Ngày đăng kí nhận xe'
                            onChange={(e) => {
                                setStartDate(e.target.value)
                            }}
                        />
                    </Box >
                    <Box className={classes.inputText}>
                        <TextField fullWidth className={classes.rightItem} value={endDate} type='datetime-local' label='Ngày đăng kí trả xe'
                            onChange={(e) => { setEndDate(e.target.value) }}
                        />
                    </Box >
                    <Box className={classes.inputText}>
                        <TextField fullWidth size='small' value={money + ` đ`} variant='outlined' label='Tổng tiền' />
                    </Box >

                </Box>
                <Box className={classes.buttonGroup}>
                    <Box className={classes.button}>
                        <Button className={classes.buttonLeft} color='primary' variant='contained'
                            onClick={handleSubmit}
                        >Đồng ý</Button>
                    </Box>
                    <Box className={classes.button}>
                        <Button color='#414141' variant='contained'
                            onClick={() => {
                                window.location.href = window.location.origin
                            }} > Huỷ </Button>
                    </Box>
                </Box>
            </Box>
        </Box>
    );
}

const mapStateToProps = (state) => {
    return ({
        carDetail: state.car.carDetail

    })
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCarDetail: (data) => {
            dispatch(getCarDetailEURequest(data))
        },
        postContact: (data) => {
            dispatch(postContactRequest(data))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ContactViewComponent);

const useStyles = makeStyles((themes) => ({
    block: {
        // backgroundColor: 'rgba(0,0,0,0.8)'
    },
    container: {
        maxWidth: 800,
        margin: '0 auto'
        // padding:30
    },
    dialogHead: {
        width: 550,
        textAlign: 'center',
        padding: '10px 0',
        fontWeight: 'bold',
        fontSize: 20,
        borderBottom: '1px solid #000',
        margin: '0 auto'
    }, inputText: {
        paddingTop: 20,
        width: 400,
        margin: '0 auto'
    }, button: {
        padding: "15px 50px",
        display: 'flex',
        justifyContent: 'flex-end'
    }, buttonLeft: {
        marginRight: 15
    }, textarea: {
        width: '100%',
        maxWidth: '100%',
        minWidth: '100%',
    }, iconbtn: {
        position: 'relative',
        left: 130,
        top: -10,
        fontSize: 16
    }, buttonGroup: {
        display: 'flex',
        padding: '16px 0px',
        justifyContent: 'center'
    }
}))