import React, { useState, useEffect } from 'react'
import { GoogleMap, Marker, } from '@react-google-maps/api';
import { Box, Button, Dialog } from "@material-ui/core"
import useStyles from './GoogleMapStyles';

const Map = (props) => {
    const { height, width } = props
    const classes = useStyles()
    // google map
    const [currentPosition, setCurrentPosition] = useState({});

    const onMarkerDragEnd = (e) => {
        const lat = 20.98077185729812;
        const lng = 105.79621440209964;
        setCurrentPosition({ lat, lng })
    };

    const mapStyles = {
        height: height,
        width: width,
        margin: "0 auto"
    };

    const success = position => {
        const currentPosition = {
            lat: 20.98077185729812,
            lng: 105.79621440209964
        }
        setCurrentPosition(currentPosition);
    };

    navigator.geolocation.getCurrentPosition(success);

    return (
        <Box >
            <Box className={classes.mapContainer}>
                <GoogleMap
                    mapContainerStyle={mapStyles}
                    zoom={17}
                    center={currentPosition}>
                    {
                        currentPosition.lat ?
                            <Marker
                                position={currentPosition}
                                // onDragEnd={(e) => onMarkerDragEnd(e)}
                                draggable={false}
                                 /> :
                            null
                    }
                </GoogleMap>
            </Box>
        </Box>
    );
}

export default Map


// import React from 'react'
// import { GoogleMap, useJsApiLoader, Marker } from '@react-google-maps/api';

// const containerStyle = {
//     width: '400px',
//     height: '400px'
// };

// var center = {
//     lat: 20.98077185729812,
//     lng: 105.79621440209964
// };

// function MyComponent() {
//     const { isLoaded } = useJsApiLoader({
//         id: 'google-map-script',
//         googleMapsApiKey: "YOUR_API_KEY"
//     })

//     const [map, setMap] = React.useState(null)

//     const onLoad = React.useCallback(function callback(map) {
//         const bounds = new window.google.maps.LatLngBounds();
//         map.fitBounds(bounds);
//         setMap(map)
//     }, [])

//     const onUnmount = React.useCallback(function callback(map) {
//         setMap(null)
//     }, [])
//     return isLoaded ? (
//         <GoogleMap
//             mapContainerStyle={containerStyle}
//             center={center}
//             zoom={17}
//             onLoad={onLoad}
//             onUnmount={onUnmount}

//         >
//             { /* Child components, such as markers, info windows, etc. */}
//             {/* {
//                 // <Marker
//                 //     position={center}
//                 //     draggable={false}
//                 // />} */}
//             <></>
//         </GoogleMap>
//     ) : <></>
// }

// export default React.memo(MyComponent)