import { makeStyles } from "@material-ui/core";
const useStyles = makeStyles(theme => ({
    mapContainer: {
        maxWidth: "800px",
        height: "100%",
        margin: '50px auto'
    }, buttonGroup: {
        padding: "10px 15px",
        display: "flex",
        justifyContent: "flex-end",
    }, buttonItem: {
        marginLeft: "15px"
    }
}))

export default useStyles;