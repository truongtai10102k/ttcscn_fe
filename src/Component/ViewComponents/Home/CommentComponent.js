import React from 'react';
import { Box, makeStyles, Typography, Grid, Container} from '@material-ui/core';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
const SlideComponent = (props) => {
    const classes = useStyle()
    return (
        <Box>
            <Box className={classes.textBorder}>
                <Typography component={'span'} className={classes.text}>
                    Về Chúng Tôi
                </Typography>
            </Box>
            <Container className={classes.listItem}>
                <Grid container spacing={3} justifyContent={'space-between'}>
                    <Grid item sm={4}>
                        <Box className={classes.item}>
                            <Typography className={classes.textContentOne} >
                                Công ty cho thuê xe ACTVN Car được thành lập và cuối năm 2010.
                                Là doanh nghiệp hoạt động chính ở lĩnh vực cho thuê xe du lịch,
                                cho thuê xe tự lái, hợp đồng cho thuê xe tháng, cho thuê xe đưa rước
                                chuyên gia nước ngoài, đưa rước học sinh, đưa rước cán bộ công nhân viên,
                                dịch vụ xe hoa, xe cưới…
                            </Typography>
                            <Box>
                                <img className={classes.image} src={'https://i.pinimg.com/originals/f5/78/ed/f578edc10547c0f2170d4a3ff8a70f3b.jpg'} />
                            </Box>
                        </Box>
                    </Grid>
                    <Grid item sm={4}>
                        <Box className={classes.item}>
                            <Box>
                                <img className={classes.image} src={'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJOunCFmLKkNKCplyyau919MWlrARfuqBw8Q&usqp=CAU'} />
                            </Box>
                            <Typography className={classes.textContent}>
                                Với kinh nghiệm hơn 10 năm sáng tạo và dẫn đầu trong lĩnh vực cho thuê xe ,Công ty cho thuê xe ACTVN Car
                                đã nhanh chóng nắm bắt được thời cuộc, khắc phục khó khăn để vươn lên, định hướng phát triển ngành dịch
                                vụ cho thuê xe theo hướng hiện đại hóa. Chúng tôi tự tin tạo ra những trải nghiệm đặc biệt , đáng nhớ,
                                với những chuyến đi của chính mình
                            </Typography>
                        </Box>
                    </Grid>
                    <Grid item sm={4} >
                        <Box className={classes.item}>
                            <Typography className={classes.textContentOne}>
                                Hiện tại chúng tôi đã có hơn 300 chiếc xe. Bao gồm xe 4 chỗ, cho thuê xe 7 chỗ, 9 chỗ
                                ,thuê xe 16 chỗ, 29 chỗ, 35 chỗ, 45 chỗ chỗ từ phổ thông đến cao cấp. Với những tiềm năng này
                                chúng tôi hoàn toàn đáp ứng được toàn bộ nhu cầu thuê xe của quý khách với mọi mục đích yêu cầu
                            </Typography>
                            <Box>
                                <img className={classes.image} src={'https://cdn.motor1.com/images/mgl/BnQNe/s1/2018-mercedes-amg-s63-coupe.webp'} />
                            </Box>
                        </Box>
                    </Grid>
                </Grid>
            </Container>

        </Box>
    );
}

export default SlideComponent;


const useStyle = makeStyles((themes) => {
    return ({
        textBorder: {
            margin: '40px 0 15px',
            position: 'relative',
            '&::before': {
                content: '""',
                width: '100%',
                position: 'absolute',
                zIndex: 1,
                top: '20px',
                left: 0,
                borderTop: '4px solid #d1a9a9',
            },
        }, text: {
            color: '#43a047',
            fontSize: 28,
            fontWeight: '700',
            marginLeft: '100px',
            backgroundColor: '#323232',
            'zIndex': 2,
            position: 'relative',
            padding: '0 15px',
        }, image: {
            width: '100%',
            height: '200px',
            objectFit: 'cover',
            borderRadius: '10px'
        }, listItem: {
            padding: '0 40px',
            maxWitdh: '50%'
        }, item: {
            height: '100%',
            borderRadius: '5px',
            border: '6px solid #43a04782',
            color: '#ffffff96',
            margin: '10px 0',
            padding: '10px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
            '&:hover': {
                border: '6px solid #43a047',
                color: '#fff',
            }
        }, textContent: {
            marginTop: 40,
            position: 'relative',
            '&::before': {
                content: '""',
                width: '60%',
                position: 'absolute',
                zIndex: 1,
                top: '-15px',
                left: '20%',
                borderTop: '2px solid #43a04782',
            },
        }, textContentOne: {
            position: 'relative',
            '&::before': {
                content: '""',
                width: '60%',
                position: 'absolute',
                zIndex: 1,
                bottom: '-15px',
                left: '20%',
                borderTop: '2px solid #43a04782',
            },
        }
    })
})