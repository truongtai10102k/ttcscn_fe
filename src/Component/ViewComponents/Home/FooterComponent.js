import React from 'react';
import { Box, makeStyles, Grid, Typography, Container } from '@material-ui/core'
import Logo from '../../../image/logo.png'

const FooterComponent = () => {
    const classes = useStyles()
    return (
        <Box className={classes.container}>
            <Container>
                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <Grid container spacing={2} alignItems={'center'}>
                            <Grid item>
                                <Box className={classes.image}>
                                    <img
                                        src={Logo}
                                        style={{ width: '70px', height: '70px', borderRadius: '50%' }}
                                    />
                                </Box>
                                <Typography>© Công ty cho thuê xe ACTVN Car</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={8} className={classes.itemRight}>
                        <Grid container spacing={2}>
                            <Grid item>
                                <Typography>Địa Chỉ: Học viện Kỹ Thuật Mật Mã</Typography>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <Typography>Mã số thuế : MT-0001</Typography>
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Email : actvncar123@gmail.com</Typography>
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Điện thoại: 0123456789</Typography>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <Typography>Ngân hàng MB Bank - CN Hà Nội</Typography>
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>STK : 1234567890</Typography>
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Tên tài khoản : CT ACTVN Car </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Container>
        </Box>
    );
}

export default FooterComponent;

const useStyles = makeStyles((themes) => {
    return ({
        container: {
            color: '#fff',
            position: 'relative',
            padding: '5px 10px 40px',
            '&::before': {
                content: '""',
                width: '82%',
                position: 'absolute',
                zIndex: 1,
                top: '-20px',
                left: '9%',
                borderTop: '2px solid #d1a9a9',
            },
        }, textBox: {
            display: 'flex',
            justifyContent: 'space-between'
        }, itemRight: {
            marginTop: 12
        }, image: {
            display: 'flex',
            justifyContent: 'center'

        }
    })
})