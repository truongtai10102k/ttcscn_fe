import React, { useState } from 'react';
import { Box, TextField, Button, makeStyles, withStyles } from '@material-ui/core';
import BackgroundHome from '../../../image/bgHome.png'
import SlideComponent from './SlideComponent';
import CommentComponent from './CommentComponent';
import GoogleMap from '../GoogleMap/GoogleMap';
import FooterComponent from '../Home/FooterComponent';
import CarItemComponent from '../CarView/CarItemComponent';
import { Redirect, useHistory } from 'react-router-dom';
const HomeComponent = (props) => {
    const classes = useStyles()
    const { listDataEU } = props
    return (
        <Box className={classes.containerHome}>
            <Box className={classes.header}>
                <Box className={classes.headerItem}>

                    <Box className={classes.searchContainer}>
                        <SearchBox />
                    </Box>
                </Box>
            </Box>
            <Box>
                <SlideComponent />
            </Box>
            <Box className={classes.carItem}>
                <CarItemComponent listDataEU={listDataEU} title={'Xe Nổi Bật'} />
            </Box>
            <Box>
                <CommentComponent />
            </Box>
            <Box className={classes.googleMap}>
                <GoogleMap height={'400px'} width={'600px'} />
            </Box>
            <FooterComponent />
        </Box>
    );
}

export default HomeComponent;


const useStyles = makeStyles((themes) => {
    return ({
        containerHome: {
            width: '100%',
            backgroundColor: 'rgba(0,0,0,0.8)'
        }, header: {
            backgroundImage: `url(${BackgroundHome})`,
            backgroundRepeat: "none",
            backgroundSize: "cover",
            width: '100%',
            height: '92vh',
            top: 0,
            left: 0,
            // position: 'absolute',
            padding: 0,
            '&.MuiContainer-maxWidthLg': {
                maxWidth: '100%',
            }
        }, headerItem: {
            width: '750px',
            margin: '0 auto',
            padding: '3rem 2rem 3rem',
            background: 'rgba(0,0,0,.35)',
            position: 'relative',
            top: '20%'
        },
        searchBox: {
            width: '90%',
            // display: 'flex',
            margin: '0 auto',
            backgroundColor: 'rgba(0,0,0,.4)',
            padding: '2rem 0rem 4rem'
        }, searchGroup: {
            display: 'flex',
            width: '90%',
            margin: '0 auto'
        }
        , text: {
            color: '#fff',
            textAlign: 'center',
            fontSize: 26,
            fontWeight: 'bold',
            marginBottom: 30
        }, textField: {
            width: '90%',
            marginLeft: 'auto',
            marginRight: 'auto',
            paddingBottom: 0,
            marginTop: 0,
            fontWeight: 500,
            color: 'white',
            borderColor: 'white'
        },
        input: {
            color: 'white'
        }, multilineColor: {
            color: 'white'
        }, googleMap: {
            width: '100%',
            margin: '0 auto'
        }, carItem: {
            marginTop: 125
        }
    })
})


const CssTextField = withStyles({
    root: {
        color: '#43a047',
        '& label.Mui-focused': {
            color: 'green',
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: 'green',
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: '#43a047',
            },
            '&:hover fieldset': {
                borderColor: '#43a047',
            },
            '&.Mui-focused fieldset': {
                borderColor: 'green',
            },
        },
    },
})(TextField);
const SearchBox = () => {
    const [textSearch, setTextSearch] = useState('');
    const classes = useStyles()
    let history = useHistory();
    const redirectTo = () => {
        history.push({
            pathname: '/viewcar',
            state: { textSearch: textSearch }
        })
    }
    return (
        <Box className={classes.searchBox}>
            <Box className={classes.textGroup}>
                <Box className={classes.text}>ACTVN CAR - Bên Bạn Mọi Hành trình</Box>
                <Box className={classes.text}>Bắt Đầu Nào</Box>
            </Box>
            <Box className={classes.searchGroup}>
                <CssTextField
                    label='Tìm kiếm xe... '
                    variant="outlined"
                    id="custom-css-outlined-input"
                    className={classes.textField}
                    fullWidth
                    color='primary'
                    InputProps={{
                        className: classes.multilineColor
                    }}
                    InputLabelProps={{
                        style: {
                            textOverflow: 'ellipsis',
                            whiteSpace: 'nowrap',
                            overflow: 'hidden',
                            width: '100%',
                            color: '#43a047'
                        }
                    }}
                    onChange={(e) => setTextSearch(e.target.value)}
                />
                <Button
                    variant='contained' color='primary' style={{ width: '105px', marginLeft: '15px' }}
                    onClick={redirectTo}
                >Tìm Kiếm</Button>
            </Box>
        </Box>
    )
}