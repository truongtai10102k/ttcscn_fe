import React from 'react';
import { Box, Typography, makeStyles } from '@material-ui/core';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
const SlideComponent = (props) => {
    const classes = useStyle()
    const data = [
        {
            url: 'https://hyundaiotocaugiay.vn/wp-content/uploads/2021/06/Hyundai-Santa-Fe-2021-5-scaled.jpg',
            name: 'Huyndai Santafe',
            price: '900000'
        },
        {
            url: 'https://data.1freewallpapers.com/download/lexus-ls-500h-2021-4k-5k-cars-2560x1440.jpg',
            name: 'Lexus ls500h',
            price: '550.000đ'
        },
        {
            url: 'https://i.pinimg.com/originals/47/31/8b/47318be20a076d7afe3dd15a9fda6422.jpg',
            name: 'Vinfast Lux A2.0',
            price: '760000'
        },
        {
            url: 'https://hyundaiotocaugiay.vn/wp-content/uploads/2021/06/Hyundai-Santa-Fe-2021-5-scaled.jpg',
            name: 'Huyndai Santafe',
            price: '900000'
        },
        {
            url: 'https://data.1freewallpapers.com/download/lexus-ls-500h-2021-4k-5k-cars-2560x1440.jpg',
            name: 'Lexus ls500h',
            price: '550.000đ'
        },
        {
            url: 'https://i.pinimg.com/originals/47/31/8b/47318be20a076d7afe3dd15a9fda6422.jpg',
            name: 'Vinfast Lux A2.0',
            price: '760000'
        },
        {
            url: 'https://hyundaiotocaugiay.vn/wp-content/uploads/2021/06/Hyundai-Santa-Fe-2021-5-scaled.jpg',
            name: 'Huyndai Santafe',
            price: '900000'
        },
        {
            url: 'https://data.1freewallpapers.com/download/lexus-ls-500h-2021-4k-5k-cars-2560x1440.jpg',
            name: 'Lexus ls500h',
            price: '550.000đ'
        },
        {
            url: 'https://i.pinimg.com/originals/47/31/8b/47318be20a076d7afe3dd15a9fda6422.jpg',
            name: 'Vinfast Lux A2.0',
            price: '760000'
        },
    ]
    let listItem = []
    if (data.length > 0) {
        listItem = data.map((item, idx) => {
            return (
                <div className={classes.item} key={idx}>
                    <img className={classes.image} src={item.url} />
                    <p className="legend">{item.name} , giá : {item.price} / ngày</p>
                </div>
            )
        })
    }
    return (
        <Box className={classes.container}>
            <Box className={classes.textBorder}>
                <Typography component={'span'} className={classes.text}>
                    Xe Hot Tháng 9
                </Typography>
            </Box>
            <Box className={classes.carouselContainer}>
                <Carousel
                    className={classes.carousel}
                    centerMode={true}
                    swipeable={true}
                    width={'100%'}
                    autoPlay={true}
                    transitionTime={1500}
                    infiniteLoop={true}
                    interval={3000}
                    stopOnHover={true}
                // renderItem={3}
                >
                    {listItem}
                </Carousel>
            </Box>

        </Box>
    );
}

export default SlideComponent;


const useStyle = makeStyles((themes) => {
    return ({
        container: {
            display: 'block',
            height: '100%'
        }, image: {
            width: '100%',
            height: '100%',
            objectFit: 'unset'
        }, textBorder: {
            margin: '15px 0 ',
            position: 'relative',
            '&::before': {
                content: '""',
                width: '100%',
                position: 'absolute',
                zIndex: 1,
                top: '20px',
                left: 0,
                borderTop: '4px solid #d1a9a9',
            },
        }, text: {
            color: '#43a047',
            fontSize: 28,
            fontWeight: '700',
            marginLeft: '100px',
            backgroundColor: '#323232',
            'zIndex': 2,
            position: 'relative',
            padding: '0 15px',
        }, carousel: {
            height: '100%',
        }, item: {
            width: '100%',
            height: '600px',
            overflow: 'hidden',
        }, carouselContainer: {
            height: '600px',
            margin: '0 auto'
        }
    })
})