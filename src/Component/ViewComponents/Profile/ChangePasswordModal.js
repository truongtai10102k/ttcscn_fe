import React, { useState } from 'react';
import Local from '../../../localStorage';
import { Box, TextField, Button, makeStyles, Dialog } from '@material-ui/core';
const ChangePasswordModal = (props) => {
    const { open, close, putAccount } = props

    const [oldPassword, setOldPassword] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    const handleChange = (e, setState) => {
        setState(e.target.value)
    }

    const handleSubmit = () => {
        const user = Local.getToken();
        if (password.length < 6 || oldPassword.length < 6) {
            alert('Mật khẩu dài hơn 6 kí tự');
            return;
        }
        if (password === oldPassword) {
            alert('Mật khẩu cũ phải khác mật khẩu mới')
            return;
        }
        if (password !== confirmPassword) {
            alert("Mật khẩu không khớp")
            return;
        } else {
            putAccount({ userName: user.user.userName, password, currentPassword: oldPassword })
            close()
        }

    }

    const classes = useStylesDialog()
    return (
        <Dialog open={open} onClose={close}>
            <Box className={classes.contaier}>
                <Box className={classes.text}> Thay Đổi Mật Khẩu</Box>
                <Box className={classes.listInput}>
                    <TextField type='password' size='small' fullWidth variant='outlined' label='Nhập Mật Khẩu Cũ' onChange={(e) => handleChange(e, setOldPassword)} />
                    <TextField type='password' className={classes.input} size='small' fullWidth variant='outlined' label='Nhập Mật Khẩu Mới' onChange={(e) => handleChange(e, setPassword)} />
                    <TextField type='password' className={classes.input} size='small' fullWidth variant='outlined' label='Nhập Lại Mật Khẩu Mới' onChange={(e) => handleChange(e, setConfirmPassword)} />
                </Box>
                <Box className={classes.buttonGroup}>
                    <Button variant='contained' color='primary'
                        onClick={handleSubmit}
                    >Xác Nhận</Button>
                    <Button className={classes.buttonItem} color='#414141' variant='contained'
                        onClick={close}
                    >Hủy</Button>
                </Box>
            </Box>
        </Dialog>
    )
}
export default ChangePasswordModal;
const useStylesDialog = makeStyles({
    contaier: {
        width: 400,
        height: 330
    }, text: {
        padding: '20px 0',
        textAlign: 'center',
        fontSize: 18,
        fontWeight: 600,
        borderBottom: '1px solid #000'
    }, listInput: {
        padding: '20px 20px'
    },
    input: {
        margin: '10px 0'
    },
    buttonGroup: {
        display: 'flex',
        justifyContent: 'space-evenly',
    },
});