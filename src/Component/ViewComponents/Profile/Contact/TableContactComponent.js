import React, { useState, useEffect } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {
    IconButton, Table,
    TableBody, TableContainer,
    TableCell, TableHead,
    TableRow, Paper, Box,
    TextField, Button
} from '@material-ui/core'
import {
    Visibility as ViewIcon,
    Done as DoneIcon,
    NotInterested as ProhibitIcon,
    Description as ExportIcon
} from "@material-ui/icons"
import DialogContract from './ContactDialog';
import Pagination from '@material-ui/lab/Pagination'
import localStorageWorker from '../../../../localStorage';

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    }
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        }, '& > *': {
            marginTop: theme.spacing(1),
        },
    },
}))(TableRow);

const useStyles = makeStyles({
    tableContainer: {
        maxHeight: "69vh"
    },
    table: {
        minWidth: 700,

    },
    tableHead: {
        backgroundColor: "#43a047",
        borderRight: "1px solid #fff"

    },
    tableCell: {
        borderRight: "1px solid #43a047a6",
    }, pagination: {
        display: "flex",
        justifyContent: 'center',
        marginTop: '10px'
    }, searchBox: {
        paddingBottom: 10
    }, inputSearch: {
        maxWidth: 350,
        marginRight: 20
    }
});

export default function CustomizedTables(props) {
    const { getCarRequest, listData, putContact, type, getContactClinet, tx, activePage, totalPage, carDetail } = props
    const [openDialog, setOpenDialog] = useState(false);
    const [data, setData] = useState({});
    const [textSearch, setTextSearch] = useState('')
    const classes = useStyles();

    const hanldeCloseDialog = () => {
        setOpenDialog(false)
    }

    return (
        <Box>
            <Box className={classes.searchBox}>
                <TextField fullWidth className={classes.inputSearch} label='Tìm Kiếm ...' size="small" variant='outlined'
                    onChange={(e) => setTextSearch(e.target.value)}
                />
                <Button variant="contained" color="primary"
                    onClick={() => {
                        getContactClinet({ page: 1, textSearch: textSearch, status: type })
                    }}
                >Tìm Kiếm</Button>
            </Box>
            <TableContainer className={classes.tableContainer} component={Paper}>
                <Table stickyHeader className={classes.table}>
                    <TableHead className={classes.head} >
                        <TableRow>
                            <StyledTableCell className={classes.tableHead} align="center" width="5%" >STT</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="12.5%">Mã Hợp Đồng</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="15%">Ngày Giờ</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="27.5%">Tên Khách Hàng</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="15%">Tên Xe</StyledTableCell>
                            <StyledTableCell className={classes.tableHead} align="center" width="25%">Hành Động</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {listData && listData.map((row, index) => (
                            <StyledTableRow key={index} onClick={() => {
                                // alert(row.name_car)
                            }}>
                                <StyledTableCell component="th" scope="row" className={classes.tableCell} width="5%" align="center">
                                    {index + 1}
                                </StyledTableCell>
                                <StyledTableCell className={classes.tableCell} align="center" width="12.5%" >{row?.contractId}</StyledTableCell>
                                <StyledTableCell className={classes.tableCell} align="center" width="15%">{new Date(+row?.dateRegester).toLocaleString()}</StyledTableCell>
                                <StyledTableCell className={classes.tableCell} align="center" width="27.5%">{row?.customerName}</StyledTableCell>
                                <StyledTableCell className={classes.tableCell} align="center" width="15%">{row?.name_car}</StyledTableCell>
                                <StyledTableCell align="center" width="25%">
                                    <IconButton color="primary" component="span"
                                        onClick={() => {
                                            setData(row)
                                            setOpenDialog(true)
                                            getCarRequest({ id: row?.id_car })
                                        }}>
                                        <ViewIcon />
                                    </IconButton>
                                </StyledTableCell>
                            </StyledTableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <Pagination
                className={classes.pagination}
                color="secondary"
                count={totalPage}
                page={activePage}
                onChange={(e, number) => {
                    getContactClinet({ userName: localStorageWorker.getToken().user.userName, page: number, status: type, textSearch: tx })
                }}
            />
            <DialogContract
                openContract={openDialog}
                closeContract={hanldeCloseDialog}
                data={data}
                putContact={putContact}
                type={type}
                carDetail={carDetail}
            />
        </Box >
    );
}