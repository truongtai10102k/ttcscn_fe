import React from 'react';
import { Box, List, ListItem, ListItemText, ListItemIcon, makeStyles, Grid, Button } from "@material-ui/core";
import { BrowserRouter, Switch, Route, Link, NavLink } from "react-router-dom";
import {
    CollectionsBookmarkOutlined as Contract,
    PersonOutline as AccountIcon,
} from "@material-ui/icons"
import ProfileInfoComponent from './ProfileInfoComponent';
import ContactInfoComponent from './Contact/ContactInfoComponent';
const ListRouter = [
    {
        path: "/me",
        label: "Thông Tin Tài Khoản",
        component: ProfileInfoComponent,
        exact: true,
        icon: AccountIcon
    },
    {
        path: "/me/contact",
        label: "Hợp Đồng",
        component: ContactInfoComponent,
        exact: true,
        icon: Contract
    },
]

const useStyles = makeStyles((themes) => {
    return {
        container: {
            display: "flex",
            // backgroundColor: 'rgba(0,0,0,0.8)'
        },
        nav: {
            backgroundColor: themes.palette.primary.main,
            maxWidth: "300px",
            color: "#fff",
            fontWeight: "500",
            margin: themes.spacing(1, 0),
            // height: "100vh",
            '&.active': {
                backgroundColor: "#414141",
            }

        },
        icon: {
            color: "#fff",
        },
        listItem: {
            height: "91.7vh",
            backgroundColor: themes.palette.primary.main,

        }, logout: {
            width: "100%",
            fontWeight: "500",
            fontSize: 15,
            position: "absolute",
            bottom: "20px",
            color: "#defff7"
        },
        userName: {
            backgroundColor: themes.palette.primary.main,
            textAlign: "center",
            padding: "12px 0",
            fontSize: "16px",
            fontWeight: "700",
            color: "#defff7"
        }
    }
})

const Index = (props) => {
    const classes = useStyles()
    const renderNavbar = (item, index) => {
        return (
            <ListItem
                className={classes.nav}
                button
                key={index}
                exact={item.exact.toString()}
                to={item.path}
                component={NavLink}
                activeClassName={'active'}
            >
                <ListItemIcon className={classes.icon} activeClassName={'active'}>
                    <item.icon />
                </ListItemIcon>
                <ListItemText>
                    {item.label}
                </ListItemText>

            </ListItem>
        )
    }

    const renderRoute = (item, index) => {
        return (
            <Route key={index} path={item.path} exact={item.exact} component={item.component} />
        )
    }

    return (
        <BrowserRouter>
            <Box className={classes.container}>
                <Grid xs={2}>
                    <List className={classes.listItem}>
                        {ListRouter.map(renderNavbar)}
                    </List>
                </Grid>
                <Grid xs={10}>
                    <Switch>
                        {ListRouter.map(renderRoute)}
                    </Switch>
                </Grid>
            </Box>
        </BrowserRouter>
    );
}

export default Index;


