import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import ChangePasswordModal from './ChangePasswordModal';
import { connect } from "react-redux"
import {
    activeAccountRequest, deactiveAccountRequest, deleteAccountRequest,
    postAccountRequest, putAccountRequest, getAccountRequest, postAccountAdminRequest,
    updateAccountEURequest
} from '../../../actions/index'
import Local from '../../../localStorage';
const ProfileInfoComponent = (props) => {
    const classes = useStyles();
    const {   updateAccountEURequest, putAccount } = props
    let user = Local.getToken().user

    const [customerName, setCustomerName] = useState(user.CustomerName ? user.CustomerName : '');
    const [userName, setUserName] = useState(user.userName ? user.userName : '');
    const [phone, setPhone] = useState(user.phone ? user.phone : '');
    const [email, setEmail] = useState(user.email ? user.email : '');

    const [isEdit, setIsEdit] = useState(true);
    const [openChangePw, setOpenChangePw] = useState(false);
    function validateEmail(email) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    const handleClose = () => {
        setOpenChangePw(false)
    }


    const handleSubmit = () => {
        if (!validateEmail(email)) {
            alert("vui lòng nhập đúng định dang mail")
            return;
        }
        updateAccountEURequest({ CustomerName: customerName, phone: phone, email: email, userName: userName })
    }


    const handleChange = (e, setState) => {
        setState(e.target.value)
    }
    return (
        <div className={classes.paper}>
            <Typography component="h1" variant="h5">
                Thông Tin Tài Khoản
            </Typography>
            <Box className={classes.form}>
                <Grid container spacing={2}>
                    <Grid item xs={12} >
                        <TextField
                            autoComplete="fname"
                            name="firstName"
                            variant="outlined"
                            required
                            fullWidth
                            id="firstName"
                            label="Họ và Tên"
                            autoFocus
                            size="small"
                            onChange={(e) => {
                                handleChange(e, setCustomerName)
                            }}
                            disabled={isEdit}
                            value={customerName}
                        />
                    </Grid>
                    {/* <Grid item xs={12} sm={6}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            id="lastName"
                            label="Last Name"
                            name="lastName"
                            autoComplete="lname"
                            size="small"
                            onChange={(e) => {
                                handleChange(e, setLastName)
                            }}
                            disabled={isEdit}
                        />
                    </Grid> */}
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            label="Tên Đăng Nhập"
                            size="small"
                            value={userName}
                            onChange={(e) => {
                                handleChange(e, setUserName)
                            }}
                            disabled
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            disabled={isEdit}
                            value={phone}
                            label="Số Điện Thoại"
                            type="Number"
                            size="small"
                            onChange={(e) => {
                                handleChange(e, setPhone)
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            id="email"
                            label="Email"
                            name="email"
                            autoComplete="email"
                            value={email}
                            size="small"
                            disabled={isEdit}
                            type="email"
                            onChange={(e) => {
                                handleChange(e, setEmail)
                            }}
                        />
                    </Grid>

                </Grid>

                <Grid container justifyContent='space-evenly'>
                    <Grid item>
                        {
                            isEdit === true
                                ?
                                <Button
                                    // fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={classes.submit}
                                    onClick={() => { setIsEdit(false) }}
                                >
                                    Sửa Thông tin
                                </Button>
                                :
                                <Button
                                    // fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={classes.submit}
                                    onClick={() => {
                                        setIsEdit(true)
                                        handleSubmit()
                                    }}
                                >
                                    Xác Nhận
                                </Button>
                        }
                    </Grid>
                    <Grid item>

                        <Button
                            className={classes.submit}
                            variant="contained" onClick={() => {
                                setOpenChangePw(true)
                            }}>
                            Đổi Mật khẩu
                        </Button>
                    </Grid>
                </Grid>
            </Box>
            <ChangePasswordModal putAccount={putAccount} open={openChangePw} close={handleClose} />
        </div>
    );
}

const mapStateToProps = (state) => {
    return ({
        listData: state.account.listData,
        activePage: state.account.activePage,
        totalPage: state.account.totalPage,
        tx: state.account.textSearch,
        isLoading: state.account.isLoading,

    })
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateAccountEURequest: (data) => {
            dispatch(updateAccountEURequest(data));
        },
        putAccount: (data) => {
            dispatch(putAccountRequest(data));
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProfileInfoComponent)

const useStyles = makeStyles((theme) => ({
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        maxWidth: "450px",
        height: "500px",
        margin: 'auto',
        marginTop: "3%",
        backgroundColor: '#fafafa',
        padding: "15px 40px 15px",
        borderRadius: "10px",
        position: 'relative',
        zIndex: 2,
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    }
}));