import React, { Component, PropTypes } from 'react';

// download html2canvas and jsPDF and save the files in app/ext, or somewhere else
// the built versions are directly consumable
// import {html2canvas, jsPDF} from 'app/ext';
import html2canvas from 'html2canvas';
import { jsPDF } from 'jspdf';
import { Dialog, Button, makeStyles, Box, withStyles } from '@material-ui/core';

const useStyles = theme => ({
    header1: {
        textAlign: 'center',
        fontSize: 12,
        lineHeight: 1,
    }, header2: {
        textAlign: 'center',
        fontSize: 10,
        lineHeight: 1,
    }, header3: {
        textAlign: 'left',
        fontSize: 10,
        lineHeight: 1,
    }, header4: {
        textAlign: 'left',
        fontSize: 10,
        fontWeight: 600,
        lineHeight: 1,
    }, italic: {
        textAlign: 'center',
        fontSize: 8,
        lineHeight: 1,
        fontStyle: 'italic',
        display: 'block',
        paddingBottom: '2mm'
    }, italic2: {
        textAlign: 'left',
        fontSize: 10,
        lineHeight: 1,
        fontStyle: 'italic',
        width: '160mm',
        display: 'inline-block'
    }, text: {
        textAlign: 'left',
        fontSize: 12,
        lineHeight: 1.2,
        fontStyle: 'italic',
        width: '160mm',
    }, textDate: {
        textAlign: 'left',
        fontSize: 12,
        lineHeight: 1,
        fontStyle: 'italic',
        width: '160mm',
        textAlign: 'right',
        paddingRight: '10mm'
    }, groupSign: {
        padding: '0mm 25mm',
        display: 'flex',
        justifyContent: 'space-between',
        alignItem: 'center'
    }
});
class Export extends Component {
    constructor(props) {
        super(props);
    }


    printDocument() {
        const input = document.getElementById('divToPrint');
        html2canvas(input)
            .then((canvas) => {
                const imgData = canvas.toDataURL('image/png');
                const pdf = new jsPDF();
                pdf.addImage(imgData, 'JPEG', 0, 0);
                // pdf.output('dataurlnewwindow');
                pdf.save("Hợp Đồng.pdf");
            })
            ;
    }
    render() {
        const { open, close, classes, data, carDetail } = this.props
        var d = new Date();
        var n = d.getMonth() + 1;
        var date = d.getDate();
        return (
            <Dialog open={open} onClose={close}>
                <Box style={{
                    width: 350,
                    height: 200
                }}>
                    <Box style={{
                        fontSize: 18,
                        fontWeight: 600,
                        textAlign: 'center',
                        padding: '40px 0'
                    }} >Xác Nhận Xuất PDF</Box>
                    <Box style={{
                        display: 'flex',
                        justifyContent: 'space-evenly',
                        marginTop: 20
                    }}>
                        <Button variant='contained' color='primary' className="mb5"
                            onClick={this.printDocument}
                        >Xuất PDF</Button>
                        <Button style={{ padding: '0 40px' }} color='#414141' variant='contained'
                            onClick={close}
                        >Hủy</Button>
                    </Box>
                </Box>


                <div id="divToPrint" className="mt4" style={{
                    // width: '210mm',
                    // minHeight: '3000mm',
                    margin: '0 auto',
                    // display: 'none',
                    position: 'absolute',
                    zIndex: -1
                }}>
                    <div style={{
                        padding: '0 20px'
                    }}>
                        <h1 className={classes.header1} >CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</h1>
                        <h2 className={classes.header2} >Độc Lập – Tự Do – Hạnh Phúc</h2>
                        <h2 className={classes.header2} >HỢP ĐỒNG THUÊ XE</h2>
                        <span className={classes.italic}>Số: {data.contractId ? data.contractId : '01'}–HĐTX</span>
                        <span className={classes.italic2}>
                            - Căn cứ Bộ Luật Dân sự số 33/2005/QH 11 đã được Quốc Hội nước Cộng Hòa Xã Hội Chủ Nghĩa Việt Nam khóa XI, kỳ họp thứ 7 thông qua ngày 14/06/2005;<br />
                            - Căn cứ luật thương mại số 36/2005/QH 11 đã được Quốc Hội nước Cộng Hòa Xã Hội Chủ Nghĩa Việt Nam khóa XI, kỳ họp thứ 7 thông qua ngày 14/06/2005;<br />
                            - Căn cứ vào nhu cầu và khả năng cung ứng của các bên dưới đây.
                        </span>
                        <div className={classes.text}>Hôm nay, ngày {date} tháng {n} năm 2021, chúng tôi gồm :</div>
                        <div className={classes.text} style={{ padding: '3mm 0' }} >
                            <div> BÊN A: (Bên cho thuê)</div>
                            <div>
                                <div className={classes.text}>- Địa chỉ: Chiến Thắng, Tân Triều, Hà Nội</div>
                                <div className={classes.text}>- Đại diện: Ông Nguyễn Văn A    </div>
                                <div className={classes.text}>- Chức vụ: Giám đốc</div>
                                <div className={classes.text}>- Mã số thuế: THUE001</div>
                            </div>
                        </div>
                        <div>
                            <div className={classes.text}> BÊN B: (Bên thuê)</div>
                            <div>
                                <div className={classes.text}>- Họ và Tên :{data.customerName ? data.customerName : 'Nguyễn Văn B'} </div>
                                <div className={classes.text}>- Số CCCD/CMT: {data.idCard ? data.idCard : '12345678 '}     </div>
                                <div className={classes.text}>- Ngày Cấp : {data.issueDate ? data.issueDate : '11/09/21'} </div>
                                <div className={classes.text} F>- Địa chỉ : {data.address ? data.address : 'Vinh, Nghệ An '} </div>
                            </div>
                        </div>
                        <div className={classes.text}>Sau khi bàn bạc, thỏa thuận, hai bên thống nhất ký kết Hợp đồng thuê xe với các điều khoản như sau:</div>
                        <h3 className={classes.header3}>ĐIỀU 1 : NỘI DUNG HỢP ĐỒNG</h3>
                        <div className={classes.text}>Bên B đồng ý thuê của bên A thuê một xe ô tô.+ Xe {carDetail.name_car_company ? carDetail.name_car_company : 'Honda '} {carDetail.name ? carDetail.name : 'Civic '}   <br />Sản xuất năm : {carDetail.manufacture ? carDetail.manufacture : '2021 '} ,<br /> Biển số kiểm soát:  {carDetail.licensePlate ? carDetail.licensePlate : '37A1234 '} </div>
                        <h3 className={classes.header3}>ĐIỀU 2 : GIÁ TRỊ HỢP ĐỒNG, PHƯƠNG THỨC THANH TOÁN:</h3>
                        <div className={classes.text}>- Giá thuê xe là:  {carDetail.price ? carDetail.price : '100.000'}đồng/ngày</div>
                        <div className={classes.text}>-  Giá bảo hiểm là:  {carDetail.insuranceFee ? carDetail.insuranceFee : '100.000'}đồng/ngày</div>
                        <div className={classes.text}>- Thuê từ ngày {data.startDate ? data.startDate : '29/09/2021'}   đến  {data.endDate ? data.endDate : '30/09/2021'} </div>
                        <div className={classes.text}>- Tổng số tiền phải thanh toán :  {data.money ? data.money : '100000'}  đồng</div>
                        <div className={classes.text}>- Bên B sẽ thanh toán cho Bên A theo (Hình thức thanh toán) .</div>
                        <h3 className={classes.header3}>ĐIỀU 3 : TRÁCH NHIỆM CỦA CÁC BÊN</h3>
                        <h4 className={classes.header4}>3.1. Trách nhiệm của bên A:</h4>
                        <div className={classes.text}>- Giao xe và toàn bộ giấy tờ liên quan đến xe ngay sau khi Hợp đồng có hiệu lực và  Giấy tờ liên quan đến xe gồm: Giấy đăng ký xe, giấy kiểm định, giấy bảo hiểm xe.</div>
                        <div className={classes.text}>- Chịu trách nhiệm pháp lý về nguồn gốc và quyền sở hữu của xe.</div>
                        <div className={classes.text}>- Mua bảo hiểm xe và đăng kiểm xe cho các lần kế tiếp trong thời hạn hiệu lực của Hợp đồng.</div>
                        <h4 className={classes.header4}>3.2. Trách nhiệm, quyền hạn của bên B</h4>
                        <div className={classes.text}>- Thanh toán tiền thuê xe cho Bên A đúng hạn</div>
                        <div className={classes.text}>- Chịu toàn bộ chi phí xăng dầu khi sử dụng xe.</div>
                        <h4 className={classes.header3}>ĐIỀU 4 : ĐIỀU KHOẢN CHUNG</h4>
                        <div className={classes.text}>- Hai bên cam kết thi hành đúng các điều khoản của hợp đồng, không bên nào tự ý đơn phương sửa đổi, đình chỉ hoặc hủy bỏ hợp đồng. Mọi sự vi phạm phải được xử lý theo pháp luật. </div>
                        <div className={classes.text}>- Hợp đồng này có hiệu lực từ ngày ký và coi như được thanh lý sau khi hai bên thực hiện xong nghĩa vụ của mình và không còn bất kỳ khiếu nại nào.</div>
                        <div className={classes.text}>- Hợp đồng được lập thành 02 (bốn) bản có giá trị pháp lý như nhau, Bên A giữ 01 bản. Bên B giữ 01 bản.</div>
                        <h4 className={classes.header4}>Hợp Đồng có hiệu lực từ ngày kí ! </h4>
                        <div className={classes.textDate}>Hà Nội, ngày {date} tháng {n} năm 2021</div>
                        <div className={classes.groupSign}>
                            <h5 >ĐẠI DIỆN BÊN A</h5>
                            <h5 >ĐẠI DIỆN BÊN B</h5>
                        </div>
                    </div>
                </div>
            </Dialog >);
    }
}
export default withStyles(useStyles)(Export)



