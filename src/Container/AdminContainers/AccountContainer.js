import React, { useEffect } from 'react';
import { connect } from "react-redux"
import {
    activeAccountRequest, deactiveAccountRequest, deleteAccountRequest,
    postAccountRequest, putAccountRequest, getAccountRequest, postAccountAdminRequest
} from '../../actions/index'
import AccountComponent from '../../Component/AdminComponents/AccountComponent/AccountComponent';
import FullScreenLoading from '../../Component/Components/Loading/FullScreenLoading';
const AccountContainer = (props) => {
    const { getAccount, isLoading } = props
    useEffect(() => {
        getAccount({ page: 1, role: 'admin' })
    }, []);
    return (
        <div>
            {/* {
                isLoading
                    ? */}
                    {/* <FullScreenLoading /> */}
                    {/* : */}
                    <AccountComponent {...props} />
            {/* } */}
        </div>
    );
}

const mapStateToProps = (state) => {
    return ({
        listData: state.account.listData,
        activePage: state.account.activePage,
        totalPage: state.account.totalPage,
        tx: state.account.textSearch,
        isLoading: state.account.isLoading,

    })
}

const mapDispatchToProps = (dispatch) => {
    return {
        getAccount: (data) => {
            dispatch(getAccountRequest(data));
        },
        putAccount: (data) => {
            dispatch(putAccountRequest(data));
        },
        postAccount: (data) => {
            dispatch(postAccountRequest(data));
        },
        postAccountAdmin: (data) => {
            dispatch(postAccountAdminRequest(data));
        },
        deleteAccount: (data) => {
            dispatch(deleteAccountRequest(data));
        },
        activeAccount: (data) => {
            dispatch(activeAccountRequest(data));
        },
        deactiveAccount: (data) => {
            dispatch(deactiveAccountRequest(data));
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AccountContainer)
