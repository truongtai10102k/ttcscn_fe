import React, { useEffect } from 'react';
import CarComponent from '../../Component/AdminComponents/CarComponent/Index';
import { connect } from "react-redux"
import FullScreenLoading from "../../Component/Components/Loading/FullScreenLoading"
import { getCompanyRequest, getCarRequest, putCarRequest, postCarRequest, deleteCarRequest } from '../../actions/index'
const CarContainer = (props) => {
    const { getCar, getCompany, isLoading } = props
    useEffect(() => {
        getCar({ page: 1, id_car_company: '' })
        getCompany({ page: 1 })
    }, []);
    return (
        <div>
            {
                isLoading
                    ?
                    <FullScreenLoading />
                    :
                    <CarComponent {...props} />
            }
        </div>
    );
}

const mapStateToProps = (state) => {
    return ({
        listData: state.car.listData,
        listCompany: state.company.listData,
        activePage: state.car.activePage,
        totalPage: state.car.totalPage,
        tx: state.car.textSearch,
        company: state.car.idCompany,
        isLoading: state.car.isLoading,
    })
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCar: (data) => {
            dispatch(getCarRequest(data));
        },
        getCompany: (data) => {
            dispatch(getCompanyRequest(data));
        },
        putCar: (data) => {
            dispatch(putCarRequest(data));
        },
        postCar: (data) => {
            dispatch(postCarRequest(data));
        },
        deleteCar: (data) => {
            dispatch(deleteCarRequest(data));
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CarContainer);
