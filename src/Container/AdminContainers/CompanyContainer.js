import React, { useEffect } from 'react';
import { connect } from "react-redux"
import { getCompanyRequest, putCompanyRequest, deleteCompanyRequest, postCompanyRequest } from '../../actions/index'
import CompanyComponent from '../../Component/AdminComponents/CompanyComponent/Index';
import FullScreenLoading from '../../Component/Components/Loading/FullScreenLoading';
const CompanyContainer = (props) => {
    const { getCompany, isLoading } = props
    useEffect(() => {
        getCompany({ page: 1 })
    }, []);

    return (
        <div>
            {
                isLoading
                    ?
                    <FullScreenLoading />
                    :
                    <CompanyComponent {...props} />
            }
        </div>
    );
}

const mapStateToProps = (state) => {
    return ({
        listData: state.company.listData,
        activePage: state.company.activePage,
        totalPage: state.company.totalPage,
        tx: state.company.textSearch,
        isLoading: state.company.isLoading,
    })
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCompany: (data) => {
            dispatch(getCompanyRequest(data));
        },
        putCompany: (data) => {
            dispatch(putCompanyRequest(data));
        },
        postCompany: (data) => {
            dispatch(postCompanyRequest(data));
        },
        deleteCompany: (data) => {
            dispatch(deleteCompanyRequest(data));
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CompanyContainer);
