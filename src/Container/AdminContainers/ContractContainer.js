import React, { useEffect } from 'react';
import { getContactRequest, putContactRequest, getCarDetailEURequest } from '../../actions/index'
import ContractComponent from "../../Component/AdminComponents/contactComponent/ContractComponent"
import { connect } from "react-redux"
const ContractContainer = (props) => {
    const { getContact, isLoading } = props
    useEffect(() => {
        getContact({ page: 1, status: 'pendding' })
    }, []);

    return (
        <div>
            <ContractComponent {...props} />
        </div>
    );
}


const mapStateToProps = (state) => {
    return ({
        listData: state.contact.listData,
        activePage: state.contact.activePage,
        totalPage: state.contact.totalPage,
        isLoading: state.contact.isLoading,
        tx: state.contact.textSearch,
        carDetail: state.car.carDetail,
    })
}

const mapDispatchToProps = (dispatch) => {
    return {
        getContact: (data) => {
            dispatch(getContactRequest(data));
        },
        putContact: (data) => {
            dispatch(putContactRequest(data));
        }, getCarDetail: (data) => {
            dispatch(getCarDetailEURequest(data))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ContractContainer);
