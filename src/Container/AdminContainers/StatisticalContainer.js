import React, { useState, useEffect } from 'react';
import { Box, TextField, Button, makeStyles, Typography } from '@material-ui/core';
import { getStatisticalRequest, } from '../../actions/index'
import { connect } from "react-redux"
import * as xlsx from 'xlsx';
import FileSaver from 'file-saver';
import { DOMAIN, HTTP_HEADER_JSON } from '../../FetchAPI/callAPIConfig';
import FullScreenLoading from '../../Component/Components/Loading/FullScreenLoading';
const Statisticalcontainer = (props) => {
    const { getContact, listData } = props
    const classes = useStyles()
    const [start, setStart] = useState(Date.now());
    const [end, setEnd] = useState(Date.now());
    const [isLoading, setIsLoading] = useState(false);

    const ExportExcel = (csvData, fileName) => {

        if (csvData.length > 0) {
            let allMoney = 0
            let dataObj = csvData.map((item, idx) => {
                allMoney += (+item.money);
                return {
                    ['Mã Hợp Đồng']: item.contractId,
                    ['Tên  Khách Hàng']: item.customerName,
                    ['Địa Chỉ']: item.address,
                    ['Ngày Bắt Đầu Thuê']: item.startDate,
                    ['Ngày Kết Thúc Thuê']: item.endDate,
                    ['Tiền']: item.money,
                }
            })
            dataObj[dataObj.length] = { ['Tiền']: allMoney }
            const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
            const fileExtension = '.xlsx';
            const ws = xlsx.utils.json_to_sheet(dataObj);
            const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(wb, { bookType: 'xlsx', type: 'array' });
            const data = new Blob([excelBuffer], { type: fileType });
            FileSaver.saveAs(data, fileName + fileExtension);
        } else {
            alert("Không có hợp đồng thành công trong khoảng thời gian đã chọn")
        }
    }

    // handle export excel

    const exportFile = async (start, end) => {
        setIsLoading(true)
        const dataAll = await new Promise((resolve, reject) => {
            const token = localStorage.getItem("user") ? JSON.parse(localStorage.getItem("user")).token : ""
            const headers = {
                "Authorization": `Bearer ${token}`
            }

            let objFetch = {
                method: 'POST',
                headers: {
                    ...headers,
                    ...HTTP_HEADER_JSON
                },
                body: JSON.stringify({ start, end })
            }
            const url = DOMAIN + `admin/statistical/`
            fetch(url, objFetch)
                .then((response) => resolve(response.json()))
                .catch((error) => reject(error));
        })
        if (dataAll.data && dataAll?.data.contract) {
            await ExportExcel(dataAll.data.contract, "Thống Kê")
            setIsLoading(false)
        } else {
            alert("lỗi xuất excel")
            setIsLoading(false)
        }
    }
    return (
        <Box className={classes.container}>
            {
                isLoading
                    ?
                    <FullScreenLoading />
                    :
                    <Box>
                        <Typography
                            className={classes.title}
                            component='h1'
                        >Thống Kê Hợp Đồng</Typography>
                        <Box className={classes.textField}>
                            <TextField
                                type='date'
                                label='Ngày Bắt Đầu'
                                variant='outlined'
                                defaultValue="2017-05-24"
                                fullWidth
                                style={{ margin: '0 10px' }}
                                onChange={(e) => {
                                    setStart(Date.parse(e.target.value))
                                }}
                            />
                            <TextField
                                type='date'
                                label='Ngày Kết Thúc'
                                defaultValue="2017-05-24"
                                variant='outlined'
                                fullWidth
                                style={{ margin: '0 10px' }}
                                onChange={(e) => {
                                    setEnd(Date.parse(e.target.value))
                                }}
                            />
                        </Box>
                        <Box className={classes.button}>
                            <Button color='primary' variant='contained'
                                onClick={() => { exportFile(start, end) }}
                            >Xuất Excel</Button>
                        </Box>
                    </Box>
            }

        </Box>
    );
}

const mapStateToProps = (state) => {
    return ({
        listData: state.statistical.listData,
    })
}

const mapDispatchToProps = (dispatch) => {
    return {
        getContact: (data) => {
            dispatch(getStatisticalRequest(data));
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Statisticalcontainer);


const useStyles = makeStyles((theme) => {
    return ({
        container: {
            width: '60%',
            margin: '0 auto',
            marginTop: 50
        }, textField: {
            width: '100%',
            display: 'flex'
        }, button: {
            display: 'flex',
            justifyContent: 'flex-end',
            margin: 20
        }, title: {
            margin: '40px 0',
            width: '100%',
            textAlign: 'center',
            fontSize: 30
        }
    })
})