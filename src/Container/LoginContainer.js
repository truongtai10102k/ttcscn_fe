import React from 'react';
import { connect } from "react-redux"
import LoginComponent from '../Component/LoginComponent';
import { loginRequest, postAccountRequest, forgotPasswordRequest } from "../actions/index"
const LoginContainer = (props) => {
    return (
        <div>
            <LoginComponent {...props} />
        </div>
    );
}

const mapStateToProps = (state) => {
    return ({})
}

const mapDispatchToProps = (dispatch) => {
    return {
        loginDispatch: (data) => {
            dispatch(loginRequest(data));
        }, postAccount: (data) => {
            dispatch(postAccountRequest(data));
        }, forgotPassword: (data) => {
            dispatch(forgotPasswordRequest(data));
        },
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
