import React, { useEffect } from 'react';
import { Box } from '@material-ui/core';
import CarViewComponent from '../../Component/ViewComponents/CarView/CarViewComponent';
import { getCarEURequest, getCarSpotLightEURequest } from '../../actions/index'
import { connect } from "react-redux"


const CarUserContainer = (props) => {
    const { getCarRequest, getCarSpotLight } = props
    useEffect(() => {
        getCarRequest({ page: 1 })
        getCarSpotLight()
    }, []);

    return (
        <Box>
            <CarViewComponent{...props} />
        </Box>
    );
}


const mapStateToProps = (state) => {
    return ({
        listDataEU: state.car.listDataEU,
        listDataEUSpotLight: state.car.listDataEUSpotLight,
    })
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCarRequest: (data) => {
            dispatch(getCarEURequest(data))
        },
        getCarSpotLight: (data) => {
            dispatch(getCarSpotLightEURequest(data))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CarUserContainer);
