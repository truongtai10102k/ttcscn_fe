import React from 'react';
import ContactViewComponent from '../../Component/ViewComponents/ContactView/ContactViewComponent';
const ContactViewContainer = () => {
    return (
        <div>
            <ContactViewComponent />
        </div>
    );
}

export default ContactViewContainer;
