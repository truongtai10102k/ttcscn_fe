import React, { useEffect } from 'react';
import HomeComponent from '../../Component/ViewComponents/Home/HomeComponent';
import { getCarEURequest, } from '../../actions/index'
import { connect } from "react-redux"


const HomeContainer = (props) => {
    const { getCarRequest, } = props
    useEffect(() => {
        getCarRequest({ page: 1 })

    }, []);

    return (
        <div>
            <HomeComponent {...props} />
        </div>
    );
}


const mapStateToProps = (state) => {
    return ({
        listDataEU: state.car.listDataEU
    })
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCarRequest: (data) => {
            dispatch(getCarEURequest(data))
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
