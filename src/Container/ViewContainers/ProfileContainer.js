import React from 'react';
import { Box } from '@material-ui/core';
import ProfileComponent from '../../Component/ViewComponents/Profile/ProfileComponent';
const ProfileContainer = () => {
    return (
        <Box>
            <ProfileComponent />
        </Box>
    );
}

export default ProfileContainer;
