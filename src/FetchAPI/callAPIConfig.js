// domain
export const DOMAIN = "http://localhost:1337/";
export const DOMAIN_IMAGE = "http://localhost:1337/admin/uploadfile/";
export const DOMAIN_GET_IMAGE = "http://localhost:1337/";

// HTTP method 
export const HTTP_READ = "GET"
export const HTTP_CREATE = "POST"
export const HTTP_UPDATE = "PUT"
export const HTTP_DELETE = "DELETE"

// HTTP headers
export const HTTP_HEADER_JSON = {
    "Content-Type": "Application/json"
}