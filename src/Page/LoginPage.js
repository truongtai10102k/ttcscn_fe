import React from 'react';
import LoginContainer from '../Container/LoginContainer';

const LoginPage = () => {
    return (
        <div>
            <LoginContainer />
        </div>
    );
}

export default LoginPage;
