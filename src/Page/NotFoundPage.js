import React from 'react';
import { Box, colors, makeStyles, Button } from "@material-ui/core"
import { WarningOutlined } from "@material-ui/icons"
const NotFoundPage = () => {
    const classes = useStyles()
    return (
        <Box>
            <Box className={classes.container}>
                <WarningOutlined />Oops, Trang Này Không Tồn Tài
            </Box>
            <Button color="secondary" href = "/" >Quay về trang chủ </Button>
        </Box>
    );
}

export default NotFoundPage;

const useStyles = makeStyles((themes) => ({
    container: {
        color: colors.green[700],
        fontSize: 24,
        fontWeight: "bold",
        textAlign: "center",
        marginTop: "30px"
    }
}))