import React from 'react';
import AccountContainer from '../../Container/AdminContainers/AccountContainer';

const AccountPage = () => {
    return (
        <div>
            <AccountContainer />
        </div>
    );
}

export default AccountPage;
