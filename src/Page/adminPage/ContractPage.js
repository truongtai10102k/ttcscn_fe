import React from 'react';
import ContractContainer from '../../Container/AdminContainers/ContractContainer';

const ContractPage = () => {
    return (
        <div>
            <ContractContainer />
        </div>
    );
}

export default ContractPage;
