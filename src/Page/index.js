import LoginPage from "./LoginPage";
import NotFoundPage from "./NotFoundPage"
import ContractPage from "./adminPage/ContractPage"
import AccountPage from "./adminPage/AccountPage"
import CompanyPage from "./adminPage/CompanyPage"
import StatisticalPage from "./adminPage/StatisticalPage"
import CarPage from "./adminPage/CarPage"
import HomePage from "./viewPage/HomePage"
import CarUserPage from "./viewPage/CarUserPage"
import ProfilePage from "./viewPage/ProfilePage"
import ContactViewPage from "./viewPage/ContactPage"
export {
    LoginPage,
    NotFoundPage,
    // admin
    ContractPage,
    AccountPage,
    CompanyPage,
    CarPage,
    StatisticalPage,
    //end user
    HomePage,
    CarUserPage,
    ProfilePage,
    ContactViewPage
}