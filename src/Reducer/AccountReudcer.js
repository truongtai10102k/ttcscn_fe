import * as types from "../constants"
const DEFAULT_STATE = {
    listData: [],
    isLoading: false,
    error: false,
    message: "",
    dataFetched: false,
    activePage: 0,
    totalPage: 0,
    textSearch: '',
    role: 'admin'
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case types.GET_ACCOUNT_REQUEST:
        case types.POST_ACCOUNT_REQUEST:
        case types.POST_ADMIN_ACCOUNT_REQUEST:
        case types.PUT_ACCOUNT_REQUEST:
        case types.DELETE_ACCOUNT_REQUEST:
        case types.ACTIVE_ACCOUNT_REQUEST:
        case types.DEACTIVE_ACCOUNT_REQUEST:
            return ({
                ...state,
                isLoading: true
            })
        case types.GET_ACCOUNT_SUCCESS:
            return ({
                ...state,
                dataFetched: true,
                isLoading: false,
                listData: action.payload.listData,
                totalPage: action.payload.totalPage,
                textSearch: action.payload.textSearch,
                activePage: action.payload.activePage,
                role: action.payload.role
            })
        case types.POST_ACCOUNT_SUCCESS:
        case types.POST_ADMIN_ACCOUNT_SUCCESS:
        case types.PUT_ACCOUNT_SUCCESS:
        case types.DELETE_ACCOUNT_SUCCESS:
        case types.ACTIVE_ACCOUNT_SUCCESS:
        case types.DEACTIVE_ACCOUNT_SUCCESS:
            return ({
                ...state,
                dataFetched: true,
                isLoading: false,
            })
        case types.GET_ACCOUNT_FAILURE:
        case types.POST_ACCOUNT_FAILURE:
        case types.POST_ADMIN_ACCOUNT_FAILURE:
        case types.PUT_ACCOUNT_FAILURE:
        case types.DELETE_ACCOUNT_FAILURE:
        case types.ACTIVE_ACCOUNT_FAILURE:
        case types.DEACTIVE_ACCOUNT_FAILURE:
            return ({
                ...state,
                isLoading: false,
                error: true,
                message: action.payload.message
            })

        default:
            return state;
    }
}