import * as types from "../constants"
const DEFAULT_STATE = {
    listData: [],
    listDataSearch: [],
    listDataEU: [],
    listDataEUSpotLight: [],
    carDetail: {},
    isLoading: false,
    error: false,
    message: "",
    dataFetched: false,
    activePage: 0,
    totalPage: 0,
    textSearch: '',
    idCompany: ''
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case types.GET_CAR_REQUEST:
        case types.GET_CAR_EU_REQUEST:
        case types.GET_CAR_SPOT_LIGHT_EU_REQUEST:
        case types.GET_CAR_DETAIL_EU_REQUEST:
        case types.POST_CAR_REQUEST:
        case types.PUT_CAR_REQUEST:
        case types.DELETE_CAR_REQUEST:
        case types.SEARCH_CAR_EU_REQUEST:
            return ({
                ...state,
                isLoading: true
            })
        case types.GET_CAR_SUCCESS:
            return ({
                ...state,
                dataFetched: true,
                isLoading: false,
                listData: action.payload.listData,
                totalPage: action.payload.totalPage,
                textSearch: action.payload.textSearch,
                activePage: action.payload.activePage,
                idCompany: action.payload.idCompany,
            })
        case types.GET_CAR_SPOT_LIGHT_EU_SUCCESS:
            return ({
                ...state,
                dataFetched: true,
                isLoading: false,
                listDataEUSpotLight: action.payload.listData,
            })
        case types.SEARCH_CAR_EU_SUCCESS:
            return ({
                ...state,
                dataFetched: true,
                isLoading: false,
                listDataSearch: action.payload.listData,
                totalPage: action.payload.totalPage,
                textSearch: action.payload.textSearch,
                activePage: action.payload.activePage,
                idCompany: action.payload.idCompany,
            })
        case types.GET_CAR_DETAIL_EU_SUCCESS:
            return ({
                ...state,
                dataFetched: true,
                isLoading: false,
                carDetail: action.payload.listData,
            })
        case types.GET_CAR_EU_SUCCESS:
            return ({
                ...state,
                dataFetched: true,
                isLoading: false,
                listDataEU: action.payload.listData,
                totalPage: action.payload.totalPage,
                textSearch: action.payload.textSearch,
                activePage: action.payload.activePage,
                idCompany: action.payload.idCompany,
            })
        case types.POST_CAR_SUCCESS:
        case types.PUT_CAR_SUCCESS:
        case types.DELETE_CAR_SUCCESS:
            return ({
                ...state,
                dataFetched: true,
                isLoading: false,
            })
        case types.GET_CAR_FAILURE:
        case types.GET_CAR_SPOT_LIGHT_EU_FAILURE:
        case types.SEARCH_CAR_EU_FAILURE:
        case types.GET_CAR_EU_FAILURE:
        case types.GET_CAR_DETAIL_EU_FAILURE:
        case types.POST_CAR_FAILURE:
        case types.PUT_CAR_FAILURE:
        case types.DELETE_CAR_FAILURE:
            return ({
                ...state,
                isLoading: false,
                error: true,
                message: action.payload.message
            })

        default:
            return state;
    }
}