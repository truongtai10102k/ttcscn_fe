import * as types from "../constants"
const DEFAULT_STATE = {
    listData: [],
    isLoading: false,
    error: false,
    message: "",
    dataFetched: false,
    activePage : 0,
    totalPage : 0,
    textSearch : '',
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case types.GET_COMPANY_REQUEST:
        case types.POST_COMPANY_REQUEST:
        case types.PUT_COMPANY_REQUEST:
        case types.DELETE_COMPANY_REQUEST:
            return ({
                ...state,
                isLoading: true
            })
        case types.GET_COMPANY_SUCCESS:
            return ({
                ...state,
                dataFetched: true,
                isLoading: false,
                listData: action.payload.listData,
                totalPage: action.payload.totalPage,
                textSearch: action.payload.textSearch,
                activePage: action.payload.activePage,
            })
        case types.POST_COMPANY_SUCCESS:
        case types.PUT_COMPANY_SUCCESS:
        case types.DELETE_COMPANY_SUCCESS:
            return ({
                ...state,
                dataFetched: true,
                isLoading: false,
            })
        case types.GET_COMPANY_FAILURE:
        case types.POST_COMPANY_FAILURE:
        case types.PUT_COMPANY_FAILURE:
        case types.DELETE_COMPANY_FAILURE:
            return ({
                ...state,
                isLoading: false,
                error: true,
                message: action.payload.message
            })

        default:
            return state;
    }
}