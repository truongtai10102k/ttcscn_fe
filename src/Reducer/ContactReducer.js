import * as types from "../constants"
const DEFAULT_STATE = {
    listData: [],
    isLoading: false,
    error: false,
    message: "",
    dataFetched: false,
    activePage : 0,
    totalPage : 0,
    textSearch : '',
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case types.GET_CONTACT_REQUEST:
        case types.GET_CONTRACT_BY_EU_REQUEST:
        case types.POST_CONTACT_REQUEST:
        case types.PUT_CONTACT_REQUEST:
        case types.DELETE_CONTACT_REQUEST:
            return ({
                ...state,
                isLoading: true
            })
        case types.GET_CONTACT_SUCCESS: 
        case types.GET_CONTRACT_BY_EU_SUCCESS: 
            return ({
                ...state,
                dataFetched: true,
                isLoading: false,
                listData: action.payload.listData,
                totalPage: action.payload.totalPage,
                textSearch: action.payload.textSearch,
                activePage: action.payload.activePage,
            })
        case types.POST_CONTACT_SUCCESS:
        case types.PUT_CONTACT_SUCCESS:
        case types.DELETE_CONTACT_SUCCESS:
            return ({
                ...state,
                dataFetched: true,
                isLoading: false,
            })
        case types.GET_CONTACT_FAILURE:
        case types.GET_CONTRACT_BY_EU_FAILURE:
        case types.POST_CONTACT_FAILURE:
        case types.PUT_CONTACT_FAILURE:
        case types.DELETE_CONTACT_FAILURE:
            return ({
                ...state,
                isLoading: false,
                error: true,
                message: action.payload.message
            })

        default:
            return state;
    }
}