import * as types from "../constants"
const DEFAULT_STATE = {
    listData: [],
    isLoading: false,
    error: false,
    message: "",
    dataFetched: false
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case types.LOGIN_REQUEST:
        case types.FORGOT_PASSWORD_REQUEST:
            return ({
                ...state,
                isLoading: true
            })
        case types.LOGIN_SUCCESS:
        case types.FORGOT_PASSWORD_SUCCESS:
            return ({
                ...state,
                dataFetched: true,
                isLoading: false
            })
        case types.LOGIN_FAILURE:
        case types.FORGOT_PASSWORD_FAILURE:
            return ({
                ...state,
                isLoading: false,
                error: true,
                message: action.payload.message
            })

        default:
            return state;
    }
};