import { combineReducers } from "redux"
import LoginReducer from "./LoginReducer";
import ContactReducer from "./ContactReducer";
import AccountReudcer from "./AccountReudcer";
import CarReducer from "./CarReducer";
import CompanyReducer from "./CompanyReducer";
import statisticalReducer from "./statisticalReducer";
const rootReducer = combineReducers({
    login: LoginReducer,
    contact: ContactReducer,
    account: AccountReudcer,
    car: CarReducer,
    company: CompanyReducer,
    statistical: statisticalReducer,
})

export default rootReducer;