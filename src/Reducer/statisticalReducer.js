import * as types from "../constants"
const DEFAULT_STATE = {
    listData: [],
    isLoading: false,
    error: false,
    message: "",
    dataFetched: false,
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case types.STATISTICAL_REQUEST:
            return ({
                ...state,
                isLoading: true
            })
        case types.STATISTICAL_SUCCESS:
            return ({
                ...state,
                dataFetched: true,
                isLoading: false,
                listData: action.payload.listData,
            })
        case types.STATISTICAL_FAILURE:
            return ({
                ...state,
                isLoading: false,
                error: true,
                message: action.payload.message
            })

        default:
            return state;
    }
}