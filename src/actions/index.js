import * as types from "../constants"

export const loginRequest = (payload) => {
    return ({
        type: types.LOGIN_REQUEST,
        payload
    })
}
// hợp đồng
export const getContactRequest = (payload) => {
    return ({
        type: types.GET_CONTACT_REQUEST,
        payload
    })
}

export const postContactRequest = (payload) => {
    return ({
        type: types.POST_CONTACT_REQUEST,
        payload
    })
}

export const putContactRequest = (payload) => {
    return ({
        type: types.PUT_CONTACT_REQUEST,
        payload
    })
}

export const deleteContactRequest = (payload) => {
    return ({
        type: types.DELETE_CONTACT_REQUEST,
        payload
    })
}
// xe
export const getCarRequest = (payload) => {
    return ({
        type: types.GET_CAR_REQUEST,
        payload
    })
}

export const postCarRequest = (payload) => {
    return ({
        type: types.POST_CAR_REQUEST,
        payload
    })
}

export const putCarRequest = (payload) => {
    return ({
        type: types.PUT_CAR_REQUEST,
        payload
    })
}

export const deleteCarRequest = (payload) => {
    return ({
        type: types.DELETE_CAR_REQUEST,
        payload
    })
}
//hãng  xe
export const getCompanyRequest = (payload) => {
    return ({
        type: types.GET_COMPANY_REQUEST,
        payload
    })
}

export const postCompanyRequest = (payload) => {
    return ({
        type: types.POST_COMPANY_REQUEST,
        payload
    })
}

export const putCompanyRequest = (payload) => {
    return ({
        type: types.PUT_COMPANY_REQUEST,
        payload
    })
}

export const deleteCompanyRequest = (payload) => {
    return ({
        type: types.DELETE_COMPANY_REQUEST,
        payload
    })
}
// tài khoản
export const getAccountRequest = (payload) => {
    return ({
        type: types.GET_ACCOUNT_REQUEST,
        payload
    })
}

export const postAccountRequest = (payload) => {
    return ({
        type: types.POST_ACCOUNT_REQUEST,
        payload
    })
}

export const postAccountAdminRequest = (payload) => {
    return ({
        type: types.POST_ADMIN_ACCOUNT_REQUEST,
        payload
    })
}

export const putAccountRequest = (payload) => {
    return ({
        type: types.PUT_ACCOUNT_REQUEST,
        payload
    })
}

export const deleteAccountRequest = (payload) => {
    return ({
        type: types.DELETE_ACCOUNT_REQUEST,
        payload
    })
}
export const updateAccountEURequest = (payload) => {
    return ({
        type: types.UPDATE_ACCOUNT_EU_REQUEST,
        payload
    })
}

export const activeAccountRequest = (payload) => {
    return ({
        type: types.ACTIVE_ACCOUNT_REQUEST,
        payload
    })
}
export const deactiveAccountRequest = (payload) => {
    return ({
        type: types.DEACTIVE_ACCOUNT_REQUEST,
        payload
    })
}
// quên mật khẩu
export const forgotPasswordRequest = (payload) => {
    return ({
        type: types.FORGOT_PASSWORD_REQUEST,
        payload
    })
}

// EU

export const getCarEURequest = (payload) => {
    return ({
        type: types.GET_CAR_EU_REQUEST,
        payload
    })
}

export const getCarSpotLightEURequest = (payload) => {
    return ({
        type: types.GET_CAR_SPOT_LIGHT_EU_REQUEST,
        payload
    })
}

export const getCarDetailEURequest = (payload) => {
    return ({
        type: types.GET_CAR_DETAIL_EU_REQUEST,
        payload
    })
}
export const searchCarEURequest = (payload) => {
    return ({
        type: types.SEARCH_CAR_EU_REQUEST,
        payload
    })
}

export const getStatisticalRequest = (payload) => {
    return ({
        type: types.STATISTICAL_REQUEST,
        payload
    })
}

export const getContracByEURequest = (payload) => {
    return ({
        type: types.GET_CONTRACT_BY_EU_REQUEST,
        payload
    })
}