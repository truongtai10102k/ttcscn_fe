import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { createStore, applyMiddleware, } from "redux"
import { Provider } from "react-redux"
// create saga middleware

import createSageMiddleware from 'redux-saga';

import rootReducer from './reducer';
import rootSaga from './saga';
import reduxLogger from "redux-logger"
// connect store + apply middleware
const sagaMiddleware = createSageMiddleware()
const store = createStore(rootReducer, applyMiddleware(reduxLogger,sagaMiddleware))

sagaMiddleware.run(rootSaga)

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
