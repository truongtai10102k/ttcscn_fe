import React from 'react';
import { Box, List, ListItem, ListItemText, ListItemIcon, makeStyles, Grid, Button } from "@material-ui/core";
import { BrowserRouter, Switch, Route, Link, NavLink } from "react-router-dom";
import {
    CollectionsBookmarkOutlined as Contract,
    LocalTaxiOutlined as CarIcon,
    EmojiTransportationOutlined as CarCompanyIcon,
    SupervisorAccountOutlined as AccountIcon,
    Pages,
    LibraryBooksOutlined as StatisticalIcon
} from "@material-ui/icons"
import localStorage from '../localStorage';
import * as Page from "../Page/index"

const Home = () => <Box>Home</Box>
const Demo = () => <Box>Demo</Box>
const Logout = () => {
    const classes = useStyles()
    const handleClick = () => {
        localStorage.clearToken();
        window.location.href = window.location.origin
    }
    return (
        <Button className={classes.logout} onClick={handleClick}>Đăng Xuất</Button>
    )
}
const ListRouter = [
    {
        path: "/dashboard",
        label: "Hợp Đồng",
        component: Page.ContractPage,
        exact: true,
        icon: Contract
    },
    {
        path: "/car",
        label: "Xe",
        component: Page.CarPage,
        exact: true,
        icon: CarIcon
    },
    {
        path: "/car-company",
        label: "Hãng Xe",
        component: Page.CompanyPage,
        exact: true,
        icon: CarCompanyIcon
    },
    {
        path: "/statistical",
        label: "Thống Kê",
        component: Page.StatisticalPage,
        exact: true,
        icon: StatisticalIcon
    },
    {
        path: "/account",
        label: "Tài Khoản",
        component: Page.AccountPage,
        exact: true,
        icon: AccountIcon
    },
]

const useStyles = makeStyles((themes) => {
    return {
        container: {
            display: "flex",
        },
        nav: {
            backgroundColor: themes.palette.primary.main,
            maxWidth: "300px",
            color: "#fff",
            fontWeight: "500",
            margin: themes.spacing(1, 0),
            // height: "100vh",
            '&.active': {
                backgroundColor: "#414141",
            }

        },
        icon: {
            color: "#fff",
        },
        listItem: {
            height: "100vh",
            backgroundColor: themes.palette.primary.main,

        }, logout: {
            width: "100%",
            fontWeight: "500",
            fontSize: 15,
            position: "absolute",
            bottom: "20px",
            color: "#defff7"
        },
        userName: {
            backgroundColor: themes.palette.primary.main,
            textAlign: "center",
            padding: "12px 0",
            fontSize: "16px",
            fontWeight: "700",
            color: "#defff7"
        }
    }
})

const Index = (props) => {
    const classes = useStyles()
    const renderNavbar = (item, index) => {
        return (
            <ListItem
                className={classes.nav}
                button
                key={index}
                exact={item.exact.toString()}
                to={item.path}
                component={NavLink}
                activeClassName={'active'}
            >
                <ListItemIcon className={classes.icon} activeClassName={'active'}>
                    <item.icon />
                </ListItemIcon>
                <ListItemText>
                    {item.label}
                </ListItemText>

            </ListItem>
        )
    }

    const renderRoute = (item, index) => {
        return (
            <Route key={index} path={item.path} exact={item.exact} component={item.component} />
        )
    }

    return (
        <BrowserRouter>
            <Box className={classes.container}>
                <Grid xs={2}>
                    <List className={classes.listItem}>
                        <Box className={classes.userName}>Xin Chào : {localStorage.getToken().user.CustomerName} </Box>
                        {ListRouter.map(renderNavbar)}
                        <Logout />
                    </List>
                </Grid>
                <Grid xs={10}>
                    <Switch>
                        {ListRouter.map(renderRoute)}
                    </Switch>
                </Grid>
            </Box>
        </BrowserRouter>
    );
}

export default Index;


