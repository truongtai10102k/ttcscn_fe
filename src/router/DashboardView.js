import React, { useState, useEffect, useRef } from 'react';
import { Box, List, ListItem, ListItemText, makeStyles, Button, IconButton } from "@material-ui/core";
import { BrowserRouter, Switch, Route, Link, NavLink } from "react-router-dom";
import Logo from '../image/logo.png'
import localStorage from '../localStorage';
import * as Page from "../Page/index"
import { ExitToAppOutlined as Login } from '@material-ui/icons';
const Home = () => <Box>Home</Box>
const Demo = () => <Box>Demo</Box>
const Logout = () => {
    localStorage.clearToken();
    window.location.href = window.location.origin
}
const ListRouter = [
    {
        path: "/home",
        label: "Trang Chủ",
        component: Page.HomePage,
        exact: true,
        // icon: Contract
    },
    {
        path: "/viewcar",
        label: "Xe",
        component: Page.CarUserPage,
        exact: false,
        // icon: CarIcon
    },
    {
        path: "/me",
        label: "Tôi",
        component: Page.ProfilePage,
        exact: false,
        // icon: AccountIcon
    },
]
const useStyles = makeStyles((themes) => {
    return {
        container: {
            // display: "flex",
        },
        nav: {
            backgroundColor: 'transparent',
            // backgroundColor: '#365a37',
            display: 'flex',
            width: "120px",
            color: "#fff",
            fontWeight: "500",
            // height: "100vh",
            textAlign: 'center',
            '&.active': {
                color: "#414141",
                backgroundColor: '#43a047'
            }

        },
        icon: {
            color: "#fff",
        },
        listItem: {
            // backgroundColor: '#365a37',
            // marginTop: '20px',
            height: "60px",
            display: 'flex',
            padding: 0,
            display: 'flex',
            justifyContent: 'space-around',
            position: '-webkit-sticky',
            zIndex: 3,
            position: 'sticky',
            top: '0px',
            width: '100%',
            // backgroundColor: '#365a37'
            backgroundColor: '#000000e6'
        }, sticky: {
            height: "80px",
            display: 'flex',
            padding: 0,
            display: 'flex',
            justifyContent: 'space-around',
            position: '-webkit-sticky',
            zIndex: 3,
            position: 'fixed',
            top: 0,
            left: 0,
            width: '100%',
            transition: '0.5s linear',
            background: '#365a37',
        }, logout: {
            // width: "100%",
            fontWeight: "500",
            fontSize: 15,
            // position: "absolute",
            bottom: "20px",
            color: "#defff7"
        },
        logo: {
            margin: '6px 0',
            textAlign: "center",
        }, siteBar: {
            display: 'flex',
            // width: '60%'
        }, user: {
            display: 'flex',
            height: '30px',
            textAlign: 'center',
            marginTop: '15px',
        },
        userName: {
            color: '#fff',
            fontWeight: '700',
            textDecoration: 'none',
            '&:hover': {
                color: '#ffd6d6'
            }
        }
    }
})

const DashboardView = (props) => {
    const classes = useStyles()
    const [position, setPosition] = useState(0);
    const myRef = useRef(null);
    // useEffect(() => {
    //     console.log("yyy : ",window.scroll );
    // }, [window.scroll]);

    const onScroll = () => {
        const scrollY = window.scrollY //Don't get confused by what's scrolling - It's not the window
        const scrollTop = myRef.current.scrollTop
       

    }

    const renderNavbar = (item, index) => {
        return (
            <ListItem
                className={classes.nav}
                button
                key={index}
                exact={item.exact.toString()}
                to={item.path}
                component={NavLink}
                activeClassName={'active'}
            >
                {/* <ListItemIcon className={classes.icon} activeClassName={'active'}>
                    <item.icon />
                </ListItemIcon> */}
                <ListItemText>
                    {item.label}
                </ListItemText>

            </ListItem>
        )
    }

    const renderRoute = (item, index) => {
        return (
            <Route key={index} path={item.path} exact={item.exact} component={item.component} />
        )
    }
    return (
        <BrowserRouter >
            <Box ref={myRef} className={classes.container} onScroll={onScroll}>
                <List className={window.scrollY < 40 ? classes.listItem : classes.sticky}>
                    <Box component={NavLink} to={'/home'} className={classes.logo}>
                        <img src={Logo} height='50px' style={{ borderRadius: '50%' }} />
                    </Box>
                    <Box className={classes.siteBar}>
                        {ListRouter.map(renderNavbar)}
                    </Box>
                    <Box className={classes.user}>
                        <Box component={NavLink} to={'/me'} className={classes.userName}>Xin Chào, 
                        {localStorage.getToken().user.CustomerName}
                        {/* user */}
                        </Box>
                        <IconButton className={classes.logout} style={{ bottom: '5px', left: '10px' }}
                            onClick={Logout}
                        >
                            <Login />
                        </IconButton>
                    </Box>
                </List>
                <Box>
                    <Switch>
                        {ListRouter.map(renderRoute)}
                    </Switch>
                </Box>
            </Box>
        </BrowserRouter >
    );
}

export default DashboardView;


