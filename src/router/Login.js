import { Route, Redirect } from "react-router-dom"
import React from 'react';

const ProtectedLogin = ({ component: Component, isAuth, isAdmin, ...rest }) => {
    return (
        <Route
            {...rest}
            render={() => {
                if (!isAuth) {
                    return (
                        <Component />
                    )
                } else {
                    if (isAdmin) {
                        return (
                            <Redirect to="/dashboard" />
                        )
                    }else{
                        return(
                            <Redirect to="/home" />
                        )
                    }
                    
                }
            }}
        />
    )
}

export default ProtectedLogin