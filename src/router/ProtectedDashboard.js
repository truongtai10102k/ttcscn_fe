import { Route, Redirect } from "react-router-dom"
import React from 'react';

const ProtectedDashboard = ({ component: Component, isAuth, isAdmin, ...rest }) => {
    return (
        <Route
            exact
            {...rest}
            render={() => {
                if (isAuth) {
                    if (isAdmin) {
                        return (
                            <Component />
                        )
                    } else {
                        alert("Bạn không có quyền vào role này")
                        window.location.href = window.location.origin
                    }
                } else {
                    return (
                        <Redirect to="/login" />
                    )
                }
            }}
        />
    )
}

export default ProtectedDashboard