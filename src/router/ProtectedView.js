import { Route, Redirect } from "react-router-dom"
import React from 'react';

const ProtectedDashboard = ({ component: Component, isAuth, ...rest }) => {
    return (
        <Route
            {...rest}
            render={() => {
                if (isAuth) {
                    return (
                        <Component />
                    )
                } else {
                    alert('Bạn Không đủ quyền vào role này')
                    return (
                        <Redirect to="/login" />
                    )
                }
            }}
        />
    )
}

export default ProtectedDashboard