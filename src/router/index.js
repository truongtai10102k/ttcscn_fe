import React from 'react';
import { BrowserRouter, Switch, Router, Redirect, Route } from 'react-router-dom';
import * as page from '../Page';
import history from '../history'
import localStorage from '../localStorage';
import Dashboard from './Dashboard';
import DashboardView from './DashboardView';
import ContactDashbroard from './ContactDashbroard';
import ProtectedLogin from './Login';
import ProtectedDashboard from './ProtectedDashboard';
import ProtectedView from './ProtectedView';
import { NotFoundPage } from "../Page"

const Routes = () => {
  const user = localStorage.getToken()
  const role = user?.user && user?.user.rule ? user?.user.rule : "";
  const status = user?.user && user?.user.status ? user?.user.status : "";
  return (
    <div className="content">
      <BrowserRouter>
        <Router history={history}>
          <Switch>
            <Route exact path="/" render={() => {
              if (!user) {
                return (
                  <Redirect to="/login" />
                )
              } else {
                if (status === 'active') {
                  if (role === "admin") {
                    return (
                      <Redirect to="/dashboard" />
                    )
                  } else {
                    return (
                      <Redirect to="/home" />
                    )
                  }
                } else {
                  localStorage.clearToken();
                  alert(`Tài khoản của bạn đang bị khóa.\nVui lòng liên hệ với Admin để mở Tài khoản
                  `);
                  window.location.href=window.location.origin
                }
              }
            }} />
            <ProtectedLogin
              isAuth={user ? true : false}
              isAdmin={role === "admin" ? true : false}
              path="/login"
              component={page.LoginPage}
            />

            <ProtectedDashboard
              isAuth={user ? true : false}
              isAdmin={role === "admin" ? true : false}
              path="/dashboard"
              component={Dashboard}
            />
            <ProtectedDashboard
              isAuth={user ? true : false}
              isAdmin={role === "admin" ? true : false}
              path="/car"
              component={Dashboard}
            />
            <ProtectedDashboard
              isAuth={user ? true : false}
              isAdmin={role === "admin" ? true : false}
              path="/car-company"
              component={Dashboard}
            />
            <ProtectedDashboard
              isAuth={user ? true : false}
              isAdmin={role === "admin" ? true : false}
              path="/account"
              component={Dashboard}
            />
            <ProtectedDashboard
              isAuth={user ? true : false}
              isAdmin={role === "admin" ? true : false}
              path="/statistical"
              component={Dashboard}
            />

            <ProtectedView
              isAuth={user ? true : false}
              path="/home"
              component={DashboardView}
            />
            <ProtectedView
              isAuth={user ? true : false}
              path="/viewcar"
              component={DashboardView}
            />
            <ProtectedView
              isAuth={user ? true : false}
              path="/viewcarcompany"
              component={DashboardView}
            />
            <ProtectedView
              isAuth={user ? true : false}
              path="/me"
              component={DashboardView}
            />

            <Route
              exact
              path="/contact/:carId?"
              component={ContactDashbroard}
            />

            <Route
              exact
              path="*"
              component={NotFoundPage}
            />

          </Switch>
        </Router>
      </BrowserRouter>

    </div>
  )
}

export default Routes;
