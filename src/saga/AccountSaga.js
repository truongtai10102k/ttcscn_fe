import { takeEvery, put, select } from "redux-saga/effects"
import * as types from '../constants';
import callAPI from "../FetchAPI/callAPI";
import * as configAPI from '../FetchAPI/callAPIConfig'

function* getAccountSaga(action) {
    try {
        // call API

        let { page, role, textSearch } = action.payload
        const skip = types.skip(page);
        if (textSearch === undefined || textSearch === null) {
            textSearch = ''
        }
        //skip : bỏ qua phần tử
        // role : lấy tài khoản loại nào (user /admin)
        // textSearch : khóa tìm kiếm
        const res = yield callAPI(configAPI.HTTP_CREATE, `admin/getlistuserdata`, {
            textSeach: textSearch, skip: skip, rule: role
        });
        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)

        } else {
            yield put({
                type: types.GET_ACCOUNT_SUCCESS,
                payload: {
                    textSearch,
                    activePage: page,
                    listData: res.data.data,
                    totalPage: Math.ceil(res.data.totalPage / 10),
                    role: role
                }
            })
        }
    } catch (error) {
        yield put({
            type: types.GET_ACCOUNT_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

function* addAccountSaga(action) {
    try {
        // call API
        const res = yield callAPI(configAPI.HTTP_CREATE, "user/createAccount/", action.payload);
        // fake user
        let role = yield select((state) => state.account.role)

        yield put({
            type: types.POST_ACCOUNT_SUCCESS,
            payload: res
        })

        if (res.message === 'userName') {
            alert("Tên đăng nhập đã được đăng kí")
            throw new Error(res)
        } else {
            if (res.message === 'email') {
                alert("Email đã được đăng kí")
                throw new Error(res)
            } else {
                if (res.message === 'phone') {
                    alert("Số điện thoại đã được đăng kí")
                    throw new Error(res)
                } else {
                    if (window.location.pathname.match("login")) {
                        yield alert("Tạo tài khoản thành công. Vui lòng đăng nhập để tiếp tục !!!")
                    } else {
                        yield put({
                            type: types.GET_ACCOUNT_REQUEST,
                            payload: {
                                page: 1,
                                role: role
                            }
                        })
                    }
                }
            }
        }
    } catch (error) {
        console.log("🚀 ~ file: AccountSaga.js ~ line 78 ~ function*addAccountSaga ~ error", error)
        yield put({
            type: types.POST_ACCOUNT_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}
function* addAccountAdminSaga(action) {
    try {
        // call API
        const res = yield callAPI(configAPI.HTTP_CREATE, "user/createAdminAccount/", action.payload);
        // fake user
        let role = yield select((state) => state.account.role)

        yield put({
            type: types.POST_ADMIN_ACCOUNT_SUCCESS,
            payload: res
        })

        if (res?.data && res?.data === 'email invalid') {
            alert("Email đã được đăng kí")
            throw new Error(res)
        }
        if (res?.data && res?.data === 'userName invalid') {
            alert("Tên đăng nhập đã được đăng kí")
            throw new Error(res)
        }
        if (res?.data && res?.data === 'phone invalid') {
            alert("Số điện thoại đã được đăng kí")
            throw new Error(res)
        }
        if (res?.errorMess) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)
        }
        yield put({
            type: types.GET_ACCOUNT_REQUEST,
            payload: {
                page: 1,
                role: role
            }
        })

    } catch (error) {
        yield put({
            type: types.POST_ADMIN_ACCOUNT_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}
// chưa connect API

function* putAccountSaga(action) {

    try {
        // call API
        let res = yield callAPI("POST", "admin/changepassword/", action.payload);
        if (res.errorCode) {
            if (res.errorMess === 'Errorpassword wrong!!') {
                alert("Sai Mật Khẩu")
            }
            if (res.errorMess === 'Erroruser not defined') {
                alert("Người Dùng không tồn tại")
            }
            throw new Error(res)
        } else {
            yield put({
                type: types.PUT_ACCOUNT_SUCCESS,
                payload: res
            })
            alert("Đổi Mật Khẩu Thành Công")
        }
    } catch (error) {
        yield put({
            type: types.PUT_ACCOUNT_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}


function* deactiveAccountSaga(action) {
    try {
        // call API
        const data = {
            id: action.payload,
            // status: false
        }

        const res = yield callAPI(configAPI.HTTP_CREATE, "admin/blockaccount/", data);
        // fake user
        let role = yield select((state) => state.account.role)

        // yield put({
        //     type: types.GET_ACCOUNT_REQUEST,
        //     payload: {
        //         page: 1
        //     }
        // })
        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)

        } else {
            yield put({
                type: types.DEACTIVE_ACCOUNT_SUCCESS,
                payload: 'hhih'
            })
            alert('Khóa tài khoản thành công!!')
            yield put({
                type: types.GET_ACCOUNT_REQUEST,
                payload: {
                    page: 1,
                    role: role
                }
            })
        }
    } catch (error) {
        yield put({
            type: types.DEACTIVE_ACCOUNT_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}
function* activeAccountSaga(action) {
    try {
        // call API
        const data = {
            id: action.payload,
            // status: true
        }
        let role = yield select((state) => state.account.role)

        const res = yield callAPI(configAPI.HTTP_CREATE, "admin/unblockaccount/", data);
        // fake user
        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)

        } else {
            yield put({
                type: types.ACTIVE_ACCOUNT_SUCCESS,
                payload: res
            })
            alert('Mở khóa tài khoản thành công!!')
            yield put({
                type: types.GET_ACCOUNT_REQUEST,
                payload: {
                    page: 1,
                    role: role
                }
            })
        }

        // yield put({
        //     type: types.GET_ACCOUNT_REQUEST,
        //     payload: {
        //         page: 1
        //     }
        // })
    } catch (error) {
        yield put({
            type: types.ACTIVE_ACCOUNT_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}
function* deleteAccountSaga(action) {
    try {
        // call API
        // yield callAPI(GET ,"/ád",{} );
        // fake user
        yield put({
            type: types.DEACTIVE_ACCOUNT_SUCCESS,
            payload: 'hhih'
        })

        yield put({
            type: types.GET_ACCOUNT_REQUEST,
            payload: {
                page: 1
            }
        })
    } catch (error) {
        yield put({
            type: types.DEACTIVE_ACCOUNT_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}
function* updateAccountEuSaga(action) {
    try {
        // call API
        const res = yield callAPI(configAPI.HTTP_CREATE, "user/update/", action.payload);

        // fake user
        yield put({
            type: types.UPDATE_ACCOUNT_EU_SUCCESS,
            payload: res
        })
        yield alert("Cập Nhật Thành Công")

    } catch (error) {
        yield put({
            type: types.UPDATE_ACCOUNT_EU_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

function* forgotPasswordSaga(action) {
    try {
        // call API
        const res = yield callAPI(configAPI.HTTP_CREATE, "user/forgotpassword/", { email: action.payload });

        // fake user

        if (res.errorMess === "ErrorKhong ton tai email") {
            alert("Email này chưa đăng kí tài khoản ")
            throw new Error(res)
        } else if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)
        } else {
            yield put({
                type: types.FORGOT_PASSWORD_SUCCESS,
            })
            yield alert(`Mật Khẩu đã được gửi vào mail của bạn!!! 
Vui lòng kiểm tra mail và đăng nhập lại!!!`)
        }

    } catch (error) {
        yield put({
            type: types.FORGOT_PASSWORD_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

export const AccountSaga = [
    takeEvery(types.GET_ACCOUNT_REQUEST, getAccountSaga),
    takeEvery(types.POST_ACCOUNT_REQUEST, addAccountSaga),
    takeEvery(types.POST_ADMIN_ACCOUNT_REQUEST, addAccountAdminSaga),
    takeEvery(types.PUT_ACCOUNT_REQUEST, putAccountSaga),
    takeEvery(types.DEACTIVE_ACCOUNT_REQUEST, deactiveAccountSaga),
    takeEvery(types.ACTIVE_ACCOUNT_REQUEST, activeAccountSaga),
    takeEvery(types.DELETE_ACCOUNT_REQUEST, deleteAccountSaga),
    takeEvery(types.FORGOT_PASSWORD_REQUEST, forgotPasswordSaga),
    takeEvery(types.UPDATE_ACCOUNT_EU_REQUEST, updateAccountEuSaga),
]