import { takeEvery, put } from "redux-saga/effects"
import * as types from '../constants';
import callAPI from "../FetchAPI/callAPI";
import * as configAPI from '../FetchAPI/callAPIConfig'

function* getCarSaga(action) {
    try {
        let { page, textSearch, id_car_company } = action.payload
        const skip = types.skip(page);
        if (textSearch === undefined || textSearch === null) {
            textSearch = ''
        }
        if (id_car_company === undefined || id_car_company === null) {
            textSearch = ''
        }
        const res = yield callAPI(configAPI.HTTP_CREATE, `admin/getlistcardata`, {
            id_car_company: id_car_company, skip: skip, textSearch: textSearch
        });

        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)
        } else {
            yield put({
                type: types.GET_CAR_SUCCESS,
                payload: {
                    textSearch,
                    activePage: page,
                    listData: res.data.data,
                    totalPage: Math.ceil(res.data.totalPage / 10),
                    idCompany: id_car_company
                }
            })
        }

    } catch (error) {
        yield put({
            type: types.GET_CAR_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}


function* addCarSaga(action) {
    try {

        // call API
        const res = yield callAPI(configAPI.HTTP_CREATE, `admin/createcar/`, action.payload);

        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)
        } else {
            // fake user
            yield put({
                type: types.POST_CAR_SUCCESS,
                payload: res
            })

            yield put({
                type: types.GET_CAR_REQUEST,
                payload: {
                    page: 1
                }
            })
        }


    } catch (error) {
        yield put({
            type: types.POST_CAR_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}


function* putCarSaga(action) {
    try {
        // call API
        const res = yield callAPI(configAPI.HTTP_CREATE, `admin/updatecar/`, action.payload.data);

        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)
        } else {
            yield put({
                type: types.PUT_CAR_SUCCESS,
                payload: res
            })

            yield put({
                type: types.GET_CAR_REQUEST,
                payload: {
                    page: 1
                }
            })
        }

        // fake user

    } catch (error) {
        yield put({
            type: types.PUT_CAR_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}


function* deleteCarSaga(action) {
    try {
        // call API
        const res = yield callAPI(configAPI.HTTP_CREATE, `admin/deletecar`, {
            id: action.payload
        });

        if (res.message === 'pendding contact') {
            alert("Xe đang được cho thuê")
            throw new Error(res)
        }
        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)
        }
        if (res.message === 'delete success') {
            yield put({
                type: types.DELETE_CAR_SUCCESS,
                payload: res
            })

            yield put({
                type: types.GET_CAR_REQUEST,
                payload: {
                    page: 1
                }
            })
        }
        yield put({
            type: types.DELETE_CAR_SUCCESS,
            payload: res
        })

        yield put({
            type: types.GET_CAR_REQUEST,
            payload: {
                page: 1
            }
        })
    } catch (error) {
        yield put({
            type: types.DELETE_CAR_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

/// EU


function* getCarEUSaga(action) {
    try {
        let { page, textSearch, id_car_company } = action.payload
        const skip = types.skip(page);
        if (textSearch === undefined || textSearch === null) {
            textSearch = ''
        }
        if (id_car_company === undefined || id_car_company === null) {
            textSearch = ''
        }
        const res = yield callAPI(configAPI.HTTP_CREATE, `admin/getlistcardata`, {
            id_car_company: id_car_company, skip: skip, textSearch: textSearch
        });

        console.log("ress ", res);

        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)
        } else {
            yield put({
                type: types.GET_CAR_EU_SUCCESS,
                payload: {
                    listData: res.data.data,
                    totalPage: Math.ceil(res.data.totalPage / 10),
                    textSearch,
                    activePage: page,
                    idCompany: id_car_company
                }
            })
        }


    } catch (error) {
        yield put({
            type: types.GET_CAR_EU_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

function* getCarDetailEUSaga(action) {
    try {
        const res = yield callAPI(configAPI.HTTP_CREATE, `user/getonecar/`, action.payload);

        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)
        } else {
            yield put({
                type: types.GET_CAR_DETAIL_EU_SUCCESS,
                payload: {
                    listData: res.data,
                }
            })
        }

    } catch (error) {
        yield put({
            type: types.GET_CAR_DETAIL_EU_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

function* getCarSpotLightSaga(action) {
    try {
        const res = yield callAPI(configAPI.HTTP_CREATE, `user/getCarSpotLight/`, action.payload);

        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)
        } else {
            yield put({
                type: types.GET_CAR_SPOT_LIGHT_EU_SUCCESS,
                payload: {
                    listData: res,
                }
            })
        }

    } catch (error) {
        yield put({
            type: types.GET_CAR_SPOT_LIGHT_EU_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

function* searchCarEUSaga(action) {
    try {
        const { textSeach, seat, company, page } = action.payload;
        const skip = types.skip(page);
        const res = yield callAPI(configAPI.HTTP_CREATE, `user/getCarSeacrh/`, {
            textSeach, seat: seat && seat.name ? seat.name : '', company: company && company.id ? company.id : '',
            skip: skip
        });

        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)
        } else {
            yield put({
                type: types.SEARCH_CAR_EU_SUCCESS,
                payload: {
                    listData: res.listCar,
                    textSearch: textSeach,
                    activePage: page,
                    totalPage: Math.ceil(res.count / 10),
                    idCompany: company
                }
            })
        }

    } catch (error) {
        yield put({
            type: types.SEARCH_CAR_EU_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}




export const CarSaga = [
    takeEvery(types.GET_CAR_REQUEST, getCarSaga),
    takeEvery(types.GET_CAR_SPOT_LIGHT_EU_REQUEST, getCarSpotLightSaga),
    takeEvery(types.POST_CAR_REQUEST, addCarSaga),
    takeEvery(types.PUT_CAR_REQUEST, putCarSaga),
    takeEvery(types.DELETE_CAR_REQUEST, deleteCarSaga),
    takeEvery(types.GET_CAR_EU_REQUEST, getCarEUSaga),
    takeEvery(types.GET_CAR_DETAIL_EU_REQUEST, getCarDetailEUSaga),
    takeEvery(types.SEARCH_CAR_EU_REQUEST, searchCarEUSaga),
]