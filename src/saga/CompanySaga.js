import { takeEvery, put } from "redux-saga/effects"
import * as types from '../constants';
import callAPI from "../FetchAPI/callAPI";
import * as configAPI from '../FetchAPI/callAPIConfig'

function* getCompanySaga(action) {
    try {
        // call API

        let { page, textSearch } = action.payload
        const skip = types.skip(page);
        if (textSearch === undefined || textSearch === null) {
            textSearch = ''
        }
        const res = yield callAPI(configAPI.HTTP_CREATE, `admin/getcarfirmslist/`, { skip, textSearch });

        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)
        } else {
            yield put({
                type: types.GET_COMPANY_SUCCESS,
                payload: {
                    textSearch,
                    activePage: page,
                    listData: res.data.data,
                    totalPage: Math.ceil(res.data.totalPage / 10),
                }
            })
        }


    } catch (error) {
        yield put({
            type: types.GET_COMPANY_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

function* addCompanySaga(action) {
    try {

        // call API
        const res = yield callAPI(configAPI.HTTP_CREATE, `admin/createcarfirms/`, action.payload);
        console.log("ressssssssssss ", res);
        if (res.errorCode) {
            // alert("Đã có lỗi xảy ra")
            throw new Error(res)
        } else {
            yield put({
                type: types.POST_COMPANY_SUCCESS,
            })
            yield put({
                type: types.GET_COMPANY_REQUEST,
                payload: {
                    page: 1
                }
            })
        }

        // fake user

    } catch (error) {
        yield put({
            type: types.GET_COMPANY_REQUEST,
            payload: {
                page: 1
            }
        })
        yield put({
            type: types.POST_COMPANY_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}
function* putCompanySaga(action) {
    try {
        // call API
        const res = yield callAPI(configAPI.HTTP_CREATE, `admin/updatecarfirms/`, action.payload);
        // fake user

        if (res.errorCode) {
            // alert("Đã có lỗi xảy ra")
            throw new Error(res)
        } else {
            yield put({
                type: types.PUT_COMPANY_SUCCESS,
            })

            yield put({
                type: types.GET_COMPANY_REQUEST,
                payload: {
                    page: 1
                }
            })
        }

    } catch (error) {
        yield put({
            type: types.GET_COMPANY_REQUEST,
            payload: {
                page: 1
            }
        })
        yield put({
            type: types.PUT_COMPANY_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}
function* deleteCompanySaga(action) {
    try {
        // call API
        const res = yield callAPI(configAPI.HTTP_CREATE, `admin/deletecarfirms`, {
            id: action.payload
        });
        // fake user

        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)
        } if (res?.message === 'delete car') {
            alert("Vui lòng xóa toàn bộ xe trong hãng xe này trước !!!")
            throw new Error(res)
        } else {
            yield put({
                type: types.DELETE_COMPANY_SUCCESS,
            })

            yield put({
                type: types.GET_COMPANY_REQUEST,
                payload: {
                    page: 1
                }
            })
        }

    } catch (error) {
        yield put({
            type: types.DELETE_COMPANY_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

export const CompanySaga = [
    takeEvery(types.GET_COMPANY_REQUEST, getCompanySaga),
    takeEvery(types.POST_COMPANY_REQUEST, addCompanySaga),
    takeEvery(types.PUT_COMPANY_REQUEST, putCompanySaga),
    takeEvery(types.DELETE_COMPANY_REQUEST, deleteCompanySaga),
]