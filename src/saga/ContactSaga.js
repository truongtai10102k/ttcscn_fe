import { takeEvery, put } from "redux-saga/effects"
import * as types from '../constants';
import callAPI from "../FetchAPI/callAPI";
import * as configAPI from '../FetchAPI/callAPIConfig'

function* getContactSaga(action) {
    try {
        // call API
        // yield callAPI(GET ,"/ád",{} );
        let { page, status, textSearch } = action.payload
        const skip = types.skip(page);
        if (textSearch === undefined || textSearch === null) {
            textSearch = ''
        }
        // skip : số phần tử bỏ qua
        // status : loại hợp đồng
        // textSearch : tìm kiếm
        const res = yield callAPI(configAPI.HTTP_CREATE, `admin/getlistuserhistory`, {
            textSearch: textSearch, skip: skip, status: status
        });

        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)

        } else {
            yield put({
                type: types.GET_CONTACT_SUCCESS,
                payload: {
                    textSearch,
                    activePage: page,
                    listData: res.data.data,
                    totalPage: Math.ceil(res.data.totalPage / 10),
                }
            })
        }


    } catch (error) {
        yield put({
            type: types.GET_CONTACT_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}
function* getContactByEUSaga(action) {
    try {
        // call API
        // yield callAPI(GET ,"/ád",{} );
        let { page, status, textSearch, userName } = action.payload
        const skip = types.skip(page);
        if (textSearch === undefined || textSearch === null) {
            textSearch = ''
        }
        // skip : số phần tử bỏ qua
        // status : loại hợp đồng
        // textSearch : tìm kiếm
        const res = yield callAPI(configAPI.HTTP_CREATE, `admin/getlistuserhistorybyclient`, {
            textSearch: textSearch, skip: skip, status: status, userName: userName
        });
        console.log("🚀 ~ file: ContactSaga.js ~ line 63 ~ function*getContactByEUSaga ~ res", res)
     
        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)

        } else {
            yield put({
                type: types.GET_CONTRACT_BY_EU_SUCCESS,
                payload: {
                    textSearch,
                    activePage: page,
                    listData: res.data.data,
                    totalPage: Math.ceil(res.data.totalPage / 10),
                }
            })
        }


    } catch (error) {
        yield put({
            type: types.GET_CONTRACT_BY_EU_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

function* putContactSaga(action) {
    try {
        let url = ``;
        if (action.payload.status === 'processing') {
            url = 'admin/acceptrentcar/'
        }
        if (action.payload.status === 'refuse') {
            url = 'admin/refuserentcar'
        }
        if (action.payload.status === 'complete') {
            url = 'admin/completerentcar/'
        }
        const data = {
            id: action.payload.id,
            note: action.payload.note || ''
        }
        // call API
        const res = yield callAPI(configAPI.HTTP_CREATE, url, action.payload);

        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)

        } else {

            // fake user
            yield put({
                type: types.PUT_CONTACT_SUCCESS,
                payload: res
            })

            yield put({
                type: types.GET_CONTACT_REQUEST,
                payload: {
                    page: 1,
                    status: action.payload.type
                }
            })
        }

    } catch (error) {
        yield put({
            type: types.PUT_CONTACT_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

function* postContactSaga(action) {
    try {
        // call API
        const res = yield callAPI(configAPI.HTTP_CREATE, `user/createcontract/`, action.payload);

        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)

        } else {
            yield put({
                type: types.POST_CONTACT_SUCCESS,
                payload: res
            })

            alert("Tạo Hợp đồng thành công")
            window.location.href = window.location.origin + '/home'
        }
    } catch (error) {
        yield put({
            type: types.POST_CONTACT_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

function* exportExcel(action) {
    try {
        // call API
        const res = yield callAPI(configAPI.HTTP_CREATE, `admin/statistical/`, action.payload);

        if (res.errorCode) {
            alert("Đã có lỗi xảy ra")
            throw new Error(res)

        } else {
            yield put({
                type: types.STATISTICAL_SUCCESS,
                payload: res?.data?.contract
            })
        }
    } catch (error) {
        yield put({
            type: types.STATISTICAL_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

export const ContactSaga = [
    takeEvery(types.GET_CONTACT_REQUEST, getContactSaga),
    takeEvery(types.GET_CONTRACT_BY_EU_REQUEST, getContactByEUSaga),
    takeEvery(types.PUT_CONTACT_REQUEST, putContactSaga),
    takeEvery(types.POST_CONTACT_REQUEST, postContactSaga),
    takeEvery(types.STATISTICAL_REQUEST, exportExcel),
]