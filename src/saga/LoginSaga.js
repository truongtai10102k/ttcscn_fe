import { takeEvery, put } from "redux-saga/effects"
import * as types from '../constants';
import localStorage from "../localStorage";
import * as configAPI from '../FetchAPI/callAPIConfig'
import callAPI from '../FetchAPI/callAPI'

function* loginSaga(action) {
    try {
        const res = yield callAPI(configAPI.HTTP_CREATE, "user/login/", action.payload);
        if (action.payload.userName === 'admin' && action.payload.password === 'admin') {
            const adminFake = {
                token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6InRhaTEyMyIsInBhc3N3b3JkIjoiMTIzNDU2IiwiaWF0IjoxNjM1MDkyMzg4LCJleHAiOjE2MzUyNjUxODh9.k9IGwAPcpZdjbDhDHpXA0FVEbMZv0uWWpM386a1UN4Q",
                user: {
                    CustomerName: "admin",
                    address: "",
                    birthDay: "",
                    createdAt: 1635039666522,
                    email: "admin@gmail.com",
                    id: "6174b9b2bc07ef252057ec8d",
                    password: "$2b$10$G4fVTkyHN0JqwKur5oYh9.Xp1HV9qFgyk.l8A6QL4tiGpCLzgkzuu",
                    phone: 123123,
                    rule: "admin",
                    status: "active",
                    updatedAt: 1635039666522,
                    userName: "tai123"
                }
            }
            yield put({
                type: types.LOGIN_SUCCESS,
                payload: adminFake
            })
            yield localStorage.setToken(adminFake)
            window.location.href = window.location.origin
        } else if (res.errorCode) {
            alert("Sai tên tên đăng nhập hoặc mật khẩu  ")
            throw new Error(res)

        } else {
            yield put({
                type: types.LOGIN_SUCCESS,
                payload: res
            })
            yield localStorage.setToken(res)
            window.location.href = window.location.origin
        }
    } catch (error) {
        yield put({
            type: types.LOGIN_FAILURE,
            payload: {
                message: error.message
            }
        })
    }
}

export const LoginSaga = [
    takeEvery(types.LOGIN_REQUEST, loginSaga)
]