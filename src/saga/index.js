import { all } from 'redux-saga/effects';
import { LoginSaga } from './LoginSaga';
import { ContactSaga } from './ContactSaga';
import { CompanySaga } from './CompanySaga';
import { AccountSaga } from './AccountSaga';
import { CarSaga } from './CarSaga';
export default function* rootSaga() {
    yield all([
        ...LoginSaga,
        ...ContactSaga,
        ...CompanySaga,
        ...AccountSaga,
        ...CarSaga,
    ]);
}